﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="media-disclosure.aspx.cs" Inherits="media_disclosure" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Media Disclosure - Alchemy Capital</title>
   <meta name="description" content="Each and every piece of information on the Alchemy Capitals' website is copyright and propriety information. Please read our disclaimer policy here to know more!"/>
  
     <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="http://newuat.alchemycapital.com/media-disclosure.aspx" />

<link rel="canonical" href="http://newuat.alchemycapital.com/media-disclosure.aspx"/>

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Media Disclosure</h1>
                                <ol class="breadcrumb">
                                    <li><a rel="canonical" href="index.aspx">Home</a></li>
                                    <li>Media Disclosure</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40 " id="main-container">

        <div class="container" id="career">
            <div class="row">



                <%--<h2 class="disclaimer_title title-small disc_title">Disclaimer</h2>--%>
                <div class="row">

                    <div class="disc_content">
                        <asp:Literal ID="ltrMediaDisclosure" runat="server"></asp:Literal>

<%--                        <p>
                            Alchemy Capital Management Private Limited ("Alchemy") is established as a private limited company under the laws of India and has its registered office at B-4, Amerchand Mansion, 16 Madame Cama Road, Mumbai 400 001, India and is regulated by the Securities and Exchange Board of India (SEBI) as a Portfolio Manager. By accessing the Alchemy website (www.alchemycapital.com) and any pages thereof or the online services, you agree to be bound by the terms and conditions below. If you do not agree to the terms and conditions below, do not access this website, or any pages thereof, or the online services. 
                        </p>
                        <p>The information, material, advices, suggestions, illustrations, notifications, announcements, disclosures etc. are collectively stated "the content" in this website. Although a reasonable care has been taken to keep the content current and correct,  Alchemy does not warrant accuracy, adequacy or completeness of the content and expressly disclaims any liability for errors or omissions in that regard.</p>
                        <p>
                            Any part of the content is not, and should not be construed as, an offer or solicitation to buy or sell any securities or make any investments. The content shall not to be relied upon as advisory or authoritative or taken in substitution for the exercise of due diligence and judgement by any user nor should it be used as a basis for making any decisions, without exercising user’s own judgment or diligence. 
                        </p>
                        <p>Hyperlinks to other websites made available here are to be accessed at the sole risk of the user; the content, accuracy, opinions expressed, and other links provided by these resources are not investigated, verified, monitored, or endorsed by Alchemy.</p>
                        <p>
                            As a condition for accessing the website, the user agrees that Alchemy Capital Management Pvt. Ltd., its Group companies or affiliates makes no representation and shall have no liability in any way arising to them or any other entity for any loss or damage, direct or indirect, arising from the use of the website. 
                        </p>
                        <p>
                            Alchemy reserves the right to change the content, limit, deny or create different access to the Website and its features with respect to different users or to change/remove any of the features or introduce new features, without prior notice.  
                        </p>
                        <p>
                            The usage and the contents of the Website shall be governed by and construed in accordance with the laws of India. All disputes arising out of or in connection with the usage and the contents of the Website shall be submitted to the exclusive jurisdiction of the courts of Mumbai. 
                        </p>
                        <p>
                            The contents are proprietary information of Alchemy Capital Management Pvt. Ltd and may not be reproduced or otherwise disseminated in whole or in part without written consent. Nobody, other than the authorized officers of Alchemy may alter, delete or modify any of the contents of the Website.
                        </p>--%>


                    </div>


                </div>







            </div>



        </div>



    </section>

    <div class="gap-60"></div>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->


        <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Media Disclosure",
    "item": "https://www.alchemycapital.com/media-disclosure.aspx"  
  }]
}
</script>

</asp:Content>

