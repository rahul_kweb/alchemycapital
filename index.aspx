﻿<%@ Page Title="Top Portfolio Management Companies in India | PMS Provider - Alchemy Capital" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeaderContent">
 <%--   <title>Top Portfolio Management Companies in India | PMS Provider - Alchemy Capital</title>--%>
    <meta name="description" content="Alchemy Capital is amongst India's leading portfolio management company that helps meet your investment goals. Contact us now!" />


    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Top Portfolio Management Companies in India | PMS Provider - Alchemy Capital" />
    <meta property="og:description" content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/" />

    <link rel="canonical" href="https://www.alchemycapital.com/" />

    <link rel="alternate" href="https://www.alchemycapital.com/" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="carousel slide" id="main-slide" data-ride="carousel">
        <!-- Indicators-->
        <ol class="carousel-indicators visible-lg visible-md">
            <li class="active" data-target="#main-slide" data-slide-to="0"></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
        </ol>
        <!-- Indicators end-->
        <!-- Carousel inner-->
        <div class="carousel-inner">

            <asp:Literal ID="ltrHomeBanner" runat="Server"></asp:Literal>
            <%--  --%>
        </div>
        <!-- Carousel inner end-->

        <%--    Added on 29_06_2021--%>
        <div class="newsTicker">
            <div class="container">
                <div class="carouselTicker carouselTicker-start">
                    <ul class="carouselTicker__list">
                         <li class="carouselTicker__item"><span>Alchemy Speaks – November 2021</span><a href="https://youtu.be/dVVyPHgM3qg" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Hiren Ved on ET Now</span><a href="https://economictimes.indiatimes.com/markets/expert-view/where-to-look-for-hidden-gems-that-can-take-this-bull-market-higher/articleshow/87645657.cms?from=mdr" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Broader markets taking a pause</span><a href="http://newuat.alchemycapital.com/AlchemyTest/thought-leadership/broader-markets-taking-a-pause.aspx" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Investment Matters, July 2021</span><a href="http://newuat.alchemycapital.com/AlchemyTest/Content/uploads/Pdf/Pdf--132703018018868092.pdf" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Decoding Alchemy Ascent</span><a href="https://www.youtube.com/watch?v=vkA4QexWJXs" target="_blank">View More</a></li>
                         <li class="carouselTicker__item"><span>Hiren Ved on The Hindu Business Line</span><a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132742885546043504.pdf" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Market Views – November 2021</span><a href="https://youtu.be/CIWNY-qz3f4" target="_blank">View More</a></li>
                        <li class="carouselTicker__item"><span>Hiren Ved on ET Now</span><a href="https://youtu.be/NBOayKAxWYM" target="_blank">View More</a></li>                  
                    </ul>
                </div>
            </div>
        </div>

        <!-- Controllers-->
        <a class="left carousel-control carousel-control-prev" href="#main-slide" data-slide="prev"><span><i class="fa fa-angle-left"></i></span></a>
        <a class="right carousel-control carousel-control-next" href="#main-slide" data-slide="next"><span><i class="fa fa-angle-right"></i></span></a>
    </div>


    <%--   <div class="row text-center">
      <div class="col-lg-12 mrt-30">
                  <p class="section_new-title"><strong> Learn About</strong><a href="/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx"> COVID-19: Impact on Economic Prospects, Markets and Investments</a> </p>
               </div>
         </div>--%>

    <!-- Carousel end-->
    <section class="section-area text-center">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 mrt-30">
                    <h1 style="display:none">Leading Asset Management & Portfolio Management Services company in India</h1>
                    <h2 class="section-title">Our Founders</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="b-team-carousel row">
                        <asp:Literal ID="ltrBindTeam" runat="server"></asp:Literal>
                        <%--       <section class="b-team-2 col-sm-3 wp-animated" data-animation="fadeInUp" data-animation-delay="0.2s">
                                    <div class="b-team-2__inner">
                                        <div class="b-team-2__media">
                                            <img src="Content/images/team/1.jpg" alt="" />
                                        </div>
                                        <h3 class="b-team-2__name">Lashit Sanghvi</h3>
                                        <div class="b-team-2__category">Co-Founder & Whole-Time Director</div>
                                    </div>
                                   <div class="b-team-hover">
                                    	<div>
                                        <div class="b-team-hover__info">
                                        Lashit Sanghvi, Director & CEO of Alchemy India: Mr. Sanghvi has over 18 years of experience and has been featured in CNBC’s “Wizards of Dalal Street”,
                                        </div>
                                         <a href="people_and_culture.html" class="read_more">read more..</a>
                                        <!--<a class="b-team-hover__btn btn btn-default" href="#">Know more<i class="icon"></i></a>-->
                                        </div>
                                    </div>
                                  
                                </section>
                                <!-- end .b-team-->
                                <section class="b-team-2 col-sm-3 wp-animated" data-animation="fadeInUp" data-animation-delay="0.4s" >
                                    <div class="b-team-2__inner">
                                        <div class="b-team-2__media">
                                            <img src="Content/images/team/2.jpg" alt="" />
                                        </div>
                                        <h3 class="b-team-2__name">Hiren Ved</h3>
                                        <div class="b-team-2__category">Co-Founder & Whole-Time Director</div>
                                    </div>
                                    <div class="b-team-hover">
                                    	<div>
                                        <div class="b-team-hover__info">
                                       Hiren Ved,  Director and CIO of Alchemy India: Mr. Ved started his career by working with KR Choksey & Co, a sell side firm known for its value investment approach, 
                                        </div>
                                         <a href="people_and_culture.html" class="read_more">read more..</a>
                                        <!--<a class="b-team-hover__btn btn btn-default" href="#">Know more<i class="icon"></i></a>-->
                                        </div>
                                    </div>
                                </section>
                                <!-- end .b-team-->
                                <section class="b-team-2 col-sm-3 wp-animated" data-animation="fadeInUp" data-animation-delay="0.6s" >
                                    <div class="b-team-2__inner">
                                        <div class="b-team-2__media">
                                            <img src="Content/images/team/rakesh_zunzunwala.jpg" alt="" />
                                        </div>
                                        <h3 class="b-team-2__name">Rakesh Jhunjhunwala</h3>
                                        <div class="b-team-2__category">Co-Founder</div>
                                    </div>
                                 <div class="b-team-hover">
                                    <div>
                                        <div class="b-team-hover__info">
                                        Rakesh Jhunjhunwala: An experienced, longstanding investor in the Indian capital markets. Mr. Jhunjhunwala has made all his wealth by investing and trading in the Indian markets 


                                        </div>
                                         <a href="people_and_culture.html" class="read_more">read more..</a>
                                        <!--<a class="b-team-hover__btn btn btn-default" href="#">Know more<i class="icon"></i></a>-->
                                      </div>
                                    </div>
                                </section>
                                <!-- end .b-team-->
                                <section class="b-team-2 col-sm-3 wp-animated" data-animation="fadeInUp" data-animation-delay="0.8s" >
                                    <div class="b-team-2__inner">
                                        <div class="b-team-2__media">
                                            <img src="Content/images/team/4.jpg" alt="Foto" />
                                        </div>
                                        <h3 class="b-team-2__name">Ashwin Kedia</h3>
                                        <div class="b-team-2__category">Co-Founder</div>
                                    </div>
                                  <div class="b-team-hover">
                                    <div>
                                            <div class="b-team-hover__info">
                                       Ashwin Kedia:  He possesses about 2 decades of comprehensive equity market experience. His key strengths are stock picking and developing corporate relationships. 
                                        </div>
                                        <a href="people_and_culture.html" class="read_more">read more..</a>

                                        <!--<a class="b-team-hover__btn btn btn-default" href="#">Know more<i class="icon"></i></a>-->
                                      </div>
                                    </div>
                                </section>--%>
                        <!-- end .b-team-->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Team end-->
    <section class="ts-features no-padding sec_2">
        <div class="row text-center">
            <div class="col-lg-12 mrt-30">
               <%-- <h2 class="section-title">Our Products & Services</h2>--%>
                 <h2 class="section-title">Our Products & Services</h2>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
            </div>
            <!-- Row end-->
        </div>
        <!-- Container end-->

        <div class="container text-center my-5">
            <h3 class="call-to-action-title">DIRECT ONBOARDING ROUTE</h3>
            <p style="font-size: 18px">All clients have an option to invest in the above products / investment approaches directly, without intermediation of persons engaged in distribution services.</p>
        </div>
    </section>
    <!-- Section Product end-->

    <section id="call-to-action" class="call-to-action solid-bg  ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="call-to-action-title">ALCHEMY ADVANTAGE</h3>
                    <asp:Literal ID="ltrAdvantage" runat="server"></asp:Literal>
                </div>
                <!--<div class="col-lg-4 text-right">
                  <a class="btn btn-primary" href="">Get Free Consultation</a>
               </div>-->
            </div>
        </div>
    </section>
    <!-- Section ADVANTAGE end-->

    <section class="">
        <div class="row facts-wrapper text-center mrt-60 mrb-40">

            <asp:Literal ID="ltrHomeStatistic" runat="server"></asp:Literal>

            <%--  <div class="col-md-4">

                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="count1">1999</span></h4>
                              <p class="facts-desc">Founded</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                     <div class="col-md-4">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="count1">100</span></h4>
                              <p class="facts-desc">Million USD funds managed</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                     <div class="col-md-4">
                        <div class="ts-facts facts-col"><span class="facts-icon"><i class="icon icon-users"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="count1">700</span>+</h4>
                              <p class="facts-desc">Families</p>
                           </div>
                        </div>
                        <!-- Facts end-->
     				 </div>
            --%>
        </div>
    </section>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  }]
}
    </script>



</asp:Content>

