﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="people-and-culture.aspx.cs" Inherits="People_and_Culture" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Who We Are? | People and Culture at Alchemy Capital</title>
   <meta name="description" content="Learn about the masterminds behind Alchemy Capital's success. Meet our financial advisors and fund managers, who can assist you in developing amazing investing strategies through different investments."/>

     <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/people-and-culture.aspx" />


<link rel="canonical" href="https://www.alchemycapital.com/people-and-culture.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/people-and-culture.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/people-and-culture.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">People &amp; Culture</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="javascript:void(0);">Who We Are</a></li>
                                    <li>People &amp; Culture</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">
        <div class="container people_culture mrb-40">

            <asp:Literal ID="ltrOverview" runat="server"></asp:Literal>

            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTabHeading" runat="server"></asp:Literal>

                </ul>
                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrFounder" runat="server"></asp:Literal>

                </div>
            </div>



            <div class="clearfix"></div>
        </div>


    </section>


    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "People & Culture",
    "item": "https://www.alchemycapital.com/people-and-culture.aspx"  
  }]
}
</script>


</asp:Content>

