﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="india-markets-post-election-outlook.aspx.cs" Inherits="india_markets_post_election_outlook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
    <title>India Markets - Post Election Outlook | Alchemy Capital Management</title>
   <meta name="description" content="Know about the 2019 elections effect on the Indian Stock Market. With policy options and reform for privatization, infrastructure sector, & much more."/>
  
    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="India Markets - Post Election Outlook | Alchemy Capital Management" />
    <meta property="og:description"  content="Know about the 2019 elections effect on the Indian Stock Market. With policy options and reform for privatization, infrastructure sector, & much more." />

    <meta property="og:url" content="https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx" />


<!-- Twitter -->
    <meta property="twitter:card"content="summary_large_image"/>
    <meta property="twitter:url"content="https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx"/>
    <meta property="twitter:title"content="India Markets - Post Election Outlook | Alchemy Capital Management"/>
    <meta property="twitter:description"content="Know about the 2019 elections effect on the Indian Stock Market. With policy options and reform for privatization, infrastructure sector, & much more."/>
    <meta property="twitter:image"content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png"/>

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx" hreflang="x-default" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>India Markets: Post-election Outlook</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css">

        <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "India Markets: Post-election Outlook",
    "item": "https://www.alchemycapital.com/thought-leadership/india-markets-post-election-outlook.aspx"  
  }]
}
</script>

</asp:Content>

