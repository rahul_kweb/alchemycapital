﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="covid-19-impact-on-economic-prospects-markets-and-investments.aspx.cs" Inherits="thought_leadership_covid_19_impact_on_economic_prospects_markets_and_investments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
        <title>COVID-19: Impact on Economic Prospects, Markets and Investments - Alchemy Capital</title>
   <meta name="description" content="COVID-19: Impact on Economic Prospects, Markets and Investments. Read our article to know more."/>
  
    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="COVID-19: Impact on Economic Prospects, Markets and Investments" />

    <meta property="twitter:image:src" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="twitter:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="twitter:description"  content="COVID-19: Impact on Economic Prospects, Markets and Investments" />

     <meta property="twitter:card" content="summary_large_image"/>

    <meta property="og:url" content="https://www.alchemycapital.com/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx" hreflang="x-default" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>COVID-19: Impact on Economic Prospects, Markets and Investments</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css">

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "COVID-19: Impact on Economic Prospects, Markets and Investments",
    "item": "https://www.alchemycapital.com/thought-leadership/covid-19-impact-on-economic-prospects-markets-and-investments.aspx"  
  }]
}
</script>
</asp:Content>

