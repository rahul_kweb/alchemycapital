﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="market-looking-for-macro-cues.aspx.cs" Inherits="thought_leadership_market_looking_for_macro_cues" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
       <title>Market Looking For Macro Cues - Alchemy Capital</title>
<meta name="title"content="Market Looking For Macro Cues - Alchemy Capital">
<meta name="description"content="NSE Midcap (up 7.8%) & NSE Small cap (up 11.5%) both outperforming as Nifty rallying 3% in August with contraction of 23.9% in 1Q GDP. Read More">

    <!-- Open Graph / Facebook -->
<meta property="og:type"content="website">
<meta property="og:url"content="https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx">
<meta property="og:title"content="Market Looking For Macro Cues - Alchemy Capital">
<meta property="og:description"content="NSE Midcap (up 7.8%) & NSE Small cap (up 11.5%) both outperforming as Nifty rallying 3% in August with contraction of 23.9% in 1Q GDP. Read More">
<meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />

    <!-- Twitter -->
<meta property="twitter:card"content="summary_large_image">
<meta property="twitter:url"content="https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx">
<meta property="twitter:title"content="Market Looking For Macro Cues - Alchemy Capital">
<meta property="twitter:description"content="NSE Midcap (up 7.8%) & NSE Small cap (up 11.5%) both outperforming as Nifty rallying 3% in August with contraction of 23.9% in 1Q GDP. Read More">
<meta property="twitter:image"content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png">



<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>Market Looking For Macro Cues</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css">

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Market Looking For Macro Cues",
    "item": "https://www.alchemycapital.com/thought-leadership/market-looking-for-macro-cues.aspx"  
  }]
}
</script>
</asp:Content>

