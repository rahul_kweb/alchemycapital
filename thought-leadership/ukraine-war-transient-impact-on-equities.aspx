﻿<%@ Page Title="Ukraine War: Transient Impact On Equities | Seshadri Sen" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="ukraine-war-transient-impact-on-equities.aspx.cs" Inherits="thought_leadership_ukraine_war_transient_impact_on_equities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">

<!-- Primary Meta Tags -->
<%--<meta name="title" content="Ukraine War: Transient Impact On Equities | Seshadri Sen">--%>
<meta name="description" content="Views on the Macro positives for India, sectoral outlook, and geopolitical risk resulted due to Russia’s invasion of Ukraine military. Read article by Seshadri Sen">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://www.alchemycapital.com/thought-leadership/ukraine-war-transient-impact-on-equities.aspx">
<meta property="og:title" content="Ukraine War: Transient Impact On Equities">
<meta property="og:description" content="Ukraine War: Transient Impact On Equities">
<meta property="og:image" content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png">

<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content=" https://www.alchemycapital.com/thought-leadership/ukraine-war-transient-impact-on-equities.aspx">
<meta property="twitter:title" content="Ukraine War: Transient Impact On Equities">
<meta property="twitter:description" content="Ukraine War: Transient Impact On Equities">
<meta property="twitter:image" content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png">

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/ukraine-war-transient-impact-on-equities.aspx"/>



</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>Ukraine War: Transient Impact On Equities</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css"/>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Near-term noise, long-term opportunity",
    "item": "https://www.alchemycapital.com/thought-leadership/ukraine-war-transient-impact-on-equities.aspx"  
  }]
}
</script>
</asp:Content>
