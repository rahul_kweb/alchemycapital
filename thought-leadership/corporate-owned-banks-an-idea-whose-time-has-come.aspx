﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="corporate-owned-banks-an-idea-whose-time-has-come.aspx.cs" Inherits="thought_leadership_corporate_owned_banks_an_idea_whose_time_has_come" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
<!-- Primary Meta Tags -->
<title>Corporate-Owned Banks: An Idea Whose Time Has Come | Alchemy Capital Management</title>
<meta name="title"content="Corporate-Owned Banks: An Idea Whose Time Has Come | Alchemy Capital Management">
<meta name="description"content="India needs loan growth in double-digits for GDP above 6%.Faster loan growth will need new banks, and most viable model is corporate-owned banks. Read More">

<!-- Open Graph / Facebook -->
<meta property="og:type"content="website">
<meta property="og:url"content="https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx">
<meta property="og:title"content="Corporate-Owned Banks: An Idea Whose Time Has Come | Alchemy Capital Management">
<meta property="og:description"content="India needs loan growth in double-digits for GDP above 6%. Faster loan growth will need new banks, and most viable model is corporate-owned banks. Read More">
<meta property="og:image" content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png">

<!-- Twitter -->
<meta property="twitter:card"content="summary_large_image">
<meta property="twitter:url"content=" https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx">
<meta property="twitter:title"content="Corporate-Owned Banks: An Idea Whose Time Has Come | Alchemy Capital Management">
<meta property="twitter:description"content="India needs loan growth in double-digits for GDP above 6%. Faster loan growth will need new banks, and most viable model is corporate-owned banks. Read More">
<meta property="twitter:image" content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png">

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx" hreflang="en-in" />

<link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>Corporate-Owned Banks: An Idea Whose Time Has Come</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css"/>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Corporate-Owned Banks: An Idea Whose Time Has Come",
    "item": "https://www.alchemycapital.com/thought-leadership/corporate-owned-banks-an-idea-whose-time-has-come.aspx"  
  }]
}
</script>
</asp:Content>

