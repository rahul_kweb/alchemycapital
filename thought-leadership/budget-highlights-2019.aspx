﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="budget-highlights-2019.aspx.cs" Inherits="budget_highlights_2019" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
        <title>Budget Highlights 2019 by Fund Managers | Alchemy Capital</title>
   <meta name="description" content="Budgets are more about policy thinking and signalling than anything else. Read more on Budget 2019 from our research experts and fund managers, including opportunities for investors."/>


    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Budget Highlights 2019" />

    <meta property="og:url" content="https://www.alchemycapital.com/thought-leadership/budget-highlights-2019.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership/budget-highlights-2019.aspx"/>


    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/budget-highlights-2019.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership/budget-highlights-2019.aspx" hreflang="x-default" />


    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
          <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h2 class="banner-title">Insights</h2>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="/">Home</a></li>
                        <li><a href="/thought-leadership.aspx">Thought Leadership</a></li>
                      <li>Budget Highlights 2019</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <div class="container" id="blog">


        <h1 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h1>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
             <%--   <div id='shareIcons'></div>--%>

               <%--   <div>
                <a class="buttonwhatsapp" data-sharer="whatsapp" data-title="" data-url="">Whatsapp</a>
                                                 
                <a class="buttonlinkedin" data-sharer="linkedin" data-url="">Linkedin</a>

                <a class="buttontwitter" data-sharer="twitter" data-title="" data-hashtags="" data-url="">Twitter</a>

                <a class="buttonemail" data-sharer="email" data-title="" data-url="" data-subject="" data-to="">mail</a>
                </div>--%>
              
                <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>

     
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css">


    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Budget Highlights",
    "item": "https://www.alchemycapital.com/thought-leadership/budget-highlights-2019.aspx"  
  }]
}


</script>

   


</asp:Content>

