﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thought_leadership_ukraine_war_transient_impact_on_equities : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(8);
            BindBlog();
        }

    }

    public void BindInnerBanner(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");

        if (dt.Rows.Count > 0)
        {

            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }
    }


    public void BindBlog()
    {
        StringBuilder strBuild = new StringBuilder();
        StringBuilder strHeading = new StringBuilder();

        DataTable dt = new DataTable();
        string PageName = "ukraine-war-transient-impact-on-equities";

        dt = utility.Display("Exec Proc_ThoughtLeadership 'bindThoughtLeadershipbyId',0,'','','','','','" + PageName + "'");

        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["Image"].ToString() != string.Empty)
            {
                strBuild.Append("<div class='floatDiv'>");
                strBuild.Append("<img class='img-fluid' src='/Content/uploads/ThoughtLeadership/" + dt.Rows[0]["Image"].ToString() + "' alt=''>");
                strBuild.Append("</div>");
            }

            strBuild.Append("<div class='row justify-content-between align-items-center mb-4'>");

            strBuild.Append("<div class='date col-md-auto'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + dt.Rows[0]["PostDate"].ToString() + "</div>");

            strBuild.Append("<div class='col-md-auto'>");
            strBuild.Append("<small class='d-inline-block'><b><i>If you find this read interesting, share it on:</i></b></small>");
            strBuild.Append("<ul class='d-inline-flex mb-0 ml-1 socialbar'>");
            strBuild.Append("<li class='px-1'><a class='buttonwhatsapp' data-sharer='whatsapp' data-title='' data-url=''><i class='fa fa-whatsapp' aria-hidden='true'></i></a></li>");
            strBuild.Append("<li class='px-1'><a class='buttonlinkedin' data-sharer='linkedin' data-url=''><i class='fa fa-linkedin' aria-hidden='true'></i></a></li>");
            strBuild.Append("<li class='px-1'><a class='buttontwitter' data-sharer='twitter' data-title='' data-hashtags='' data-url=''><i class='fa fa-twitter ' aria-hidden='true'></i></a></li>");
            strBuild.Append("<li class='px-1'><a class='buttonemail' data-sharer='email' data-title='' data-url='' data-subject='' data-to=''><i class='fa fa-envelope' aria-hidden='true'></i></a></li>");

            strBuild.Append("</ul>");
            strBuild.Append("</div>");

            strBuild.Append("</div>");

            strBuild.Append(dt.Rows[0]["Description"].ToString());

            strHeading.Append(dt.Rows[0]["Heading"].ToString());


            ltrHeading.Text = strHeading.ToString();
            ltrBlogDetail.Text = strBuild.ToString();
        }


    }
}