﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class disclaimer_disclosure : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner(14);
            BindGeneralDisclaimer(12);
        }
    }


    public string BindGeneralDisclaimer(int Id)
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS'," + Id + "");
        if (dt.Rows.Count > 0)
        {
            ltrDesclaimer.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrDesclaimer.Text;
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }
}