﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OurProcess : referer
{
    Utility utility = new Utility();
    string response;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindOverview();
            BindInnerBanner(6);
        }

    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindOverview()
    {       
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS',3");
        if (dt.Rows.Count > 0)
        {
            response = dt.Rows[0]["Description"].ToString();
        }
        ltrOverview.Text = response.ToString();
    }
}