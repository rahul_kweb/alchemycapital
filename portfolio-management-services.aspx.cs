﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class portfolio_management_services : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            Bindliteral();
            BindInnerBanner(5);
            porfolioOverview();
        }

    }


    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public string porfolioOverview()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS',11");
        if (dt.Rows.Count > 0)
        {
            ltrOverview.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrOverview.Text;
    }

    public void Bindliteral()
    {
        StringBuilder strHeading = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_whatWeOfferTab 'bindWhtaWeOfferTab'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strHeading.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /><span>" + dr["Title"].ToString() + "</span></li>");
              
                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_whatWeOfferTab 'bindWhtaWeOfferById'," + dr["Id"].ToString() + "");
                if (dt1.Rows.Count > 0)
                {

                    strDesc.Append("<div>");

                    foreach (DataRow dr1 in dt1.Rows)
                    {
                      
                        int Cid = int.Parse(dr1["WhatOfferId"].ToString());
                        strDesc.Append("<h2 class='service-title'><img src='Content/uploads/Icon/" + dr1["Icon"].ToString() + "' alt='' aria-hidden='true' />" + dr1["Title"].ToString() + "</h2>");
                        strDesc.Append(dr1["Description"].ToString());
                        if (dr1["Title"].ToString() == "Alchemy Leaders of Tomorrow – (AIF CAT-III)")
                        {
                            strDesc.Append("<p><a class='link-more' href='/aif/" + dr1["Url"].ToString() + ".aspx'>Read More<i class='icon icon-right-arrow2'></i></a></p>");

                        }
                        else
                        {
                            strDesc.Append("<p><a class='link-more' href='/portfolio-management-services/" + dr1["Url"].ToString() + ".aspx'>Read More<i class='icon icon-right-arrow2'></i></a></p>");
                        }
                    }

                    if (dr["Title"].ToString() == "PMS")
                    {

                        strDesc.Append("<div style='background: #f2f2f2; padding: 15px;'>");
                        strDesc.Append("<p class='mb-2'><strong>Direct Onboarding Route</strong></p>");
                        strDesc.Append("<div>");
                        strDesc.Append("<p class='mb-0'>All clients have an option to invest in the above products / investment approaches directly, without intermediation of persons engaged in distribution services.</p>");
                        strDesc.Append("</div>");
                        strDesc.Append("</div>");


                    }

                    strDesc.Append("</div>");

                  
                }
            }
            ltrTab.Text = strHeading.ToString();
            ltrdescription.Text = strDesc.ToString();
        }
    }
}