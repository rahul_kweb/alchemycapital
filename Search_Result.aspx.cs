﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Search_Result : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["SearchKey"] != null)
            {
                string value = Request.QueryString["SearchKey"].ToString();

                BindSearch_Result(value);
            }

        }

    }


    public void BindSearch_Result(string value1)
    {
        lbTitle.Text = value1;

        StringBuilder strHeading = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();
       

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_SearchResult 'GET_SEARCH','" + value1 + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strHeading.Append("<li>");
                strHeading.Append("<a href=" + dr["PageName"] + " class='search_hd'> "+ dr["Heading"].ToString() + "</a>");
               

                string your_String = dr["Description"].ToString();
                //string resultString = Regex.Replace(your_String, "[(<.+?)\s+style\s*=\s*(["']).*?\2(.*?>)]", "");

                string pattern = @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>";
                string Output = Regex.Replace(your_String, pattern, string.Empty).Replace("\r\n\t", "");


                strHeading.Append("<p>" + Output + "</p>");
                strHeading.Append("</li>");

                //objDictionary.Add("Description", strDesc.ToString());
            }
        }
        ltrDescription.Text= strHeading.ToString();
    }


}