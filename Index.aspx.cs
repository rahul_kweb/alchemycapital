﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Index : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
   
        if (!IsPostBack)
        {
            HomeBanner();
            BindFounders();
            BindOurProduct();
            AlchemyAdvantage();
            BindHomeStatistic();
        
        }

}

    public string HomeBanner()
    {

        StringBuilder strBuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Banner 'bindBanner'");
        if (dt.Rows.Count > 0)
        {
            int count = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (count == 1)
                {
                    strBuild.Append("<div class='carousel-item active' style='background-image:url(Content/uploads/HomeBanner/" + dr["Image"].ToString() + ");'>");
                    strBuild.Append("<div class='container'>");
                    strBuild.Append("<div class='slider-content text-center'>");
                    strBuild.Append("<div class='col-md-12'>");
                    strBuild.Append("<h3 class='slide-sub-title'>" + dr["Description"].ToString() + "</h3>");
                    strBuild.Append("<p><a class='slider btn btn-primary' rel='canonical'  href='/portfolio-management-services.aspx'>Our Services</a><a class='slider btn btn-border'rel='canonical'  href='contact.aspx' onclick='gtag('event', 'Click', { 'event_category': 'Lead Tracking',  'event_label':'Contact us Banner Image'});'>Contact Us</a></p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                else
                {
                    strBuild.Append("<div class='carousel-item' style='background-image:url(Content/uploads/HomeBanner/" + dr["Image"].ToString() + ");'>");
                    strBuild.Append("<div class='container'>");
                    strBuild.Append("<div class='slider-content text-center'>");
                    strBuild.Append("<div class='col-md-12'>");
                    strBuild.Append("<h3 class='slide-sub-title'>" + dr["Description"].ToString() + "</h3>");
                    strBuild.Append("<p><a rel='canonical' class='slider btn btn-primary' href='/portfolio-management-services.aspx'>Our Services</a><a class='slider btn btn-border' rel='canonical' href='contact.aspx' onclick='gtag('event', 'Click', { 'event_category': 'Lead Tracking',  'event_label':'Contact us Banner Image'});'>Contact Us</a></p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                count++;
            }

            ltrHomeBanner.Text = strBuild.ToString();
        }
        return ltrHomeBanner.Text;
    }


    public void BindFounders()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_ThoughtLeadership bindTeamInHome");
        StringBuilder strbuild = new StringBuilder();
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strbuild.Append("<section class='b-team-2 col-md-3 col-sm-6 wp-animated' data-animation='fadeInUp' data-animation-delay='0.2s'>");

                strbuild.Append("<div class='b-team-2__inner'>");
                strbuild.Append("<div class='b-team-2__media'>");
                strbuild.Append("<img src='Content/uploads/Team/" + dr["Image"].ToString() + "' alt='" + dr["Name"].ToString() + "-" + dr["Designation"].ToString() + "-Alchemy Capital" + "' />");
                strbuild.Append("</div>");
                strbuild.Append("<h3 class='b-team-2__name'>" + dr["Name"].ToString() + "</h3>");
                strbuild.Append("<div class='b-team-2__category'>" + dr["Designation"].ToString() + "</div>");
                strbuild.Append("</div>");


                strbuild.Append("<div class='b-team-hover'>");
                strbuild.Append("<div>");
                strbuild.Append("<div class='b-team-hover__info'>");
                strbuild.Append(dr["Description"].ToString());
                strbuild.Append("</div>");
                strbuild.Append("<a href='people-and-culture.aspx#" + dr["Url"].ToString() + "' class='read_more'>");
                strbuild.Append("Read More..");
                strbuild.Append("</a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");


                strbuild.Append("</section>");

            }
            ltrBindTeam.Text = strbuild.ToString();
        }

    }

    public string BindOurProduct()
    {
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_OurProduct 'bindOurProduct'");
        if (dt.Rows.Count > 0)
        {
            int count = 1;
            foreach (DataRow dr in dt.Rows)
            {
                strbuild.Append("<div class='col-lg-3 feat feature-box" + count + "' style='background-image: url(Content/uploads/HomeBanner/" + dr["Image"].ToString() + ");'>");
                strbuild.Append("<div class='ts-feature text-center'>");

                strbuild.Append("<div class='ts-feature-info'>");
                strbuild.Append("<img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' />");
                strbuild.Append("<h3 class='ts-feature-title'>" + dr["Heading"].ToString() + "</h3>");
                strbuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strbuild.Append("</div>");
                strbuild.Append("<a class='b-team-hover__btn btn btn-default' href=" + dr["Url"].ToString() + ">Read more</i></a>");

                strbuild.Append("</div>");
                strbuild.Append("</div>");

                count++;
            }
            ltrProduct.Text = strbuild.ToString();
        }
        return ltrProduct.Text;
    }

    public string AlchemyAdvantage()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS',8");
        if (dt.Rows.Count > 0)
        {
            ltrAdvantage.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrAdvantage.Text;
    }

    public void BindHomeStatistic()
    {
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_HomeStatistic 'get'");
        foreach (DataRow dr in dt.Rows)
        {
            strbuild.Append("<div class='col-md-4'>");
            strbuild.Append("<div class='ts-facts'><span class='facts-icon'><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /></span>");
            strbuild.Append("<div class='ts-facts-content'>");
            strbuild.Append("<h4 class='ts-facts-num'><span class='count1'>" + dr["Counting"].ToString() + "</span></h4>");
            strbuild.Append("<p class='facts-desc'>" + dr["Description"].ToString() + "</p>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
            strbuild.Append("</div>");
        }
        ltrHomeStatistic.Text = strbuild.ToString();
    }


    //This method for alchemy ascent pop up click count
    [WebMethod]
    public static void Count_ascent_popup_click()
    {
        Utility utility = new Utility();

        using (WebClient client = new WebClient())
        {
            var json = client.DownloadString("https://geoip-db.com/json");
            IPLocation location = new JavaScriptSerializer().Deserialize<IPLocation>(json);
         

            using (SqlCommand cmd = new SqlCommand("Proc_AscentPopUpHit"))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@IpAddress", location.IPv4);
                cmd.Parameters.AddWithValue("@CityName", location.city);
                cmd.Parameters.AddWithValue("@Region", location.state);
                cmd.Parameters.AddWithValue("@CountryName", location.country_name);


                utility.Execute(cmd);

            }


        }

    }
}