﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="thank-you.aspx.cs" Inherits="thank_you" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#0f2765" />
    <meta name="description" content="Alchemy Capital Mangement is leading fund management company which offers Financial Management & Equity Investment in India. Visit Alchemy online to know more.">
    <title>Asset Management & PMS Provider in India -  Alchemy Capital </title>
    <link rel="shortcut icon" href="https://www.alchemycapital.com/Content/images/favicon.ico" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="landing/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="landing/css/landing.css">

    <!-- 
Start of global snippet: Please do not remove
Place this snippet between the <head> and </head> tags on every page of your site.
-->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-10745315"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'DC-10745315');
    </script>
    <!-- End of global snippet: Please do not remove -->
</head>
<body>

    <header>
        <div class="container"><a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Teaser&utm_content=invest-compass" class="logo">
            <img src="landing/images/logo.png" title="Alchemy Capital" class="w-100"></a></div>
    </header>

    <section class="aboutSection">

        <div class="container">

            <div class="aboutContent mx-auto mb-5 text-justify">
                <h2 class="title text-center text-uppercase font-weight-bold mb-4">Thank you</h2>
                <p class="text-center mb-4">Thank you for sharing your details with us. Our representative will contact you soon.</p>
                <p class="text-center mb-4">Meanwhile, you can visit our website <a href="https://www.alchemycapital.com">www.alchemycapital.com</a> to know more about us and our products.</p>
            </div>

        </div>

    </section>

    <footer class="mt-lg-4">
        <div class="container text-center text-lg-left">

            <div class="row">
                <div class="col-lg-auto">
                    Copyright &copy; 2021 Alchemy. All Rights Reserved.<br>
                    <a href="https://www.alchemycapital.com/disclaimer-disclosure.aspx?utm_source=DV360&utm_medium=Display&utm_campaign=Teaser&utm_content=invest-compass" target="_blank">Disclaimer</a> -  Investments in securities market are subject to market risks.
                </div>
                <div class="col-lg-auto pl-lg-5 ml-lg-5">
                    <div class="row mx-auto no-gutters socialBtns">
                        <%--<div class="col"><a href="https://www.facebook.com/alchemycapital/" target="_blank">
                            <img src="landing/images/ic_fb.png"></a></div>--%>
                        <div class="col"><a href="https://www.youtube.com/channel/UCFvIGco1pAKZyVIZFGWf2ZQ/featured" target="_blank">
                            <img src="landing/images/ic_yt.png"></a></div>
                        <div class="col"><a href="https://www.linkedin.com/company/alchemy-capital-management" target="_blank">
                            <img src="landing/images/ic_li.png"></a></div>
                    </div>
                </div>
                <div class="col-lg-auto text-lg-right ml-lg-auto">
                    <a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Teaser&utm_content=invest-compass">www.alchemycapital.com</a><br>
                    <a href="https://www.kwebmaker.com" target="_blank" class="kweb">Kwebmaker&trade;</a>
                </div>
            </div>

        </div>
    </footer>


    <script src="landing/js/jquery-3.5.1.min.js"></script>
    <script src="landing/js/bootstrap.bundle.min.js"></script>

       <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-51603925-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-51603925-1');
    </script>

<!--
Event snippet for Alchemy_Capital_Conversion_Unique_New on : Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 07/26/2021
-->
<script>
  gtag('event', 'conversion', {
    'allow_custom_scripts': true,
    'send_to': 'DC-10745315/invmedia/alche00+unique'
  });
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=10745315;type=invmedia;cat=alche00;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ord=1;num=1?" width="1" height="1" alt=""/>
</noscript>
<!-- End of event snippet: Please do not remove -->


    <!-- Event snippet for Submit lead form conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-10797805103/fLcsCOn4qv8CEK_c5Zwo'});
</script>



</body>
</html>
