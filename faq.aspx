﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="Server">
    <title>FAQ's| Alchemy Capital |Portfolio Management Services</title>
   <meta name="description" content="Get answers to a list of commonly asked questions related to Alchemy Capital Management Portfolio Management Service in India."/>

  <link rel="canonical" href="https://www.alchemycapital.com/faq.aspx" />

    <link rel="alternate" href="https://www.alchemycapital.com/faq.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/faq.aspx" hreflang="x-default" />



    <style>
        .newsData span {
            color: #252a37;
            display: block;
            vertical-align: middle;
            text-align: justify;
        }

        .main-container .resp-tabs-container h2 {
            font-size: 18px;
            padding-right: 35px;
        }

        .resp-tab-content ul li.newsData {
            background: url(/Content/images/bullet.png) no-repeat left 8px !important;
        }

        .main-container .resp-tabs-container h2 {
            color: #0f2765;
        }

        .faq .resp-tab-content ul li.newsData {
            font-weight: normal;
        }

        .faq ul.newsLetter li:hover {
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
        }


        .faq ul.newsLetter li a {
            color: #f8bb02 !important;
        }

            .faq ul.newsLetter li a:hover {
                text-decoration: underline !important;
                color: #f8bb02 !important;
            }

        .faq h2.accordionHead {
            border-bottom: none;
        }

        /*added on  20_08_2020*/
        .faq .resp-tab-content ul li.newsData span {
            text-align: left;
        }

        @media only screen and (max-width: 767px) {
            .faq h2.resp-accordion span {
                color: #ffae00;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/InnerBanner-c8d95a09-2bcb-46a8-9c40-76bf81138926.jpg); background-repeat: no-repeat;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <div class="banner-heading">
                        <h1 class="banner-title">FAQ</h1>
                        <ol class="breadcrumb">
                            <li><a href="/">Home</a></li>
                            <li>FAQ</li>
                        </ol>
                    </div>
                </div>
                <!-- Col end-->
            </div>
            <!-- Row end-->
        </div>
        <!-- Container end-->
    </div>

    <section class="main-container mrt-40 faq" id="main-container">
        <div class="container mrb-40">
            <div id="verticalTab">
                <ul class="resp-tabs-list">

                    <li>
                        <img src='Content/uploads/Icon/general.png' alt='' aria-hidden='true' /><span>General PMS</span></li>
                    <li>
                        <img src='Content/uploads/Icon/about.png' alt='' aria-hidden='true' /><span>About Alchemy Capital</span></li>
                    <li>
                        <img src='Content/uploads/Icon/product.png' alt='' aria-hidden='true' /><span>Our Products</span></li>
                    <li>
                        <img src='Content/uploads/Icon/account_opening.png' alt='' aria-hidden='true' /><span>Account Opening </span></li>
                    <li>
                        <img src='Content/uploads/Icon/servicing.png' alt='' aria-hidden='true' /><span>Account Manangement</span></li>

                </ul>
                <div class="resp-tabs-container">


                    <div>
                        <h3>General PMS</h3>
                        <h2 class='accordionHead active'>What is Portfolio Management Services (PMS)? </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>A Portfolio Management Service is a platform created especially for high net-worth individuals to provide customized solutions for their financial investment needs. Investment management solutions in a PMS can be provided in three ways – Discretionary Portfolio, Non-Discretionary Portfolio, Advisory Portfolio. Portfolio Management Services are regulated by Securities and Exchange Board of India (SEBI) under PMS Regulations.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What are the different types of Porfolio Management Services (PMS) available for an investor?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><b>Discretionary</b> : Under these services, the choice as well as the timings of the investment decisions rest solely with the Portfolio Manager. </span>
                            </li>
                            <li class='newsData'><span><b>Non-Discretionary</b> : Under these services, the portfolio manager only suggests the investment ideas. The choice as well as the timings of the investment decisions rest solely with the Investor. However, the execution of trade is done by the portfolio manager.</span>
                            </li>
                            <li class='newsData'><span><b>Advisory</b> : Under these services, the portfolio manager only suggests the investment ideas. The choice as well as the execution of the investment decisions rest solely with the Investor.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the minimum investment for opening a PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>As per regulation, the minimum ticket size applicable at the time of opening a new account is Rs. 50 lakhs. Clients can bring in funds /securities to meet the minimum investment criteria.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Who can sign up for a PMS?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>The following investors are eligible to invest through PMS:<br />
                                - Resident Individuals<br />
                                - Hindu Undivided Families (HUF)<br />
                                - Body Corporates (Private / Public)<br />
                                - Limited Liability Partnership (LLP)<br />
                                - Foreign Portfolio Investor (FPI)<br />
                                - Private Trust
                                <br />
                                - Non-Resident Indians (NRI)<br />
                                - Partnership Firms or any other eligible investor<br />
                            </span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Is a demat account required for opening a PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>For investment in listed securities, an investor is required to open a new demat account in his/her own name, because the Portfolio stocks are held in the clients demat account.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the meaning of 'High Water Mark'?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>'High Water Mark' is the higher of either 'corpus investment value' or 'highest NAV at which fees has been paid historically'. Illustration of how the High-Water Mark would work: A client's initial contribution is Rs 1,00,00,000 which then rises to Rs 1,25,00,000 in its first year. Therefore, a performance fee would be payable on the Rs 25,00,000 return. Next year, the portfolio value drops to Rs 110,00,000. Therefore, no performance fee is payable. In the third year, the portfolio value rises to Rs 1,40,00,000. Performance fee is payable only on the profit which is in excess of the previously achieved high watermark ie of Rs 1,25,00,000 less performance fee (including taxes).</span>

                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the meaning of 'No Catch Up'? 
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>No catch-up means that profit share will be applicable only on the incremental return over and above the hurdle rate. For example if the value of a portfolio increases from Rs 100 to Rs 120 during a year when the fees structure included a hurdle rate of 8% (i.e. hurdle rate was at Rs 108), then the profit share will be applicable only on Rs (120-108) = Rs 12, rather than it being applicable on the entire Rs 20 profit delivered.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>How is TWRR different from XIRR?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><b>TWRR or Time-Weighted Rate of Return</b> is a measure of the compound growth of an investment irrespective of money flows. In order to calculate TWRR an investor needs to know when investment contributions or withdrawals were made, how much they were, and where the portfolio was valued at the time. Returns must be calculated for each period between contributions or withdrawals and then all the periods are multiplied together to get the compounding effect of the return. If the investment is for more than one year, the geometric mean of the annual returns is taken to find the time-weighted rate of return for the measurement period. Beginning value is the account value at the beginning of a set period. Ending value is the account value at the end of a set period. A simple rate of return is calculated by subtracting the beginning value from the ending value and then dividing by the beginning value.</span>
                            </li>
                            <li class='newsData'><span><b>XIRR or extended internal rate of return</b> is a measure of return which is used when multiple investments have been made at different points of time in a financial instrument. It is a single rate of return when applied to all transactions (investments and redemptions) would give the current rate of return.</span>
                            </li>
                            <li class='newsData'><span><b>TWRR</b> is the methodology prescribed by SEBI and is used by the investment industry to measure the performance of funds investing in publicly traded securities.  By contrast, IRR is normally used to gauge the return of funds that invest in illiquid, non-marketable assets-such as buyout, venture or real estate funds.</span>
                            </li>
                            <%--   <li class='newsData'><span>Managers of public securities funds typically do not control investor cash flows. Investors in these funds enter and exit at will. On the other hand, investors in many private or alternative funds face restrictions on their ability to invest additional assets or to redeem existing assets. These restrictions can take the form of multi-year "lock-ups" or no ability to achieve liquidity absent the sale of underlying assets.</span> 
                            </li>--%>
                        </ul>
                        <h2 class='accordionHead'>PMS is governed by which regulatory body?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>The Securities Exchange Board of India (SEBI) is the regulatory body for PMS.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Is Income from PMS taxable?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Yes, Income from PMS shall be taxable in the same manner as if the investor had dealt with the investments directly. However, the investor is well advised to consult his/her tax advisor for the same.</span>
                            </li>
                        </ul>
                        <%--<h2 class='accordionHead'>What is an AIF (Alternate Investment Fund)?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span></span> 
                            </li>
                        </ul>--%>
                    </div>
                    <div>
                        <h3>About Alchemy Capital</h3>
                        <h2 class='accordionHead active'>Who are the custodians of Alchemy Capital? </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Alchemy Capital has 2 Custodians : ICICI Bank and HDFC bank</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What are the fee options for Alchemy Capital products?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Alchemy Capital has a Fixed Fee option and a Hybrid Fee model (Where there is a Fixed Fee charge plus a Variable Fee charge over a Hurdle).</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Is there a lock in period or an exit load to be paid when closing the PMS account?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>There is no lock-in period. However, an exit load of 1% will be levied if the investment is redeemed within 1 year of opening the account.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the Disclosure Document?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>The Portfolio Manager provides the <a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132478220119620140.pdf" class="d-inline-block p-0" target="_blank">Disclosure Document</a> prior to entering into an agreement with the client. The Disclosure Document contains the product features/details of investment approaches, quantum and manner of payment of fees payable by the client for each activity, portfolio risks, complete disclosures in respect of transactions with related parties, the performance of the Portfolio Manager and the financial performance of the portfolio manager for the immediately preceding three years.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Where can I find the Disclosure Document for PMS strategies offered by Alchemy Capital?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132478220119620140.pdf" class="d-inline-block p-0" target="_blank">Disclosure document</a> is available under the regulatory information in the 'Our Products' section. </span>
                            </li>
                        </ul>

                    </div>

                    <div>
                        <h3>Our Products</h3>
                        <h2 class='accordionHead active'>What are the different PMS products Offered by Alchemy Capital?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" class="d-inline-block p-0" target="_blank">Alchemy High Growth</a>, <a href="https://www.alchemycapital.com//portfolio-management-services/alchemy-high-growth-select-stock.aspx" class="d-inline-block p-0" target="_blank">Alchemy High Growth Select Stock</a>, <a href="https://www.alchemycapital.com//portfolio-management-services/alchemy-leaders.aspx" class="d-inline-block p-0" target="_blank">Alchemy Leaders</a> & <a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" class="d-inline-block p-0" target="_blank">Alchemy Ascent</a></span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is Alchemy Ascent?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" class="d-inline-block p-0" target="_blank">Alchemy Ascent</a> a multi-cap fund endeavours to provide a consistent "high alpha" investment strategy which build portfolios to deliver consistent outperformance over the long term, using an objective, back tested and data driven approach devoid of any biases.</span>
                            </li>
                        </ul>
                        <%-- <h2 class='accordionHead'>What is Alchemy Leaders Of tomorrow?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span> Alchemy Leaders of Tomorrow, a cat III open ended AIF,   aims at capital appreciation over the long run by investing in listed equities and selectively (upto 10% of the portfolio) in PIPES/ IPO's/QIP's</span> 
                            </li>
                        </ul>--%>
                        <h2 class='accordionHead'>What is Alchemy Leaders?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-leaders.aspx" class="d-inline-block p-0" target="_blank">Alchemy Leaders</a>, with its large-cap bias, endeavours to invest in high quality established companies that have had a demonstrable track record of running a scalable profitable business at the right inflection point on its growth path and may have successfully navigated several adverse business cycles.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is Alchemy High Growth?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" class="d-inline-block p-0" target="_blank">Alchemy High Growth</a> adopts a multi-cap investment strategy and seeks to identify high quality growth that can deliver superior risk-adjusted returns.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is Alchemy High Growth Select Stock?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth-select-stock.aspx" class="d-inline-block p-0" target="_blank">Alchemy High Growth Select Stock</a> generates superior long-term returns by investing in high conviction concentrated stocks around 8 to 12 across sectors, with a focus on mid-cap companies.</span>
                            </li>
                        </ul>

                    </div>

                    <div>
                        <h3>Account Opening</h3>
                        <h2 class='accordionHead active'>How do I sign up for a PMS account with Alchemy Capital?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>You can write to us at <a href="mailto:contactus@alchemycapital.com" class="d-inline-block p-0">contactus@alchemycapital.com</a> or call us on +91 022 6617 1700.  Our representatives will connect with you and guide you with the process.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>How long does it take to set up a PMS account for Investing?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Current account opening time taken in general are listed below. The timelines  may take  little longer due to disruptions caused by Covid. Also note that the ticker starts from the day completed, signed, KRA compliant & discrepancy free application pack is received by Alchemy Capital. Any deficiencies or discrepancies will result in delays.<br />
                                ICICI Bank (Resident Indians) - 2 days (T+1)<br />
                                HDFC Bank (Resident Indians) - 4 days (T+3)</span>
                            </li>

                        </ul>
                        <h2 class='accordionHead'>What is the account opening process for Alchemy Capital PMS?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>The account opening process for Resident Individuals is different from Resident Non Individuals & Non Resident Indians.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the account opening process for Resident Individuals?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>If
                                (i) You are a resident individual
                                (ii) your KRA & CKYC is registered
                                (iii) your mobile number is linked with your Aadhar card, then
                                you can be Digitally Onboarded, all you need to give us is:<br />
                                - Your email consent allowing us to fetch your KRA details<br />
                                - Your Pancard, Aadhar Card and Address proof<br />
                                - Client Information form<br />
                                - Cancelled Cheque and funding details<br />
                                - Power of Attorney (POA)<br />
                            </span>
                                <i>Note - For the detailed process please contact the Client Servicing team.</i>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What are the Operating charges/expenses for Alchemy Capital PMS account?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Alchemy Capital has 2 Custodians : ICICI Bank and HDFC bank. All <a href="https://www.alchemycapital.com/content/uploads/Pdf/Pdf--132767733350263893.pdf" class="d-inline-block p-0" target="_blank">Operating charges/expenses</a> are at actuals. Refer to the <a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132478220119620140.pdf" class="d-inline-block p-0" target="_blank">Disclosure Document</a> for product specific details.</span>
                            </li>
                        </ul>

                        <h2 class='accordionHead'>I already have a Demat account will I have to open a separate demat  for PMS? Why?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Yes, a separate demat account be opened for  Portfolio Management Services (PMS). Portfolio Manager takes Power of Attorney (PoA) on this account so that they can operate the account on investor's behalf to settle securities etc.<br />
                                Having a separate demat account also allows investors to keep and operate their personal investments separately.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>I am an existing investor in Alchemy Capital PMS. Will I have to open a new Demat for investing in a different Alchemy Capital strategy?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>We do not need a separate demat for an existing investor wishing to invest in new strategy. We will use the same demat account. This reduces paperwork and hence, the new strategy account can be set-up much faster.</span>
                            </li>
                        </ul>

                        <h2 class='accordionHead'>Is there a direct on-boarding option available?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>All clients have an option to invest in our products/investment approaches directly, without involving the distribution services.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Can we apply for a PMS account digitally? 
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Yes, one can apply for a PMS account digitally provided the KYC is updated on KRA & CKYC portals.  This facility is currently available to resident individuals only.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Who is eligible to open a PMS account with Alchemy Capital?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Individuals and Non-Individuals such as HUFs, partnerships firms, sole proprietorship firms, Body Corporate etc. can invest in PMS with a minimum ticket size of INR 50 lakhs.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Can an NRI hold a PMS account? 
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>NRIs can invest in the PMS through the NRE or NRO accounts.There are some additional compliance/documentation requirements for NRI clients.Our relationship manager will help the NRI client with this documentation.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>How can I associate with Alchemy Capital as a channel partner? 
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>You can write to us at <a href="mailto:partners@alchemycapital.com" class="d-inline-block p-0">partners@alchemycapital.com</a>. Our representatives will connect with you and guide you with the process.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What are the accounts opened for an NRI to hold a PMS account with Alchemy Capital?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>NRI can invest through Non-repatriable (NRO) basis or Repatriable (NRE) basis. For investments in PMS, an NRI may require to open separate Bank Account, Demat account and Broking account.</span>
                            </li>
                        </ul>
                        <%--            <h2 class='accordionHead'>Is there a lock in period?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span></span> 
                            </li>
                        </ul>
                        --%>
                        <h2 class='accordionHead'>Can we open the account in the name of Minor?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>PMS account cannot be opened for a Minor.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Can we accept USA base client investment?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>We have currently restriction to accept USA based clients in PMS and AIF. However, we can get you connected with our offshore team if any investor wish to invest.</span>
                            </li>
                        </ul>

                    </div>

                    <div>
                        <h3>Account Manangement</h3>
                        <h2 class='accordionHead'>How do I connect with your customer servicing team for my account details?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Client sourced by channel partner can write to us at <a href="mailto:clientservicing@alchemycapital.com" class="d-inline-block p-0">clientservicing@alchemycapital.com</a>.</span></li>
                            <li class='newsData'><span>Client sourced directly can write to us at <a href="mailto:clientsupport@alchemycapital.com" class="d-inline-block p-0">clientsupport@alchemycapital.com</a>.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>I would like to connect with my RM?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Client sourced by channel partner can write to us at <a href="mailto:clientservicing@alchemycapital.com" class="d-inline-block p-0">clientservicing@alchemycapital.com</a>.</span></li>
                            <li class='newsData'><span>Client sourced directly can write to us at <a href="mailto:clientsupport@alchemycapital.com" class="d-inline-block p-0">clientsupport@alchemycapital.com</a>.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>How do I access my PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Please login into your client portal on our website <a href="https://www.alchemycapital.com" class="d-inline-block p-0">https://www.alchemycapital.com</a> with your login credentials.</span></li>
                            <li class='newsData'><span>For assistance,<br />
                                - please write to us at <a href="mailto:clientsupport@alchemycapital.com" class="d-inline-block p-0">clientsupport@alchemycapital.com</a> (Direct clients).<br />
                                - please write to us at <a href="mailto:clientservicing@alchemycapital.com" class="d-inline-block p-0">clientservicing@alchemycapital.com</a> (Channel clients).</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Can I fund my PMS account by transferring Stocks?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>In general, you can fund your PMS account by transferring stocks. The stocks will be liquidated and proceeds will be invested. However, we recommend to consult with our documentation team before transferring stocks.</span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the process for transferring stocks to the PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'>
                                <span>- In general, you can fund your PMS account by transferring stocks. The stocks will be liquidated and proceeds will be invested. However, we recommend to consult with our documentation team before transferring stocks.<br />
          <%--                          - Please note that Alchemy Capital can accept only listed and liquid securities. Hence, before transferring, please send us the list of stocks are we will confirm if any of these cannot be transferred in.<br />
                                    - Once we have approved the list, you can transfer the approved stocks to the Alchemy Capital PMS demat account.<br />
                                    - Please let us know once the transfer has been executed.<br />
                                    - We will email you a confirmation of receipt of stocks and will activate your PMS account.<br />
                                    - We recommend that you give us the Acquisition date and rate at which these stocks were purchased. This will enable us to give you the taxable Gain/Loss.--%>
                                </span>                               
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What are the Bank details for funding the PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>HDFC<br />
                                Account Name - Alchemy Capital Management Pvt. Ltd - PMS Pool account<br />
                                Account Number - 50200024011691<br />
                                IFS code - HDFC0000060<br />
                                Branch - Fort, Nanik Motwani Marg
                            </span>
                            </li>
                            <li class='newsData'><span>ICICI<br />
                                Account Name - Alchemy Capital Management Pvt. Ltd - PMS Pool account<br />
                                Account Number - 000405074120<br />
                                IFS code - ICIC0000004<br />
                                Branch - Free Press House, Nariman Point, Mumbai
                            </span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>How can I top up more funds to my account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>You can transfer the funds and send an email to the Servicing email ID with
                                <br />
                                (a) Proof of transfer i.e. cheque / bank statement<br />
                                (b) Standard "Transaction slip for Additonal Investment", where you need to specify  the name of the strategy in which you want to invest.<br />
                                The Client Servicing team will confirm the receipt of funds once funds reflect in PMS bank account.<br />
                            </span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>Where can I find my debit note for fees?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Log into <a href="https://client.alchemycapital.com/login_alchemy.aspx" class="d-inline-block p-0" target="_blank">Client Portal</a>, you will find this under Statements → Debit Note</span>
                            </li>
                        </ul>
                        <%--  <h2 class='accordionHead'>Where can I find my certified audited financials?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>Log into Client Portal, you will find this under Statements → Audited Financial Statements</span> 
                            </li>
                        </ul>--%>
                        <h2 class='accordionHead'>How do I withdraw / redeem part of my investments from PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'>
                                <span>- Redeeming part of your holdings is easy and does not require any paperwork. All we need is an email with the "Transaction Slip for Partial redemption", sent to us at <a href="mailto:clientsupport@alchemycapital.com" class="d-inline-block p-0">clientsupport@alchemycapital.com</a> (Direct clients) or on <a href="mailto:clientservicing@alchemycapital.com" class="d-inline-block p-0">clientservicing@alchemycapital.com</a> (Channel clients) from client's registered email id with Alchemy Capital.
                                    <br />
                                    - Requests received by Alchemy Capital by 5:30 PM will be processed on T+1 and requests received after 5:30 PM will be processsed on T+2.
                                <br />
                                    - Applicable fees and charges (Fixed fees, and / or Performance fee & Exit Load (if any)) on redemption amount will be computed and charged to PMS account on redemption date. Such fees will be recovered from the cash proceeds from your PMS account.
                          <br />
                                    - There is no min/max on redemption amount, however as per regulatory requirement, the portfolio value post redemption amount on the day of redemption request should be at-least 50 lakhs (or the minimum investment amount defined by the portfolio manager for a particular strategy, whichever is higher). Hence the partial redemption amount would be subjected to this criteria.
                                </span>
                                <i>Note - For the detailed process please contact the Client Servicing team </i>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>When and how often can I change my fee plan?
                        </h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>You can change your fee plan once every quarter. However, all requests for change are processed on the 1st  day of the next quarter.
                            </span>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the process to close my PMS account?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'>
                                <span>- We would be sad to see you go. However, if you do decide to close your PMS account, to initiate the redemption, we will need<br />
                                    (a) An email with the "Transaction Slip for Full redemption", via Funds or Stock transfer from the client's registered email id with Alchemy Capital.<br />
                                    (b) POA revocation, closure of Demat account and Bank account. (if any)<br />
                                    (c) In case of NRI's we will need additional docs such as PI cancellation & Closure of trading account.
                           <br />
                                    - Requests received by Alchemy Capital by 5:30 PM will be processed on T+1 and requests received after 5:30 PM will be processsed on T+2. 
                            <br />
                                    - We will acknowledge the receipt and notify the Portfolio Manager to initiate liquidation of your portfolio. If you have requested stock transfer, we will ask you for the CML/CMR of your demat account where you would like to transfer our securities.
                           <br />
                                    - Your portfolio will be liquidated / transferred out within next few working days.
                         <br />
                                    - We will compute fees and charges as applicable and debit from your account.
                            <br />
                                    - Sale proceeds and any cash balances will be remitted to your bank account registered with Alchemy Capital.
                           <br />
                                    - We will confirm the account closure and remittance of funds in email.
                                </span>
                                <i>Note - For the detailed process please contact the Client Servicing team.</i>
                            </li>
                        </ul>
                        <h2 class='accordionHead'>What is the minimum amount of redemption?</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>You can redeem any amount of money from your PMS account. However, post redemption, the portfolio value should not fall below SEBI mandated minimum investment ie. 50 lakhs or the minimum investment amount defined by the portfolio manager for a particular strategy, whichever is higher.</span>
                            </li>
                        </ul>
                        <%--         <h2 class='accordionHead'>Diff between Portfolio Gain/Loss and Taxable Gain Loss</h2>
                        <ul class='newsLetter'>
                            <li class='newsData'><span>When a PMS account is funded by stocks, we also compute the taxable gain/loss incurred by client on stock holdings before the stocks were 'taken-over' by Alchemy. This is computed by comparing sale value with original purchase value. The Gain/loss prior to takeover are captured by providing taxable gain/loss report .</span> 
                            </li>
                        </ul>--%>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>


    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />
</asp:Content>

