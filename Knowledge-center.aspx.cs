﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class KnowledgeCenter : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindKnowledgeCenter();
            BindInnerBanner(9);
        }
        
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }


    public void BindKnowledgeCenter()
    {
        int Id = 1;
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'bindYear',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            int count = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (count == 1)
                {
                    strbuild.Append("<h2 class='accordionHead active'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + dr["Year"].ToString() + "</h2>");
                    count++;
                }
                else
                {
                    strbuild.Append("<h2 class='accordionHead'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + dr["Year"].ToString() + "</h2>");
                }

                strbuild.Append("<ul class='newsLetter'>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_Insights 'bindYearAndContent',0,'" + Id + "','" + dr["Year"].ToString() + "'");
                if (dt1.Rows.Count > 0)
                {
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        strbuild.Append("<li class='newsData'>");
                        if (dr1["Type"].ToString() == "Pdf")
                        {
                            strbuild.Append("<a href='Content/uploads/Pdf/" + dr1["Pdf"].ToString() + "' target='_blank'>");
                        }
                       else if (dr1["Type"].ToString() == "Image")
                        {
                            strbuild.Append("<a href='Content/uploads/MediaImages/" + dr1["MediaImages"].ToString() + "' target='_blank'>");
                        }
                        else
                        {
                            strbuild.Append("<a href='" + dr1["Url"].ToString() + "' target='_blank'>");
                        }

                        strbuild.Append("<div class='newsDate'><i class='fa fa-file-pdf-o'aria-hidden='true'></i></div>");
                        strbuild.Append("<span><strong>" + dr1["Title"].ToString() + "</strong></span>");
                        strbuild.Append("</a>");
                        strbuild.Append("</li>");
                    }
                }

                strbuild.Append("</ul>");
            }
            
        }
        ltrBindKnowledge.Text = strbuild.ToString();

    }
}