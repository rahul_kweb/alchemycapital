﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="request_call_back_landing.ascx.cs" Inherits="request_call_back_landing" %>



<div class="formArea" id="request_call_back_landing">

    <div class="formHolder text-white mx-auto mb-4 mb-lg-0">
        <div class="formMain">

            <div class="formTitle font-weight-bold mb-3">Request a Callback</div>
            <asp:Label ID="lblMsg" runat="server"></asp:Label>
            <form id="formrequest" runat="server" class="formBox">

                <asp:HiddenField ID="hdnutm_source" runat="server" />
                <asp:HiddenField ID="hdnutm_medium" runat="server" />
                <asp:HiddenField ID="hdnutm_campaign" runat="server" />
                <asp:HiddenField ID="hdnutm_device" runat="server" />
                <asp:HiddenField ID="hdnutm_content" runat="server" />
                <asp:HiddenField ID="hdnutm_term" runat="server" />
                 <asp:HiddenField ID="hdnutm_referrer" runat="server" />

                <asp:HiddenField ID="hdnreferrer" runat="server" />

                <div class="form-group">
                    <asp:TextBox ID="txtName" CssClass="form-control rounded-0" placeholder="Name" autocomplete="off" runat="server"></asp:TextBox>
                    <span id="ErrorName1" class="errorMsg">Name is required.</span>
                </div>

                <div class="form-group">
                    <asp:TextBox ID="txtPhone" CssClass="form-control rounded-0" placeholder="Mobile No." runat="server" autocomplete="off" MinLength="10" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                    <span id="ErrorPhone1" class="errorMsg">Mobile No. is required.</span>
                </div>

                <div class="form-group">
                    <asp:TextBox ID="txtEmail" CssClass="form-control rounded-0" autocomplete="off" placeholder="Email" runat="server"></asp:TextBox>
                    <span id="ErrorEmail3" class="errorMsg">Email is required.</span>
                    <span id="ErrorEmail4" class="errorMsg">Invalid email address.</span>
                </div>

                <div class="form-group">
                    <asp:TextBox ID="txtCity" CssClass="form-control rounded-0" placeholder="City" autocomplete="off" runat="server"></asp:TextBox>
                    <span id="ErrorCity" class="errorMsg">City is required.</span>
                     <span id="ErrorCity1" class="errorMsg">Please enter a valid City.</span>
                </div>

                <div class="form-group">
                    <asp:DropDownList ID="ddlIntrest" runat="server" CssClass="form-control rounded-0">
                        <asp:ListItem Value="I am Interested in Investing">I am Interested in Investing</asp:ListItem>
                        <asp:ListItem Value="I am Interested in IFA / Channel Partner Relation">I am Interested in IFA / Channel Partner Relation</asp:ListItem>
                           <asp:ListItem Value="Others">Others</asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="custom-control custom-checkbox consentStmt mb-2">
                    <input type="checkbox" class="custom-control-input" id="check">
                    <label class="custom-control-label" for="check">I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.</label>
                    <span id="errorcheck" class="errorMsg">*</span>
                </div>

                <p class="consentStmt font-weight-medium mb-3">As per SEBI guidelines, minimum investment required is of &#8377;50 Lakhs</p>

                <asp:Button ID="btn_Req_Submit" CssClass="btn rounded-pill" Text="Submit" runat="server" OnClick="btn_Req_Submit_Click" OnClientClick="javascript:return Checkfields();" Style="padding: 10px 25px; width: auto; color: white" />

            </form>

        </div>
        <div class="formBottom d-flex">
            <div class="col strip"></div>
            <div class="col-auto fold"></div>
        </div>
    </div>

</div>

<style>
    .ErrorValidation {
        display: none;
        color: #dc3545 !important;
        margin-bottom: 10px;
    }
</style>


<script>
    ///  store referrer value

    document.getElementById("<%=hdnreferrer.ClientID%>").value = document.referrer;

</script>

<script>

      function isNumberKey1(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;
        var textBox = document.getElementById('<%=txtPhone.ClientID%>').value.length;
        if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
            return false;

        return true;
      }

    function Checkfields() {

        var Name1 = $('#<%=txtName.ClientID%>').val();
            var Phone1 = $('#<%=txtPhone.ClientID%>').val();
            var Email1 = $('#<%=txtEmail.ClientID%>').val();
            var City = $('#<%=txtCity.ClientID%>').val();

            var Blank1 = false;
            var message = "";

            if (Name1 == '') {
                $('#ErrorName1').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#ErrorName1').css('display', 'none');

            }


            if (Phone1 == '') {
                $('#ErrorPhone1').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#ErrorPhone1').css('display', 'none');
            }


            if (Email1 == '') {
                $('#ErrorEmail3').css('display', 'block');
                $('#ErrorEmail4').css('display', 'none');
                Blank1 = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email1)) {
                    $('#ErrorEmail3').css('display', 'none');
                    $('#ErrorEmail4').css('display', 'block');
                    Blank1 = true;
                }
                else {
                    $('#ErrorEmail4').css('display', 'none');


                }
                $('#ErrorEmail3').css('display', 'none');


            }

            if (City == '') {
                $('#ErrorCity').css('display', 'block');
                //$('#ErrorCity1').css('display', 'none');
                Blank1 = true;
            }
            else {

                //only alphabets allowed

                var cityvalid = /^[a-zA-Z]+$/;

                if (!cityvalid.test(City)) {
                    $('#ErrorCity').css('display', 'none');
                    $('#ErrorCity1').css('display', 'block');
                    Blank1 = true;
                }
                else {
                    $('#ErrorCity1').css('display', 'none');
                }

                $('#ErrorCity').css('display', 'none');
            }


            if (check.checked == false) {
                $('#errorcheck').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#errorcheck').css('display', 'none');
            }


            if (Blank1) {

                return false;

            }
            else {
                return true;
            }
        }
</script>
