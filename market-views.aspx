﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="market-views.aspx.cs" Inherits="markets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
      <title>Latest Market Views - Alchemy Capital Management</title>
   <meta name="description" content="Alchemy Capital Management - an asset management & PMS provider firm that gives you insights into the Indian market perspective. For more information, go here!"/>

    <link rel="canonical" href="https://www.alchemycapital.com/markets-views.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/market-views.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/market-views.aspx" hreflang="x-default" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Market Views</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                       <li>Insights</li>
                                    <li>Market Views</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->

    <section class="main-container mrt-40" id="main-container">
        <div class="container mrb-40">
            <asp:Literal ID="ltrMarkets" runat="server"></asp:Literal>
            </div>
                </section>
    <div class="gap-30"></div>
</asp:Content>

