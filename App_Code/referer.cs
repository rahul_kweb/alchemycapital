﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for referer
/// </summary>
public class referer : System.Web.UI.Page
{
    public referer()
    {
             
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        string domainName = HttpContext.Current.Request.Url.Host;

        if (Request.UrlReferrer != null)
        {
            string refererurl = Request.UrlReferrer.ToString();


            if (!refererurl.Contains("alchemycapital"))
            {
                HttpCookie nameCookiecook = new HttpCookie("referrer");
                nameCookiecook.Values["referrer"] = refererurl;
                Response.Cookies.Add(nameCookiecook);

            }
            
        }

    }
}