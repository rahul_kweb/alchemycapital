﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for ContactUsMail
/// </summary>
public class ContactUsMail
{
    //string strFrom = "ho_hr@patel-india.com";
    string Subject = "Contact Us";
    //string strMailUserName = "noreply@kwebmaker.com";
    //string strMailPassword = "noreply123";
    //string strMailUserName = "prl_autobilling@patel-india.com";
    //string strMailPassword = "EWG$212";

    string strMailUserName = "";
    string strMailPassword = "";


    public ContactUsMail()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public int SendEmail(string Body, string EmailTo)
    {

        int sent = 0;
        try
        {

            SmtpClient mailClient = null;
            MailMessage message = null;
            mailClient = new SmtpClient();
            message = new MailMessage();
            //mailClient.Host = "smtpout.secureserver.net";
            //mailClient.Port = 25;
            mailClient.Host = "mail.patel-india.com";
            mailClient.Port = 587;
            //mailClient.Host = "mail.icewarpcloud.in";
            //mailClient.Port = 25;




            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential(strMailUserName, strMailPassword);
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = SMTPUserInfo;
            mailClient.EnableSsl = false;
            mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            //string strFromMail = strFrom;
            string strFromMail = EmailTo;
           // string strFromMail = strMailUserName;
            MailAddress fromAddress = new MailAddress(strFromMail, "");
            message.From = fromAddress;
       
            message.Priority = System.Net.Mail.MailPriority.High;

            //emailaddress of house of patel 
            //message.To.Add("ho_hr@patel-india.com");
            //message.To.Add("contactus@patel-india.com");

            ///message.To.Add("contactus@patel-india.com");
            //message.To.Add("rahul@kwebmaker.com");
            //message.To.Add("balasaheb.phapale@patel-india.com");
            //message.To.Add("pandit.omkar@gmail.com");
            //message.To.Add("developer3@patel-india.com");






            message.Subject = Subject;
         


            message.Body = Body;
            message.IsBodyHtml = true;
            mailClient.Send(message);
            message = null;
            mailClient = null;

            sent = 1;

        }
        catch (Exception ex)
        {
            sent = 0;
        }

        return sent;
    }

    public bool EmailContactUs(string FirstName, string LastName, string JobTilte, string CompanyName, string City, string Pincode, string Address1, string Address2, string Email, string Mobile, string TimeToContact, string Industry, string Requirement, string StorageVolume, string ServiceProvider)
    {
        bool blnRetVal = false;
        try
        {
            System.Text.StringBuilder strEmailBuilder = new System.Text.StringBuilder();

            strEmailBuilder.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
            strEmailBuilder.AppendFormat("<tr> <td colsPAN=\"2\" align=\"center\" bgcolor=\"#0d1740\"><strong><font color=\"#FFFFFF\"> Contact Us Detail </font></strong></td></tr>");
            strEmailBuilder.AppendFormat("<tr><td width=\"200px\"><strong> First Name : </strong></td><td> " + FirstName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong> Last Name : </strong></td> <td> " + LastName + " </td> </tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Job Title : </strong></td> <td> " + JobTilte + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Company Name : </strong></td> <td> " + CompanyName + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + City + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Pincode : </strong></td> <td> " + Pincode + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Address1 : </strong></td> <td> " + Address1 + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Address1 : </strong></td> <td> " + Address2 + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + Email + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Mobile : </strong></td> <td> " + Mobile + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Time To Contact : </strong></td> <td> " + TimeToContact + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Industry : </strong></td> <td> " + Industry + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Your Requirement : </strong></td> <td> " + Requirement + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Storage Volume : </strong></td> <td> " + StorageVolume + " </td></tr>");
            strEmailBuilder.AppendFormat("<tr> <td><strong>Service Provider : </strong></td> <td> " + ServiceProvider + " </td></tr>");



            strEmailBuilder.Append("</table>");

            SendEmail(strEmailBuilder.ToString(),Email);


            blnRetVal = true;
        }
        catch (Exception ex)
        {
            blnRetVal = false;
        }
        catch
        {
            blnRetVal = false;
        }
        return blnRetVal;
    }
	
}