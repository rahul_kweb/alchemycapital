﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IPLocation
/// </summary>
public class IPLocation
{
    public string IPv4 { get; set; }
    public string country_code { get; set; }
    public string country_name { get; set; }
    public string postal { get; set; }
    public string state { get; set; }
    public string CityCode { get; set; }
    public string city { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
}