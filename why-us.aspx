﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="why-us.aspx.cs" Inherits="WhyUs" %>


<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Why Choose Us? - Alchemy Capital</title>
   <meta name="description" content="Alchemy Capital focuses on long-term wealth building for investors. We also assist investors in selecting the appropriate stocks and understanding current market norms."/>
  
    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/why-us.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/why-us.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/why-us.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/why-us.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Why Us</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li>Why Us</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->

    <section class="main-container mrt-40" id="main-container">
        <div class="container mrb-40">

            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
                    <%--    <li><i class="glyph-icon flaticon-growth-1" aria-hidden="true"></i> <span>Why Alchemy?</span></li>
            <li><i class="glyph-icon flaticon-team" aria-hidden="true"></i> <span>Founders and Investment Team</span></li>
            <li><i class="glyph-icon flaticon-column" aria-hidden="true"></i> <span>Strong Emphasis on Compliance and Risk Management</span></li>
            <li><i class="glyph-icon flaticon-plant" aria-hidden="true"></i> <span>Disciplined Investment Process</span></li>--%>
                </ul>

                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                    <%--<div>
<h3>Why Alchemy?</h3>
<p>Alchemy has an exemplary track record of having provided superior client experience, in keeping with the investor’s long-term wealth creation goals, over more than one and a half decades. With a collective experience of 100 years between its directors, aided by proficiency and meticulousness, Alchemy has lent its skills and advice to assets worth more than USD 1 billion over the years. Yet, Alchemy doesn’t rest on past laurels.</p>
<p>A dedicated team of professionals works round the clock to make sure that you benefit from the association. The exclusive Bottom-Up strategy and periodic monitoring make Alchemy what is it today; an asset management organization of choice. </p>
</div>

<div>
<h3>The founders and their men</h3>
<ul>
<li> Bring a century of equity market experience to the table.</li>
<li> Are on solid market footing, one that bases on knowledge, transparency and process.</li>
<li> Have a reliable network that converges both information and intelligence.</li>
<li> Are consistent with the investment patterns and philosophy.</li>
<li>Work with a team of professionals who have imbibed the investment philosophy practiced by the company.</li>
</ul>

</div>

<div>

<h3>Risk management and compliance</h3>
<ul>
<li> Alchemy abides by a strict code of conduct and ensures all the teams do that as well.</li>
<li> Believes in absolute transparency and unambiguity in the ways of doing business.</li>
</ul>


</div>

<div>

<h3>A disciplined investment philosophy</h3>

<ul>
<li> Alchemy are the pioneers in Bottom-Up stock selection whereby companies can perform and realize their full potential irrespective of industry conditions</li>
<li> A good and early judge of trends and latest market developments</li>
<li> Considers primary research inviolable for a comprehensive evaluation </li>
<li> Consistent and impeccable track record; initial investment of Rs.1 crore in 2002 has grown to Rs.26 crores in Alchemy High Growth product</li>
<li>Consistent performance track record since inception in 2002.</li>
</ul>

</div>--%>
                </div>
            </div>


            <div class="clearfix"></div>
        </div>

    <a class="to_bottom " id="callback" style="display: block;">
       <i class="fa fa-phone" aria-hidden="true"></i>
        <%--<button class="contact_us_btn" data-spy="affix" data-offset-top="10" title="Request a Callback"><i class="fa fa-phone" aria-hidden="true"></i></button>--%>
    </a>
    </section>
    <div class="gap-60"></div>

    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />


    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Why Us",
    "item": "https://www.alchemycapital.com/why-us.aspx"  
  }]
}
</script>


</asp:Content>

