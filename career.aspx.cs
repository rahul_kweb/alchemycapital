﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Career : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            BindInnerBanner(13);
            BindCareerOverview();
            BindCurrentOpening();

            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_CurrentOpening 'bindRole'");
            ddlCareer.DataSource = dt;
            ddlCareer.DataBind();
            ddlCareer.DataTextField = "Role";
            ddlCareer.DataValueField = "Role";
            ddlCareer.DataBind();

            ddlCareer.Items.Insert(0, new ListItem("Select Role :", "0"));
        }
    }



    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindCareerOverview()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS',6");
        if (dt.Rows.Count > 0)
        {
            ltrCareerOverview.Text = dt.Rows[0]["Description"].ToString();
        }
    }

    public void BindCurrentOpening()
    {

        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CurrentOpening 'bindOpening'");
        if (dt.Rows.Count > 0)
        {

            foreach (DataRow dr in dt.Rows)
            {
                strbuild.Append("<div class='col-md-4'>");
                strbuild.Append("<div class='opening_box_setup'>");
                strbuild.Append("<p><strong>Role :</strong> " + dr["Role"].ToString() + " </p>");
                strbuild.Append("<p><strong>Location :</strong> " + dr["Location"].ToString() + " </p>");
                strbuild.Append("<a href='#careerForm' onclick=\"RoleSelected('" + dr["Role"].ToString() + "')\">Apply Now <i class='fa fa-angle-right' aria-hidden='true'></i></a>");
                strbuild.Append("</div>");
                strbuild.Append("</div>");
            }


            ltrCrrentOprning.Text = strbuild.ToString();

        }
    }

    protected void btnCareerSubmit_Click(object sender, EventArgs e)
    {
        string VirtualFile = "~/Content/uploads/Resume/";
        string Img = string.Empty;
        Stream inputStr = null;
        if (CheckSaveCareer())
        {

            if (fileResume.HasFile)
            {
                string ext = Path.GetExtension(fileResume.FileName).ToLower();

                if (!utility.IsValidResumeFileExtension(ext))
                {
                    lblfileUploadResume.Visible = true;
                    lblfileUploadResume.Text = "Please upload files having extensions:.doc,.docx,.pdf";
                    lblfileUploadResume.ForeColor = System.Drawing.Color.Red;
                    return; // STOP FURTHER PROCESSING
                }
                else
                {
                    Img = utility.GetUniqueName(VirtualFile, "Resume", ext, this, false);
                    fileResume.SaveAs(Server.MapPath(VirtualFile + Img + ext));
                    inputStr = fileResume.PostedFile.InputStream;


                    using (SqlCommand cmd = new SqlCommand("Proc_ApplyForJob"))
                    {
                        using (WebClient client = new WebClient())
                        {
                            var json = client.DownloadString("https://geoip-db.com/json");
                            IPLocation location = new JavaScriptSerializer().Deserialize<IPLocation>(json);


                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@para", "add");
                            cmd.Parameters.AddWithValue("@FullName", txtName.Text.ToString().Trim());
                            cmd.Parameters.AddWithValue("@Email", txtCareerEmail.Text.ToString().Trim());
                            cmd.Parameters.AddWithValue("@Role", ddlCareer.SelectedItem.Text.Trim());
                            cmd.Parameters.AddWithValue("@ContactNo", txtContactNo.Text.ToString().Trim());
                            cmd.Parameters.AddWithValue("@Resume", Img + ext);
                            cmd.Parameters.AddWithValue("@CityName", location.city);
                            cmd.Parameters.AddWithValue("@Region", location.state);
                            cmd.Parameters.AddWithValue("@CountryName", location.country_name);

                            if (utility.Execute(cmd))
                            {
                                StringBuilder sb = new StringBuilder();

                                sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                                sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Apply For Job </font></strong></td></tr>");
                                sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + txtName.Text.ToString().Trim() + " </td></tr>");
                                sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + txtCareerEmail.Text.ToString().Trim() + " </td></tr>");
                                sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Phone : </strong></td> <td> " + txtContactNo.Text.ToString().Trim() + " </td> </tr>");
                                sb.AppendFormat("<tr> <td><strong>Role : </strong></td> <td> " + ddlCareer.SelectedItem.Text.Trim() + " </td></tr>");
                                sb.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + location.city + " </td></tr>");
                                sb.AppendFormat("<tr> <td><strong>Region : </strong></td> <td> " + location.state + " </td></tr>");
                                sb.AppendFormat("<tr> <td><strong>Country : </strong></td> <td> " + location.country_name + " </td></tr>");
                                sb.Append("</table>");

                                //string ToEmailid = ConfigurationManager.AppSettings["CareerEmail"].ToString();
                                //String[] emailid = new String[] { ToEmailid };

                                if (ddlCareer.SelectedItem.Text.Trim() == "Administration")
                                {
                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailAdministration"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("ThankYou.aspx");
                                }
                                else if (ddlCareer.SelectedItem.Text.Trim() == "Client Servicing")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailClientServicing"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Compliance")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailCompliance"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Corporate Accounts")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailCorporateAccounts"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Information Technology")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailInformationTechnology"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Operations")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailOperations"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Product")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailProduct"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else if (ddlCareer.SelectedItem.Text.Trim() == "Relationship Managers")
                                {

                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailRelationshipManagers"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                else
                                {
                                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmailResearchFundManagement"].ToString();
                                    String[] emailid = new String[] { ToEmailid };
                                    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                    Response.Redirect("thankyou_career.aspx");

                                }

                                //    utility.SendEmail(sb.ToString(), emailid, "Apply for job with us", Img + ext, inputStr);
                                //Response.Redirect("ThankYou.aspx");
                                Reset();

                            }

                        }
                    }


                }
            }
            else
            {

                lblfileUploadResume.Visible = true;
                lblfileUploadResume.Text = "Resume is Required";
                return;


            }
        }
    }

    private void Reset()
    {
        txtName.Text = "";
        txtCareerEmail.Text = "";
        txtContactNo.Text = "";
        ddlCareer.SelectedValue = "0";

    }



    public bool CheckSaveCareer()
    {
        bool isOK = true;
        string message = string.Empty;

        string emailCareer = txtCareerEmail.Text;
        Regex regexemailCareer = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match matchemailCareer = regexemailCareer.Match(emailCareer);


        string PhoneCareer = txtContactNo.Text;
        Regex regexCareer = new Regex("^[1-9]{1}[0-9]{0,11}$");
        Match matchphoneCareer = regexCareer.Match(PhoneCareer);

        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Full Name, ";
        }

        if (txtCareerEmail.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Email, ";
        }
        if (!matchemailCareer.Success)
        {
            isOK = false;
            message += "Valid Email, ";
        }

        if (txtContactNo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Phone, ";
        }

        if (!matchphoneCareer.Success)
        {
            isOK = false;
            message += "Valid Phone no, ";
        }


        if (ddlCareer.SelectedItem.Text == "Select Role")
        {
            isOK = false;
            message += "Select Role, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CheckSave", "alert('Please fill following fields')", true);
            lblMsgcareer.Text = "Please fill following fields <br />" + message;
            lblMsgcareer.ForeColor = System.Drawing.Color.Red;
        }
        return isOK;
    }

}