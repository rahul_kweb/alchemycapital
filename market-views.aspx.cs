﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class markets : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMarkets();
            BindInnerBanner(9);
        }

    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }


    public void BindMarkets()
    {
        int Id = 6;
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'bindYear',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            int count = 1;
            foreach (DataRow dr in dt.Rows)
            {
                if (count == 1)
                {
                    strbuild.Append("<h2 class='accordionHead active'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + dr["Year"].ToString() + "</h2>");
                    count++;
                }
                else
                {
                    strbuild.Append("<h2 class='accordionHead'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + dr["Year"].ToString() + "</h2>");
                }

                strbuild.Append("<ul class='newsLetter'>");

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_Insights 'BindMarkets',0,'" + Id + "','" + dr["Year"].ToString() + "'");
                if (dt1.Rows.Count > 0)
                {
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        strbuild.Append("<li class='newsData'>");
                        if (dr1["Type"].ToString() == "Pdf")
                        {
                            strbuild.Append("<a href='Content/uploads/Pdf/" + dr1["Pdf"].ToString() + "' target='_blank'>");
                        }
                        else if (dr1["Type"].ToString() == "Image")
                        {
                            strbuild.Append("<a href='Content/uploads/MediaImages/" + dr1["Image"].ToString() + "' target='_blank'>");
                        }
                        else
                        {
                            strbuild.Append("<a href='" + dr1["Url"].ToString() + "' target='_blank'>");
                        }



                        strbuild.Append("<div class='newsDate'>" + dr1["PostDate"].ToString() + "</div>");
                        strbuild.Append("<span><strong>" + dr1["Title"].ToString() + "</strong></span>");
                        strbuild.Append("</a>");
                        strbuild.Append("</li>");
                    }
                }

                strbuild.Append("</ul>");
            }

        }
        ltrMarkets.Text = strbuild.ToString();

    }
}