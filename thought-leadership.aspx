﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="thought-leadership.aspx.cs" Inherits="thought_leadership" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Thought Leadership | Get Insights By Industry Experts - Alchemy Capital</title>
   <meta name="description" content="Thought Leadership page helps you to track all updates related to the asset management system by the Fund Management experts at Alchemy Capital."/>
  
    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/thought-leadership.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/thought-leadership.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/thought-leadership.aspx" hreflang="x-default" />


</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h1 class="banner-title">Thought Leadership</h1>
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><a href="javascript:void(0);">Insights</a></li>
                        <li>Thought Leadership</li>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
                </ItemTemplate>
    </asp:Repeater>
<!-- Banner area end-->

<section class="main-container mrt-40" id="main-container">

    <div class="container">
      <%--  <h2 class="column-title title-small text-center">Thought Leadership</h2>--%>
<asp:Literal ID="ltrOverview" runat="server"></asp:Literal>       

        <!-- Title row end-->
        <div class="row">
           <asp:Literal ID="ltrBlog" runat="server"></asp:Literal> 
        </div>
    </div>

</section>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thought Leadership",
    "item": "https://www.alchemycapital.com/thought-leadership.aspx"  
  }]
}
</script>


</asp:Content>

