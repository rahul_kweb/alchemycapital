﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="what-we-offers.aspx.cs" Inherits="WhatWeOffers" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Portfolio Management Services in India - AIF - Alchemy Capital</title>
   <meta name="description" content="PMS Services - Alchemy Capital is leading Portfolio Management company in India, who help investors for PMS investments & to build portfolio for equities in large cap, mid cap & low cap companies."/>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("BannerImage") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Portfolio Management & AIF Services </h1>

                                <ol class="breadcrumb">
                                    <li><a rel="canonical" href="/">Home</a></li>
                                    <li><a href="what-we-offer.aspx">Our Products</a></li>
                                    <li><%#Eval("Title") %></li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">


        <div class="container">
            <asp:HiddenField ID="hdnId" runat="server" />

            <h2 class="column-title title-small text-center">
                <asp:Literal ID="ltrPageName" runat="server"></asp:Literal><small class="subtitle"> </small></h2>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTabHeading" runat="server"></asp:Literal>
                    <%--<li><i class="glyph-icon flaticon-money" aria-hidden="true"></i> <span>Investment Objective</span></li>
<li><i class="glyph-icon flaticon-diagram" aria-hidden="true"></i> <span>Why Alchemy High Growth?</span></li>
<li><i class="glyph-icon flaticon-chess" aria-hidden="true"></i> <span>Strategy at a Glance</span></li>
<li><i class="glyph-icon flaticon-employee" aria-hidden="true"></i> <span>Fund Manager</span></li>
<li><i class="glyph-icon flaticon-job-opportunities" aria-hidden="true"></i> <span>Opportunities galore in India</span></li>--%>
                </ul>

                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                    <%--<div>
<h3>Investment Objective</h3>
<p>The objective is to generate superior returns over the long run by investing in equities of primarily mid-cap companies.</p>

</div>
<div>
<h3>Why Alchemy High Growth?</h3>


<ul>
<li>India is one of the fastest growing economies with an entrepreneurial pool to exploit opportunities</li>

<li> “Top-Down” and “Bottom-Up” stock picking leads to consistent wealth creation</li>

<li>The High Growth strategy has up-surged its benchmark in 11 of the total 13 calendar years since 2002</li>
<li>Has created wealth reserves substantially - Initial investment of Rs.1 crore in 2002 has grown to 26 crores </li>
<li>A usual portfolio consists of 20-25 stocks; more than 25% for large-cap, less than 10% small-cap, and the balance for mid-cap</li>

</ul>


</div>
<div>

<h3>Strategy at a Glance</h3>


                          <ul>
                            <li><strong>Category </strong> -  Equity Diversified</li>
                            <li><strong>Type</strong>  - Open ended</li>
                            <li><strong>Benchmark</strong>  - BSE 500 </li>
                            <li><strong>Suggested time horizon</strong>  -  3 – 5 years</li>
                            <li><strong>Fund Style</strong>  -  Multi-cap growths</li>
                            <li><strong>Launch Date </strong> -  8th May, 2002</li>
                            <li><strong>Minimun Investment</strong> -  Rs.50 lakhs</li>
                          </ul>
</div>

<div>

<h3>Fund Manager – Amit Nadekar</h3>

<p>A Chartered Accountant by profession, Mr. Nadekar has worked across equity research, corporate strategy, taxation and audit over the last one and a half decades. He started his career on the sell side, tracking the US banking & financial sector; later moving on to the corporate side as a part of the Corporate Strategy team at Raymond.  
</p>
<p>Mr. Nadekar has been a core member of the investment team at Alchemy since October 2005.</p>


</div>




<div>
<h3>Opportunities galore in India</h3>


<ul>
<li>India ranks 6th in the world in terms of nominal GDP yet 14th according to per capita income.</li>

<li>  GDP projected at USD 6 trillion by the next decade – Study by Morgan Stanley</li>

<li>India’s encouraging demographics; more than 50 percent of its population is below the age of 25</li>
<li>India more resilient vis-à-vis other countries because of high domestic consumption.</li>
<li>Recent structural reforms in key areas</li>
<li> Domestic investors expected to outdo foreign counterparts in the next three years</li>

</ul>


</div>--%>
                </div>
            </div>


            <div class="clearfix"></div>
              <div class="social_resp">
            
            <p>	For complete details of the product, please <a href="Contact.aspx">Contact Us.</a></p>
        </div>
        </div>
       <a class="to_bottom "  id="callback" style="display: block;">
       <i class="fa fa-phone" aria-hidden="true"></i>
        <%--<button class="contact_us_btn" data-spy="affix" data-offset-top="10" title="Request a Callback"><i class="fa fa-phone" aria-hidden="true"></i></button>--%>
    </a>

    </section>


    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

</asp:Content>

