﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;

public partial class Contact : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SocialMedia(2);
            BindInnerBanner(11);

        }

        hdnutm_source.Value = Request.QueryString["utm_source"];

        hdnutm_medium.Value = Request.QueryString["utm_medium"];

        hdnutm_campaign.Value = Request.QueryString["utm_campaign"];

        hdnutm_term.Value = Request.QueryString["utm_term"];

        hdnutm_content.Value = Request.QueryString["utm_content"];

        hdnutm_device.Value = Request.QueryString["utm_device"];


    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void SocialMedia(int Id)
    {
        DataTable dt = new DataTable();
        Dictionary<string, string> obj = new Dictionary<string, string>();
        dt = utility.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                ltrFindUs.Text = dr["FindUs"].ToString();
                ltrCallUs.Text = dr["CallUs2"].ToString();
                ltrClient.Text = dr["Clients"].ToString();
                ltrPartners.Text = dr["Partners"].ToString();
            }

        }

    }


    protected void btnConSubmit_Click(object sender, EventArgs e)
    {
        if (CheckSaveContact())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
            {
                // store referrer value
                string referrerVal = string.Empty;
                HttpCookie nameCookie = Request.Cookies["referrer"];
                if (nameCookie != null)
                {
                    referrerVal = nameCookie["referrer"].ToString();
                }


          
                using (WebClient client = new WebClient())
                {
                    //var json = client.DownloadString("https://geoip-db.com/json");
                    //IPLocation location = new JavaScriptSerializer().Deserialize<IPLocation>(json);


                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@para", "add");
                        cmd.Parameters.AddWithValue("@FullName", txtFullName.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@Email", txtContactEmail.Text.ToString().Trim());
                        cmd.Parameters.AddWithValue("@ContactNo", txtContact.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@Interest", ddlContactIntrest.SelectedItem.Text.ToString().Trim());
                    //cmd.Parameters.AddWithValue("@Product", ddlProduct.SelectedItem.Text.ToString().Trim());
                    //cmd.Parameters.AddWithValue("@Comment", txtComments.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@utm_source", hdnutm_source.Value);
                        cmd.Parameters.AddWithValue("@utm_medium", hdnutm_medium.Value);
                        cmd.Parameters.AddWithValue("@utm_campaign", hdnutm_campaign.Value);
                        cmd.Parameters.AddWithValue("@utm_content", hdnutm_content.Value);
                        cmd.Parameters.AddWithValue("@utm_device", hdnutm_device.Value);
                        cmd.Parameters.AddWithValue("@utm_term", hdnutm_term.Value);
                        cmd.Parameters.AddWithValue("@Referrer", referrerVal);
                        cmd.Parameters.AddWithValue("@Request_From_Pop_Up", (string)(Session["popup"]));
                        //cmd.Parameters.AddWithValue("@CityName", location.city);
                        //cmd.Parameters.AddWithValue("@Region", location.state);
                        //cmd.Parameters.AddWithValue("@CountryName", location.country_name);

                    
                    cmd.Parameters.AddWithValue("@City", txtContactCity.Text.ToString().Trim());


                    if (ddlContactIntrest.SelectedItem.Text.ToString().Trim() == "Investing")
                    {
                        cmd.Parameters.AddWithValue("@Investing", ddlContactInvesting.SelectedItem.Text.ToString().Trim());
                    }

                    if (ddlContactIntrest.SelectedItem.Text.ToString().Trim() == "Others")
                    {
                        cmd.Parameters.AddWithValue("@Comment", txtComments.Text.ToString().Trim());
                    }

                    if (utility.Execute(cmd))
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                        sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact Us Enquiry </font></strong></td></tr>");
                        sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + txtFullName.Text.ToString().Trim() + " </td></tr>");
                        sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + txtContactEmail.Text.ToString().Trim() + " </td></tr>");
                        sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Phone : </strong></td> <td> " + txtContact.Text.ToString().Trim() + " </td> </tr>");

                        sb.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + txtContactCity.Text.ToString().Trim() + " </td> </tr>");

                        sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>I'm Interested : </strong></td> <td> " + ddlContactIntrest.SelectedItem.Text.ToString().Trim() + " </td></tr>");
                        if (ddlContactIntrest.SelectedItem.Text.ToString().Trim() == "Investing")
                        {
                            sb.AppendFormat("<tr> <td><strong>Interested in Investing : </strong></td> <td> " + ddlContactInvesting.SelectedItem.Text.ToString().Trim() + " </td></tr>");
                        }

                        if (ddlContactIntrest.SelectedItem.Text.ToString().Trim() == "Others")
                        {
                            sb.AppendFormat("<tr> <td><strong>Interested in Others : </strong></td> <td> " + txtComments.Text.ToString().Trim() + " </td></tr>");
                        }

                        //sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Products : </strong></td> <td> " + ddlProduct.SelectedItem.Text.ToString().Trim() + " </td> </tr>");
                        //sb.AppendFormat("<tr> <td><strong>Comments : </strong></td> <td> " + txtComments.Text.ToString().Trim() + " </td></tr>");
                        //sb.AppendFormat("<tr> <td><strong>Request from Ascent Pop-up : </strong></td> <td> " + (string)(Session["popup"]) + " </td></tr>");
                        //sb.AppendFormat("<tr> <td><strong>City : </strong></td> <td> " + location.city + " </td></tr>");
                        //sb.AppendFormat("<tr> <td><strong>Region : </strong></td> <td> " + location.state + " </td></tr>");
                        //sb.AppendFormat("<tr> <td><strong>Country : </strong></td> <td> " + location.country_name + " </td></tr>");





                        sb.Append("</table>");

                        string ToEmailid = ConfigurationManager.AppSettings["ContactUsEmail"].ToString();
                        String[] emailid = new String[] { ToEmailid };

                        utility.SendEmail(sb.ToString(), emailid, "Contact Us", "", null);

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ContactEventTracking", "ContactEventTracking(); window.location ='ThankYou.aspx';", true);

                        if ((string)(Session["popup"]) != null)
                        {
                            Session["popup"] = null;

                        }

                        Reset();

                    }
                    
                }
            }
        }
    }

    public bool CheckSaveContact()
    {
        bool isOK = true;
        string message = string.Empty;

        string emailemail = txtContactEmail.Text;
        Regex regexemail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match matchemailcontact = regexemail.Match(emailemail);


        string Phonecontact = txtContact.Text;

        Regex regexcontact = new Regex("^[1-9]{1}[0-9]{0,11}$");
        Match matchphonecontact = regexcontact.Match(Phonecontact);

        if (txtFullName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Full Name, ";
        }

        if (txtContact.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Phone, ";
        }

        if (!matchphonecontact.Success)
        {
            isOK = false;
            message += "Valid Phone no, ";
        }


        if (txtContactEmail.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Email, ";
        }
        if (!matchemailcontact.Success)
        {
            isOK = false;
            message += "Valid Email, ";
        }


        //if (txtComments.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "Comment, ";
        //}


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CheckSave", "alert('Please fill following fields')", true);
            lblMsgcontact.Text = "Please fill following fields <br />" + message;
            lblMsgcontact.ForeColor = System.Drawing.Color.Red;
        }
        return isOK;
    }




    private void Reset()
    {
        txtFullName.Text = "";
        txtContact.Text = "";
        txtContactEmail.Text = "";
        txtComments.Text = "";
        //ddlContactIntrest.SelectedValue = "ddlContactIntrest";
        //ddlProduct.SelectedValue = "Products";
    }


    public string ConvertDataTabletoString()
    {
        DataTable dt = new DataTable();
        StringBuilder JSONString = new StringBuilder();
        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row;
        dt = utility.Display("Exec Proc_ContactMapLocation 'GetLocationForFront'");
        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }


        return serializer.Serialize(rows);
    }

}

