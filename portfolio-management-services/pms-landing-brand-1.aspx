﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pms-landing-brand-1.aspx.cs" Inherits="portfolio_management_services_pms_landing_brand_2" %>
<%@ Register Src="~/request_call_back_landing.ascx" TagPrefix="uc2" TagName="request_call_back_landing" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#0f2765" />
	<meta name="description" content="Alchemy Capital Mangement is leading fund management company which offers Financial Management & Equity Investment in India. Visit Alchemy online to know more.">
	<title>Asset Management & PMS Provider in India -  Alchemy Capital </title>
	<link rel="shortcut icon" href="https://www.alchemycapital.com/Content/images/favicon.ico" type="image/x-icon" />
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/landing/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/landing/css/landing.css">
  
        <!-- 
Start of global snippet: Please do not remove
Place this snippet between the <head> and </head> tags on every page of your site.
-->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-10745315"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'DC-10745315');
    </script>
    <!-- End of global snippet: Please do not remove -->
</head>
<body>
	<header>
		<div class="container"><a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Brand-1&utm_content=invest-compass" class="logo"><img src="/landing/images/logo.png" title="Alchemy Capital" class="w-100"></a></div>
	</header>

	<section class="bannerSection">
		<img src="/landing/images/banner_compass.jpg" class="bannerImg">
		<div class="container">

			<div class="row">
				<div class="col-lg col-12 d-flex pt-4 pt-lg-0">
					
					<div class="bannerTextArea align-self-end text-white mb-lg-3 mr-lg-5">
						<h1 class="font-weight-bold mb-4 pr-xl-5 animateThis slide-right">Does your portfolio have companies that will benefit from the major thrust on "Atmanirbhar India"?</h1>
						<h2 class="font-weight-medium mx-auto mb-0 mb-lg-3 d-block d-lg-inline-block animateThis fade-in">Portfolio Management Services (PMS)</h2>
					</div>

				</div>
				<div class="col-lg-auto col-12">

					<div class="stickyBtnAnchor" id="stickyBtnAnchor"></div>
					
					<%--<div class="formArea">

						<div class="formHolder text-white mx-auto mb-4 mb-lg-0">
							<div class="formMain">

								<div class="formTitle font-weight-bold mb-3">Request a Callback</div>
								
								<form class="formBox">
									
									<div class="form-group">
										<input type="text" name="" placeholder="Name" class="form-control rounded-0">
										<span class="errorMsg">Name is required.</span>
									</div>
									
									<div class="form-group">
										<input type="text" name="" placeholder="Mobile No." class="form-control rounded-0">
										<span class="errorMsg">Mobile No. is required.</span>
									</div>
									
									<div class="form-group">
										<input type="email" name="" placeholder="Email" class="form-control rounded-0">
										<span class="errorMsg">Email is required.</span>
									</div>
									
									<div class="form-group">
										<input type="text" name="" placeholder="City" class="form-control rounded-0">
										<span class="errorMsg">City is required.</span>
									</div>
									
									<div class="form-group">
										<select class="form-control rounded-0">
											<option>I am interested in investing.</option>
											<option>I am Interested in IFA/ Channel Partner Relation</option>
										</select>
									</div>

									<div class="custom-control custom-checkbox consentStmt mb-2">
									  <input type="checkbox" class="custom-control-input" id="customCheck1">
									  <label class="custom-control-label" for="customCheck1">I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.</label>
									  <span class="errorMsg">This is required.</span>
									</div>

									<p class="consentStmt font-weight-medium mb-3">As per SEBI guidelines, minimum investment required is of &#8377;50 Lakhs</p>

									<button type="submit" class="btn rounded-pill">Submit</button>

								</form>

							</div>
							<div class="formBottom d-flex">
								<div class="col strip"></div><div class="col-auto fold"></div>
							</div>
						</div>
						
					</div>		--%>	
                    
                          <uc2:request_call_back_landing runat="server" ID="request_call_back_landing" />		
       

				</div>
			</div>

		</div>
	</section>

	<section class="aboutSection">

		<div class="container">

			<div class="aboutContent mx-auto mb-5 text-justify animateThis slide-top">
				<h2 class="title text-center text-uppercase font-weight-bold mb-4">About Us</h2>			
				<p class=" mb-4">Alchemy Capital has grown in repute over the last <strong>18 years</strong> and is currently one of the largest Portfolio Management Services (PMS) provider in the country, having lent our advice and investment skills to <strong>USD 893 million</strong> (as on 31st December 2020) worth of group assets. Alchemy’s institutional pedigree rests on integrity, process, insight and an unrelenting endeavour to offer consistently superior client experience.</p>
				<p class=" mb-4">Alchemy Capital has created substantial wealth since its inception*. &#8377;1 Cr invested in the flagship product has grown to <strong>&#8377;33.1 Cr v/s &#8377;14.9 Cr</strong> in S&P BSE 500.</p>
				<p class="font-italic mb-4"><small>* Inception date: 8th May 2002. Data as of 31st Jan 2021. Past Performance is not indicative of future performance.</small></p>
			</div>

			<div class="row mx-auto justify-content-lg-between justify-content-around text-center aboutStats">
			<%--	<div class="col-auto mb-3 mb-md-0 animateThis fade-in">
					<img src="/Content/images/Landing/icon_1.png" class="d-block mx-auto mb-3"> 
					<p class="mb-0"><strong class="number font-weight-bold d-block">1999</strong> Founded in year</p>
				</div>
				<div class="col-auto mb-3 mb-md-0 animateThis fade-in" style="transition-delay:.3s">
					<img src="/Content/images/Landing/icon_2.png" class="d-block mx-auto mb-3"> 
					<p class="mb-0"><strong class="number font-weight-bold d-block">893</strong> Million USD Funds Managed</p>
				</div>
				<div class="col-auto mb-3 mb-md-0 animateThis fade-in" style="transition-delay:.6s">
					<img src="/Content/images/Landing/icon_3.png" class="d-block mx-auto mb-3"> 
					<p class="mb-0"><strong class="number font-weight-bold d-block">4666</strong> Families <br><small>(as on 31st December 2020)</small></p>
				</div>--%>
                 <asp:Literal ID="ltrHomeStatistic" runat="server"></asp:Literal>
			</div>
			
		</div>		

	</section>

<%--	<section class="mrktviewSection">

		<div class="container">

			<div class="aboutContent mx-auto mb-5 text-justify animateThis slide-top">
				<h2 class="title text-center text-uppercase font-weight-bold mb-4">Market Views</h2>			
				<p class="text-center mb-4">Gain insights relating to the economy, markets, and the investment environment, from our <strong class="d-inline-block">Director, CEO & CIO Mr. Hiren Ved</strong></p>
			</div>

			<div class="row row-cols-lg-5 row-cols-sm-3 row-cols-2 justify-content-center mx-auto mrktviewList">
			              <asp:Literal ID="ltrTopFiveMarketViews" runat="server"></asp:Literal>
            </div>

		</div>
		
	</section>--%>

	<footer class="mt-lg-4">
		<div class="container text-center text-lg-left animateThis fade-in">
			
			<div class="row">
				<div class="col-lg-auto">
					Copyright &copy; 2021 Alchemy. All Rights Reserved.<br>
					<a href="https://www.alchemycapital.com/disclaimer.aspx?utm_source=DV360&utm_medium=Display&utm_campaign=Brand-1&utm_content=invest-compass" target="_blank">Disclaimer</a> -  Investments in securities market are subject to market risks.
				</div>
				<div class="col-lg-auto pl-lg-5 ml-lg-5">
					<div class="row mx-auto no-gutters socialBtns">
<%--						<div class="col"><a href="https://www.facebook.com/alchemycapital/" target="_blank"><img src="/landing/images/ic_fb.png"></a></div>--%>
						<div class="col"><a href="https://www.youtube.com/channel/UCFvIGco1pAKZyVIZFGWf2ZQ/featured" target="_blank"><img src="/landing/images/ic_yt.png"></a></div>
						<div class="col"><a href="https://www.linkedin.com/company/alchemy-capital-management" target="_blank"><img src="/landing/images/ic_li.png"></a></div>
					</div>
				</div>
				<div class="col-lg-auto text-lg-right ml-lg-auto">
					<a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Brand-1&utm_content=invest-compass">www.alchemycapital.com</a><br>
					<a href="https://www.kwebmaker.com" target="_blank" class="kweb">Kwebmaker&trade;</a>
				</div>
			</div>

		</div>
	</footer>

	<div class="stickBtn">
    <button type="button" class="btn floatBtn text-uppercase d-flex align-items-center justify-content-center" id="gotoForm"><strong><img src="/landing/images/arw.png" class="d-block mx-auto mb-1"> Request a Callback</strong></button>
	</div>


	<script src="/landing/js/jquery-3.5.1.min.js"></script>
	<script src="/landing/js/bootstrap.bundle.min.js"></script>


	<script>
		/*  ---------- ON-SCROLL ANIMATION ------------  */
		$(window).on('load',function() {
			$(".bannerTextArea h1, .bannerTextArea h2").addClass("in-view");
		});


		var $animation_elements = $('.animateThis');
	  var $window = $(window);

	  function check_if_in_view() {
	    var window_height = $window.height();
	    var window_top_position = $window.scrollTop();
	    var window_bottom_position = (window_top_position + window_height - 30);
	   
	    $.each($animation_elements, function() {
	      var $element = $(this);
	      var element_height = $element.outerHeight();
	      var element_top_position = $element.offset().top+30;
	      var element_bottom_position = (element_top_position + element_height);

	      if ((element_top_position <= window_bottom_position))  {
	        $element.addClass('in-view');
	      } else {
	        $element.removeClass('in-view');
	      }
	    });
	  }
	  $window.on('scroll resize', check_if_in_view);

	  /* ---------- Sticky Btn ----------- */

	  function sticky_relocate() {    
        var window_top = $(window).scrollTop();
        var div_top = $('#stickyBtnAnchor').offset().top;
        if (window_top > div_top) {
            $('.stickBtn').addClass('stick');
        } else {
            $('.stickBtn').removeClass('stick');
        }   
    }
    $(function () {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });

    $('#gotoForm').click(function(){
      $('html,body').animate({scrollTop: $('body').offset().top});
    });

	</script>

    <!--google analytics code -->
        <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-51603925-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-51603925-1');
    </script>


<!--
Event snippet for Remarketing-LP-1 on : Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 03/16/2021
-->
<script>
  gtag('event', 'conversion', {
    'allow_custom_scripts': true,
    'send_to': 'DC-10745315/invmedia/remar0+standard'
  });
</script>
<noscript>
<img src="https://ad.doubleclick.net/ddm/activity/src=10745315;type=invmedia;cat=remar0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ord=1?" width="1" height="1" alt=""/>
</noscript>
<!-- End of event snippet: Please do not remove -->

        <!-- Global site tag (gtag.js) - Google Ads: 10797805103 -->
<script async="async" src="https://www.googletagmanager.com/gtag/js?id=AW-10797805103"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'AW-10797805103');
</script>
</body>
</html>
