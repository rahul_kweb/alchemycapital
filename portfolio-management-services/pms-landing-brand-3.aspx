﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="pms-landing-brand-3.aspx.cs" Inherits="portfolio_management_services_pms_landing_brand_3" %>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://www.alchemycapital.com/Content/images/favicon.ico" type="image/x-icon" />
    <title>Top Portfolio Management Companies in India | PMS Provider - Alchemy Capital</title>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/landing/css/main.css">

    <!-- 
Start of global snippet: Please do not remove
Place this snippet between the <head> and </head> tags on every page of your site.
-->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-10745315"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'DC-10745315');
    </script>
    <!-- End of global snippet: Please do not remove -->
</head>
<body class="home">
    <div class="site">
        <header class="site-header">
            <div class="header-container width-setter">
                <div class="logo-container">
                    <a href="https://www.alchemycapital.com/" target="_blank">
                        <img src="/landing/images/logo_new.png" alt="Alchemy">
                    </a>
                </div>
            </div>
        </header>

        <div class="home-banner-container">
            <div class="banner-image">
                <div class="width-setter">
                    <div class="banner-text">
                        <h3><span>Alchemy capital's</span> portfolio management services.</h3>
                        <h3>A pedigree of uninterrupted performance.</h3>
                    </div>

                    <div class="callback-form-container">
                        <form method="post" id="cform_1" runat="server" action="">
                            <asp:HiddenField ID="hdnutm_source" runat="server" />
                            <asp:HiddenField ID="hdnutm_medium" runat="server" />
                            <asp:HiddenField ID="hdnutm_campaign" runat="server" />
                            <asp:HiddenField ID="hdnutm_device" runat="server" />
                            <asp:HiddenField ID="hdnutm_content" runat="server" />
                            <asp:HiddenField ID="hdnutm_term" runat="server" />
                            <asp:HiddenField ID="hdnutm_referrer" runat="server" />
                            <div class="cform_heading">
                                <h3 class="cform_title">Request a Callback</h3>
                            </div>
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            <div class="cform_body">
                                <ul id="cform_fields_1" class="cform_fields">
                                    <li id="field_1_1" class="cfield">
                                        <label class="cfield_label" for="input_1_1">Name<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_text">
                                            <%--	<input name="input_1" id="input_1_1" type="text" value="" class="large" placeholder="Name">--%>
                                            <asp:TextBox ID="txtName" CssClass="large" placeholder="Name" autocomplete="off" runat="server"></asp:TextBox>
                                            <span id="ErrorName1" class="errorMsg">This field is required.</span>
                                        </div>
                                    </li>
                                    <li id="field_1_2" class="cfield">
                                        <label class="cfield_label" for="input_1_2">Mobile No.<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_phone">
                                            <asp:TextBox ID="txtPhone" CssClass="large" placeholder="Mobile No." runat="server" autocomplete="off" MinLength="10" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                                            <span id="ErrorPhone1" class="errorMsg">This field is required.</span>
                                        </div>
                                    </li>
                                    <li id="field_1_3" class="cfield">
                                        <label class="cfield_label" for="input_1_3">Email<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_email">
                                            <asp:TextBox ID="txtEmail" CssClass="large" autocomplete="off" placeholder="Email" runat="server"></asp:TextBox>
                                            <span id="ErrorEmail3" class="errorMsg">This field is required.</span>
                                            <span id="ErrorEmail4" class="errorMsg">Invalid email address.</span>
                                        </div>
                                    </li>
                                    <li id="field_1_4" class="cfield">
                                        <label class="cfield_label" for="input_1_4">City<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_text">
                                            <asp:TextBox ID="txtCity" CssClass="large" placeholder="City" autocomplete="off" runat="server"></asp:TextBox>
                                            <span id="ErrorCity" class="errorMsg">This field is required.</span>
                                            <span id="ErrorCity1" class="errorMsg">Please enter a valid City.</span>
                                        </div>
                                    </li>
                                    <li id="field_1_5" class="cfield">
                                        <label class="cfield_label" for="input_1_5">Investment<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_select">
                                            <%--<select name="input_5" id="input_1_5" class="large cfield_select">
					                    			<option value="" selected="selected" class="cf_placeholder">I am interested in investing</option>
					                    			<option value="First Choice">First Choice</option>
					                    			<option value="Second Choice">Second Choice</option>
					                    			<option value="Third Choice">Third Choice</option>
					                    		</select>--%>
                                            <asp:DropDownList ID="ddlIntrest" runat="server" CssClass="large cfield_select">
                                                <asp:ListItem Value="I am Interested in Investing">I am Interested in Investing</asp:ListItem>
                                                <asp:ListItem Value="I am Interested in IFA / Channel Partner Relation">I am Interested in IFA / Channel Partner Relation</asp:ListItem>
                                                <%--<asp:ListItem Value="First Choice">First Choice</asp:ListItem>
                                                <asp:ListItem Value="Second Choice">Second Choice</asp:ListItem>
                                                <asp:ListItem Value="Third Choice">Third Choice</asp:ListItem>--%>
                                            </asp:DropDownList>
                                            <span id="ErrorIntrest" class="errorMsg">This field is required.</span>
                                        </div>
                                    </li>
                                    <li id="field_1_6" class="cfield">
                                        <label class="cfield_label">Authorize<span class="cfield_required">*</span></label>
                                        <div class="cinput_container cinput_container_checkbox">
                                            <ul class="cfield_checkbox" id="input_1_6">
                                                <li class="cchoice_1_6_1">
                                                    <input name="input_6.1" type="checkbox" value="I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC" id="check">
                                                    <label for="choice_1_6_1" id="label_1_6_1">I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC</label>
                                                    <span id="errorcheck" class="errorMsg">This field is required.</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li id="field_1_7" class="cfield">As per SEBI guidelines, minimum investment required is of ₹50 Lakhs</li>
                                </ul>
                            </div>
                            <div class="cform_footer">
                                <%--<input type="submit" id="cform_submit_button_1" class="cform_button button" value="Submit">--%>
                                <asp:Button ID="btn_Req_Submit" CssClass="cform_button button" Text="Submit" runat="server" OnClick="btn_Req_Submit_Click" OnClientClick="javascript:return Checkfields();" Style="padding: 10px 25px; width: auto; color: white" />

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="banner-note-container">
                <div class="width-setter">
                    <div>19 Years of Enduring Legacy</div>
                    <div>Serving ~3000 Families & Institutions</div>
                    <div>AUM USD 1 Billion*</div>
                    <!-- 19 Years of Enduring Legacy | Serving 3000+ Families & Institutions | AUM USD 1 Billion* -->
                </div>
            </div>
        </div>

        <div class="pms-section width-setter">
            <h3 class="section-title">Pioneers in portfolio management services
            </h3>

            <div class="pms-flex-container">
                <div class="flex-section">
                    <p>
                        Alchemy Capital is one of the largest Portfolio Management Services (PMS) provider in India. Through more than 19 years of its existence, Alchemy has nurtured a pedigree on values of integrity, scientific process, insight and an unrelenting endeavour to offer a consistently superior client experience.
                    </p>
                    <p>
                        We are proud to share that Alchemy Capital has created substantial wealth since its inception**. ₹1 Cr invested in 2002 in the flagship product has grown to ₹45.2 Cr in S&amp;P BSE 500.
                    </p>
                    <div class="pms-note">
                        <div>*Data as on 31st December, 2021</div>
                        <div>**Inception Date – 8th May 2002 | Past Performance is not indicative of future performance</div>
                    </div>
                </div>
                <div class="flex-section">
                    <p>
                        Alchemy Capital’s Portfolio Management Services has been working untiringly and smartly towards its investors’ long-term wealth creation goals. Alchemy Capital capitalises on a collective experience of 100 years between the founders to provide an exemplary client experience and a solid investment approach that has weathered many milestones.
                    </p>
                    <p>
                        The exclusive Bottom-Up strategy and periodic monitoring makes Alchemy Capital stand out as an asset management organization of choice. Today Alchemy has lent its skills and advice to group assets worth USD 1 billion as on 31st December, 2021.
                    </p>
                </div>
            </div>
            <div class="pms-mobile-container">
                <div class="pms-note">
                    <div>*Data as on 31st December, 2021</div>
                    <div>**Inception Date – 8th May 2002 | Past Performance is not indicative of future performance</div>
                </div>
            </div>
        </div>



        <div class="process-section width-setter">
            <h3 class="section-title">Our process for your progress
            </h3>

            <div class="process-flex-container">
                <div class="flex-section">
                    <div class="process-icon">
                        <img src="/landing/images/ideation.png" alt="">
                        <h6 class="icon-title">Ideation
                        </h6>
                    </div>
                </div>
                <div class="flex-section">
                    <div class="process-icon">
                        <img src="/landing/images/interact.png" alt="">
                        <h6 class="icon-title">Interact with the management of companies and ecosystem
                        </h6>
                    </div>
                </div>
                <div class="flex-section">
                    <div class="process-icon">
                        <img src="/landing/images/finance.png" alt="">
                        <h6 class="icon-title">Financial Analysis
                        </h6>
                    </div>
                </div>
                <div class="flex-section">
                    <div class="process-icon">
                        <img src="/landing/images/reviews.png" alt="">
                        <h6 class="icon-title">Regular reviews to assess ongoing risk-reward and anticipate risks
                        </h6>
                    </div>
                </div>
                <div class="flex-section">
                    <div class="process-icon">
                        <img src="/landing/images/exit.png" alt="">
                        <h6 class="icon-title">Exit strategy
                        </h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="strategies-section width-setter">
            <h3 class="section-title">Our PMS Strategies
            </h3>

            <div class="strategy-flex-container">
                <div class="flex-section">
                    <h6 class="strategy-title"><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth-select-stock.aspx" target="_blank" style="color: #545454; text-decoration: none;">Alchemy High Growth Select Stock</a></h6>
                    <p class="strategy-desc">
                        Alchemy High Growth Select Stock provides you superior long-term returns by investing in high conviction concentrated stocks around 8 to 12 across sectors, with a focus on mid-cap companies.
                    </p>
                </div>
                <div class="flex-section">
                    <h6 class="strategy-title"><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" target="_blank" style="color: #545454; text-decoration: none;">Alchemy high growth</a></h6>
                    <p class="strategy-desc">
                        Alchemy High Growth seeks to identify high quality growth that can deliver superior risk-adjusted returns through a multi-cap investment strategy.
                    </p>
                </div>
                <div class="flex-section">
                    <h6 class="strategy-title"><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-leaders.aspx" target="_blank" style="color: #545454; text-decoration: none;">Alchemy Leaders</a></h6>
                    <p class="strategy-desc">
                        Alchemy Leaders, with its large-cap bias, endeavours to invest in high quality established companies that have had a demonstrable track record of running a scalable profitable business at the right inflection point on its growth path and have successfully navigated several adverse business cycles.
                    </p>
                </div>
                <div class="flex-section">
                    <h6 class="strategy-title"><a href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" target="_blank" style="color: #545454; text-decoration: none;">Alchemy Ascent</a></h6>
                    <p class="strategy-desc">
                        Alchemy ASCENT a multi-cap fund endeavours to provide a consistent “high alpha” investment strategy which build portfolios to deliver consistent outperformance over the long term, using an objective, back tested and data driven approach devoid of any biases.
                    </p>
                </div>
            </div>

            <div class="strategy-note">
                For complete details, please refer to our <strong><a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132478220119620140.pdf" style="color: #545454; text-decoration: none;" target="_blank">Disclosure Document.</a></strong>
            </div>

            <div class="know-more-container">
                <p>To know more about our offerings and strategies,</p>
                <span class="know-more-link" style="cursor: pointer;" id="gotoForm">connect with us today!</span>
            </div>
        </div>



        <footer class="site-footer">
            <div class="footer-container width-setter">
                <div class="widget_nav_menu">
                    <nav class="menu-footer-menu-container">
                        <ul id="menu-footer-menu" class="menu">
                            <li class="menu-item">
                                <a href="/media-center.aspx" class="menu-link" target="_blank">Media centre</a>
                            </li>
                            <li class="menu-item">
                                <a href="/market-views.aspx" class="menu-link" target="_blank">Insights</a>
                            </li>
                            <li class="menu-item">
                                <a href="/portfolio-management-services.aspx" class="menu-link" target="_blank">Our products & services</a>
                            </li>
                            <li class="menu-item">
                                <a href="/people-and-culture.aspx" class="menu-link" target="_blank">People & culture</a>
                            </li>
                            <li class="menu-item">
                                <a href="/contact.aspx" class="menu-link" target="_blank">Contact us</a>
                            </li>
                            <li class="menu-item">
                                <a href="/faq.aspx" class="menu-link" target="_blank">FAQs</a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="site-info-container">
                    <div class="info-left-container">
                        <div class="site-disclaimer">
                            <a href="/disclaimer-disclosure.aspx" class="footer-link" target="_blank">Disclaimer - Investments in securities market are subject to market risks. Please refer the link for more details.</a>
                        </div>
                        <div class="site-info">
                            copyright © 2021 alchemy. All rights reserved.
                        </div>
                    </div>
                    <div class="info-right-container">
                        <a href="https://www.alchemycapital.com/" target="_blank" class="footer-link">www.alchemycapital.com
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <style>
        .errorMsg {
            font-size: 13px;
            line-height: 1.5;
            color: #ff0000;
            font-weight: 700;
            display: none;
        }

        .consentStmt .errorMsg {
            position: absolute;
            top: -5px;
            left: -10px;
            font-size: 18px;
            font-weight: 700;
        }
    </style>


    <script>
        ///  store referrer value

        <%--    document.getElementById("<%=hdnreferrer.ClientID%>").value = document.referrer;--%>

    </script>

    <script src="/landing/js/jquery-3.5.1.min.js"></script>

    <script>

        $('#gotoForm').click(function () {
            $('html,body').animate({ scrollTop: $('body').offset().top });
        });

        function isNumberKey1(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            var textBox = document.getElementById('<%=txtPhone.ClientID%>').value.length;
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
                return false;

            return true;
        }

        function Checkfields() {
            debugger;
            var Name1 = $('#<%=txtName.ClientID%>').val();
        var Phone1 = $('#<%=txtPhone.ClientID%>').val();
        var Email1 = $('#<%=txtEmail.ClientID%>').val();
        var City = $('#<%=txtCity.ClientID%>').val();
        var Intrest = $('#<%=ddlIntrest.ClientID%>').val();


        var Blank1 = false;
        var message = "";

        if (Name1 == '') {
            $('#ErrorName1').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorName1').css('display', 'none');

        }


        if (Phone1 == '') {
            $('#ErrorPhone1').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorPhone1').css('display', 'none');
        }


        if (Email1 == '') {
            $('#ErrorEmail3').css('display', 'block');
            $('#ErrorEmail4').css('display', 'none');
            Blank1 = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(Email1)) {
                $('#ErrorEmail3').css('display', 'none');
                $('#ErrorEmail4').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#ErrorEmail4').css('display', 'none');


            }
            $('#ErrorEmail3').css('display', 'none');


        }

        if (City == '') {
            $('#ErrorCity').css('display', 'block');
            //$('#ErrorCity1').css('display', 'none');
            Blank1 = true;
        }
        else {

            //only alphabets allowed

            var cityvalid = /^[a-zA-Z]+$/;

            if (!cityvalid.test(City)) {
                $('#ErrorCity').css('display', 'none');
                $('#ErrorCity1').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#ErrorCity1').css('display', 'none');
            }

            $('#ErrorCity').css('display', 'none');
        }
        if (Intrest == '') {
            $('#ErrorIntrest').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorIntrest').css('display', 'none');

        }




        if (check.checked == false) {
            $('#errorcheck').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#errorcheck').css('display', 'none');
        }


        if (Blank1) {

            return false;

        }
        else {
            return true;
        }
    }
    </script>

    <!--google analytics code -->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-51603925-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-51603925-1');
    </script>




    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'AW-10797805103');
    </script>

    <!--
Event snippet for Remarketing-LP-1 on : Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 03/16/2021
-->
    <script>
        gtag('event', 'conversion', {
            'allow_custom_scripts': true,
            'send_to': 'DC-10745315/invmedia/remar0+standard'
        });
    </script>
    <noscript>
        <img src="https://ad.doubleclick.net/ddm/activity/src=10745315;type=invmedia;cat=remar0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ord=1?" width="1" height="1" alt="" />
    </noscript>
    <!-- End of event snippet: Please do not remove -->

</body>



</html>
