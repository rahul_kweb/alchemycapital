﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="alchemy-ascent.aspx.cs" Inherits="portfolio_management_services_alchemy_ascent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
       <title>Portfolio Management Service Providers in India - Alchemy Ascent</title>
   <meta name="description" content="Alchemy Ascent is a Portfolio Management Service (PMS) that uses an objective, back tested and data-driven strategy to produce continuous outperformance over time."/>

 <meta property="og:image" content="https://www.alchemycapital.com/Content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Alchemy Ascent – A Portfolio Management Service (PMS) to deliver consistent outperformance over the long term, using an objective, back tested & data driven approach." />
    <meta property="keywords" content=" Portfolio Management Service"/>

    <meta property="og:url" content="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx"/>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("BannerImage") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Alchemy Ascent – PMS</h1>

                                <ol class="breadcrumb">
                                    <li><a rel="canonical" href="/">Home</a></li>
                                    <li><a href="/portfolio-management-services.aspx">Our Products</a></li>
                                    <li>Alchemy Ascent</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">


        <div class="container">
            <asp:HiddenField ID="hdnId" runat="server" />

            <h2 class="column-title title-small text-center">
                <asp:Literal ID="ltrPageName" runat="server"></asp:Literal><small class="subtitle"> </small></h2>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTabHeading" runat="server"></asp:Literal>
                </ul>

                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </div>
            </div>


            <div class="clearfix"></div>
              <div class="social_resp">
            
            <p>	For complete details of the product, please <a href="/Contact.aspx">Contact Us.</a></p>
        </div>
        </div>
       <a class="to_bottom "  id="callback" style="display: block;">
       <i class="fa fa-phone" aria-hidden="true"></i>
    </a>

    </section>


    <link rel="stylesheet" type="text/css" href="/Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="/Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="/Content/css/easy-responsive-tabs.css" />



 <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Our Products",
    "item": "https://www.alchemycapital.com/portfolio-management-services.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Alchemy Ascent",
    "item": "https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "Product", 
  "name": "Alchemy Ascent – (PMS)",
  "image": "https://www.alchemycapital.com/content/uploads/innerbanner/innerbanner--132173354853437781.jpg",
  "description": "Alchemy ASCENT a multi-cap fund endeavours to provide a consistent “high alpha” investment strategy which build portfolios to deliver consistent outperformance over the long term, using an objective, back tested and data driven approach devoid of any biases.",
  "brand": "Alchemy Capital",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "5",
    "bestRating": "5",
    "worstRating": "1",
    "ratingCount": "4"
  }
}
</script>

 <script type="text/javascript"  src="/Content/js/jquery.js"></script>
 
    <script type="text/javascript">
        //remove double slash from url
        $(document).ready(function() {

            var url = window.location.href;
            
            if (url == "https://www.alchemycapital.com/portfolio-management-services//alchemy-ascent.aspx") {
                clean_url = url.replace(/([^:]\/)\/+/g, "$1");
                window.location.replace(clean_url);
            }

        });
    </script>
    

</asp:Content>

