﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class portfolio_management_services_alchemy_leaders : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindLiteral();
        getwhatweoffers();
    }

    //public void BindString()
    //{
    //    if (Request.QueryString["PageName"] != null && Request.QueryString["PageName"] != "")
    //    {

    //        hdnId.Value = Request.QueryString["PageName"];
    //        getwhatweoffers();

    //    }
    //}

    public void getwhatweoffers()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_WhatWeOfferContents 'bindWhatWeOfferUrl',7");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();


        }
    }

    public void BindLiteral()
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();
        StringBuilder strPageName = new StringBuilder();


        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_WhatWeOfferInnerPages 'bindWhatWeOfferDetailPage',0,0,7");
        if (dt.Rows.Count > 0)
        {
            strPageName.Append(dt.Rows[0]["PageName"].ToString());



            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='/Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /> <span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append(dr["Description"].ToString());
                strDesc.Append("</div>");

            }

        }
        ltrPageName.Text = strPageName.ToString();
        ltrTabHeading.Text = strTitle.ToString();
        ltrDescription.Text = strDesc.ToString();
    }
}