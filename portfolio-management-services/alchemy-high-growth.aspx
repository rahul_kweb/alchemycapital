﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="alchemy-high-growth.aspx.cs" Inherits="alchemy_high_growth" %>

<asp:Content ID="Content1" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Alchemy High Growth | Asset Management Companies in India</title>
   <meta name="description" content="Alchemy High Growth - Alchemy Capital offers a multi-cap investment strategy aimed at selecting high-quality growth stocks with better risk and return characteristics."/>

  <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx" hreflang="x-default" />


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("BannerImage") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Alchemy High Growth- PMS</h1>

                                <ol class="breadcrumb">
                                    <li><a rel="canonical" href="/">Home</a></li>
                                    <li><a href="/portfolio-management-services.aspx">Our Products</a></li>
                                    <li>Alchemy High Growth- PMS</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">


        <div class="container">
            <asp:HiddenField ID="hdnId" runat="server" />

            <h2 class="column-title title-small text-center">
                <asp:Literal ID="ltrPageName" runat="server"></asp:Literal><small class="subtitle"> </small></h2>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTabHeading" runat="server"></asp:Literal>
                </ul>

                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </div>
            </div>


            <div class="clearfix"></div>
              <div class="social_resp">
            
            <p>	For complete details of the product, please <a href="/Contact.aspx">Contact Us.</a></p>
        </div>
        </div>
       <a class="to_bottom "  id="callback" style="display: block;">
       <i class="fa fa-phone" aria-hidden="true"></i>
    </a>

    </section>


    <link rel="stylesheet" type="text/css" href="/Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="/Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="/Content/css/easy-responsive-tabs.css" />




    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Our Products",
    "item": "https://www.alchemycapital.com/portfolio-management-services.aspx"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Alchemy High Growth - PMS",
    "item": "https://www.alchemycapital.com/portfolio-management-services/alchemy-high-growth.aspx"  
  }]
}
</script>


</asp:Content>

