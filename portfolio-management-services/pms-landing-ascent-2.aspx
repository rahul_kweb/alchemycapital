﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pms-landing-ascent-2.aspx.cs" Inherits="portfolio_management_services_pms_landing_ascent_2" %>

<%@ Register Src="~/request_call_back_landing.ascx" TagPrefix="uc2" TagName="request_call_back_landing" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#0f2765" />
    <meta name="description" content="Alchemy Capital Mangement is leading fund management company which offers Financial Management & Equity Investment in India. Visit Alchemy online to know more.">
    <title>Asset Management & PMS Provider in India -  Alchemy Capital </title>
    <link rel="shortcut icon" href="https://www.alchemycapital.com/Content/images/favicon.ico" type="image/x-icon" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/landing/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/landing/css/landing.css">

    <!-- 
Start of global snippet: Please do not remove
Place this snippet between the <head> and </head> tags on every page of your site.
-->
    <!-- Global site tag (gtag.js) - Google Marketing Platform -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=DC-10745315"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'DC-10745315');
    </script>
    <!-- End of global snippet: Please do not remove -->

</head>
<body>

    <header>
        <div class="container"><a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Ascent-2&utm_content=globe" class="logo">
            <img src="/landing/images/logo.png" title="Alchemy Capital" class="w-100"></a></div>
    </header>

    <section class="bannerSection">
        <img src="/landing/images/Ascent2.jpg" class="bannerImg">
        <div class="container">

            <div class="row">
                <div class="col-lg col-12 d-flex pt-4 pt-lg-0 transbg">

               <%--     <div class="bannerTextArea whiteBg align-self-end text-white mb-lg-3 mr-lg-5 text-center text-lg-left">
                        <h1 class="pr-xl-5 font-weight-bold animateThis slide-right text-uppercase"><span>ALCHEMY ASCENT</span></h1>
                        <h2 class="font-weight-medium mx-auto mb-3 d-block d-lg-inline-block animateThis fade-in">Portfolio Management Services (PMS)</h2>
                    </div>--%>

                      <div class="bannerTextArea ascentformat_1 align-self-center text-center text-lg-left mt-lg-n5">
                        <h1 class="mb-4 font-weight-bold animateThis slide-right text-uppercase" style="font-family:Calibri,sans-serif;font-size:38px;""><span>ALCHEMY ASCENT</span><br class="d-none d-lg-block">
                           METHOD BASED<br class="d-none d-lg-block">
                            COMBINATION OF RIGHT<br class="d-none d-lg-block">
                            DESIRABLE ATTRIBUTES.
                        </h1>
                     <%--   <h2 class="font-weight-medium mx-auto mb-3 d-block d-lg-inline-block text-white text-uppercase animateThis fade-in">Portfolio Management Services (PMS)</h2>--%>
                    </div>

                </div>
                <div class="col-lg-auto col-12">

                    <div class="stickyBtnAnchor" id="stickyBtnAnchor"></div>

                    <%--<div class="formArea">

						<div class="formHolder text-white mx-auto mb-4 mb-lg-0">
							<div class="formMain">

								<div class="formTitle font-weight-bold mb-3">Request a Callback</div>
								
								<form class="formBox">
									
									<div class="form-group">
										<input type="text" name="" placeholder="Name" class="form-control rounded-0">
										<span class="errorMsg">Name is required.</span>
									</div>
									
									<div class="form-group">
										<input type="text" name="" placeholder="Mobile No." class="form-control rounded-0">
										<span class="errorMsg">Mobile No. is required.</span>
									</div>
									
									<div class="form-group">
										<input type="email" name="" placeholder="Email" class="form-control rounded-0">
										<span class="errorMsg">Email is required.</span>
									</div>
									
									<div class="form-group">
										<input type="text" name="" placeholder="City" class="form-control rounded-0">
										<span class="errorMsg">City is required.</span>
									</div>
									
									<div class="form-group">
										<select class="form-control rounded-0">
											<option>I am interested in investing.</option>
											<option>I am Interested in IFA/ Channel Partner Relation</option>
										</select>
									</div>

									<div class="custom-control custom-checkbox consentStmt mb-2">
									  <input type="checkbox" class="custom-control-input" id="customCheck1">
									  <label class="custom-control-label" for="customCheck1">I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.</label>
									  <span class="errorMsg">This is required.</span>
									</div>

									<p class="consentStmt font-weight-medium mb-3">As per SEBI guidelines, minimum investment required is of &#8377;50 Lakhs</p>

									<button type="submit" class="btn rounded-pill">Submit</button>

								</form>

							</div>
							<div class="formBottom d-flex">
								<div class="col strip"></div><div class="col-auto fold"></div>
							</div>
						</div>
						
					</div>		--%>

                    <uc2:request_call_back_landing runat="server" ID="request_call_back_landing" />


                </div>
            </div>

        </div>
    </section>

    <section class="aboutSection">

        <div class="container">

            <div class="aboutContent mx-auto mb-5 text-justify animateThis slide-top">
                <h2 class="title text-center text-uppercase font-weight-bold mb-4">Alchemy Ascent - Magic or Method</h2>
                <p class="mb-4">
                    <strong>Preamble</strong><br>
                    In the current investment world it is believed that the key to any successful investment strategy is in finding the "right stock", the "right theme", the "right sector" and the "right cycle". Even the most experienced investors are prone to biases which are inherent in the investing process. This results in inconsistent alpha generation.
                </p>

                <ul class="ml-4">
                    <li class="mb-4">However, we believe that building a high performance portfolio involves much more. Stock allocation, its relative ranking in the portfolio and timing along with risk controls, also play a very important part of the alpha generation process. We believe in strong data based research process, which considers all sources of alpha generation comprehensively, thus helps in creating an unbiased and disciplined investment approach – a key to successful and consistent investment outcomes.</li>
                    <li class="mb-4"><strong style="color: #333">Alchemy ASCENT*</strong> endeavors to provide a consistent "high alpha" investment strategy which build portfolios to deliver consistent outperformance over the long term, using an objective, back tested and data driven approach devoid of any biases.</li>
                </ul>

                <picture>
					<source media="(max-width:767.98px)" srcset="/landing/images/accent_img_mob.jpg">
					<img src="/landing/images/accent_img.jpg" class="w-100 mb-4 animateThis slide-top">
				</picture>

                <p class="font-italic mb-4">
                    <small>* Alchemy Ascent is one of the Portfolio Management Services product managed by M/s Alchemy Capital Management Pvt Ltd, a SEBI registered Portfolio Manager.<br>
                        * The investment objectives and strategy are indicative and there are no assurances that they will be achieved.</small>
                </p>
            </div>



            <div class="aboutContent mx-auto pt-5 text-justify animateThis slide-top">
                <h2 class="title text-center text-uppercase font-weight-bold mb-4">Why Alchemy Ascent</h2>
                <p class="mb-4"><strong>Objective Method Investing*</strong></p>

                <div class="row omiHolder">
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">1</div>
                        <div class="omiTxt">Unbiased approach towards each stock </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">2</div>
                        <div class="omiTxt">Greed/Fear/Hope are mitigated by having objective method for investment </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">3</div>
                        <div class="omiTxt">Focus on Discipline & Data to generate CAGR </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">4</div>
                        <div class="omiTxt">Back Tested for more than 21 years across many market cycles </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">5</div>
                        <div class="omiTxt">Ability to scan over 2000 stocks daily to find a winner </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">6</div>
                        <div class="omiTxt">Endeavor to generate returns outperforming the relevant Index over 3-5 Year </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">7</div>
                        <div class="omiTxt">Churn ratio of ~ 0.67 </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">8</div>
                        <div class="omiTxt">Average cash level is ~ 10% </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 mb-5 d-flex animateThis slide-top omiBox">
                        <div class="omiNo">9</div>
                        <div class="omiTxt">Focused unbiased active portfolio management </div>
                    </div>
                </div>

                <p class="mb-0 font-italic"><small>* The investment objectives are indicative and there are no assurances that they will be achieved. </small></p>
            </div>

        </div>

    </section>

    <section class="mrktviewSection">

        <div class="container">

            <div class="aboutContent mx-auto mb-5 text-justify animateThis slide-top">
                <h2 class="title text-center text-uppercase font-weight-bold mb-4">Market Views</h2>
                <p class="text-center mb-4">Gain insights relating to the economy, markets, and the investment environment, from our <strong class="d-inline-block">Director, CEO & CIO Mr. Hiren Ved</strong></p>
            </div>

            <div class="row row-cols-lg-5 row-cols-sm-3 row-cols-2 justify-content-center mx-auto mrktviewList">
                <%--			<div class="col mb-4 mb-lg-0 animateThis fade-in"><a href="https://youtu.be/S-10yzT9MT0" target="_blank" class="btn rounded-pill d-block">February 2021</a></div>
				<div class="col mb-4 mb-lg-0 animateThis fade-in"><a href="https://www.youtube.com/watch?v=TgNVWAF0Ip4" target="_blank" class="btn rounded-pill d-block">January 2021</a></div>
				<div class="col mb-4 mb-lg-0 animateThis fade-in"><a href="https://youtu.be/I_nL_JL7D18" target="_blank" class="btn rounded-pill d-block">December 2020</a></div>
				<div class="col mb-4 mb-lg-0 animateThis fade-in"><a href="https://youtu.be/7fCdVXR06YU" target="_blank" class="btn rounded-pill d-block">November 2020</a></div>
				<div class="col mb-4 mb-lg-0 animateThis fade-in"><a href="https://youtu.be/w-qqKRJ2-KE" target="_blank" class="btn rounded-pill d-block">October 2020</a></div>--%>
                <asp:Literal ID="ltrTopFiveMarketViews" runat="server"></asp:Literal>
            </div>

        </div>

    </section>

    <footer class="mt-lg-4">
        <div class="container text-center text-lg-left animateThis fade-in">

            <div class="row">
                <div class="col-lg-auto">
                    Copyright &copy; 2021 Alchemy. All Rights Reserved.<br>
                    <a href="https://www.alchemycapital.com/disclaimer-disclosure.aspx?utm_source=DV360&utm_medium=Display&utm_campaign=Ascent-2&utm_content=globe" target="_blank">Disclaimer</a> -  Investments in securities market are subject to market risks.
                </div>
                <div class="col-lg-auto pl-lg-5 ml-lg-5">
                    <div class="row mx-auto no-gutters socialBtns">
                        <%--<div class="col"><a href="https://www.facebook.com/alchemycapital/" target="_blank">
                            <img src="/landing/images/ic_fb.png"></a></div>--%>
                        <div class="col"><a href="https://www.youtube.com/channel/UCFvIGco1pAKZyVIZFGWf2ZQ/featured" target="_blank">
                            <img src="/landing/images/ic_yt.png"></a></div>
                        <div class="col"><a href="https://www.linkedin.com/company/alchemy-capital-management" target="_blank">
                            <img src="/landing/images/ic_li.png"></a></div>
                    </div>
                </div>
                <div class="col-lg-auto text-lg-right ml-lg-auto">
                    <a href="https://www.alchemycapital.com/?utm_source=DV360&utm_medium=Display&utm_campaign=Ascent-2&utm_content=globe">www.alchemycapital.com</a><br>
                    <a href="https://www.kwebmaker.com" target="_blank" class="kweb">Kwebmaker&trade;</a>
                </div>
            </div>

        </div>
    </footer>

    <div class="stickBtn">
        <button type="button" class="btn floatBtn text-uppercase d-flex align-items-center justify-content-center" id="gotoForm"><strong>
            <img src="/landing/images/arw.png" class="d-block mx-auto mb-1">
            Request a Callback</strong></button>
    </div>

    <style>
        .ErrorValidation {
            display: none;
            color: #dc3545 !important;
            margin-bottom: 10px;
        }
    </style>

    <script src="/landing/js/jquery-3.5.1.min.js"></script>
    <script src="/landing/js/bootstrap.bundle.min.js"></script>


    <script>
        /*  ---------- ON-SCROLL ANIMATION ------------  */
        $(window).on('load', function () {
            $(".bannerTextArea h1, .bannerTextArea h2").addClass("in-view");
        });


        var $animation_elements = $('.animateThis');
        var $window = $(window);

        function check_if_in_view() {
            var window_height = $window.height();
            var window_top_position = $window.scrollTop();
            var window_bottom_position = (window_top_position + window_height - 30);

            $.each($animation_elements, function () {
                var $element = $(this);
                var element_height = $element.outerHeight();
                var element_top_position = $element.offset().top + 30;
                var element_bottom_position = (element_top_position + element_height);

                if ((element_top_position <= window_bottom_position)) {
                    $element.addClass('in-view');
                } else {
                    $element.removeClass('in-view');
                }
            });
        }
        $window.on('scroll resize', check_if_in_view);

        /* ---------- Sticky Btn ----------- */

        function sticky_relocate() {
            var window_top = $(window).scrollTop();
            var div_top = $('#stickyBtnAnchor').offset().top;
            if (window_top > div_top) {
                $('.stickBtn').addClass('stick');
            } else {
                $('.stickBtn').removeClass('stick');
            }
        }
        $(function () {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
        });

        $('#gotoForm').click(function () {
            $('html,body').animate({ scrollTop: $('body').offset().top });
        });

    </script>


    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-51603925-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-51603925-1');


    </script>


    <!--
Event snippet for Remarketing-LP-1 on : Please do not remove.
Place this snippet on pages with events you’re tracking. 
Creation date: 03/16/2021
-->
    <script>
        gtag('event', 'conversion', {
            'allow_custom_scripts': true,
            'send_to': 'DC-10745315/invmedia/remar0+standard'
        });
    </script>

    <noscript>
        <img src="https://ad.doubleclick.net/ddm/activity/src=10745315;type=invmedia;cat=remar0;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ord=1?" width="1" height="1" alt="" />
    </noscript>
    <!-- End of event snippet: Please do not remove -->


        <!-- Global site tag (gtag.js) - Google Ads: 10797805103 -->
<script async="async" src="https://www.googletagmanager.com/gtag/js?id=AW-10797805103"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'AW-10797805103');
</script>
</body>
</html>

