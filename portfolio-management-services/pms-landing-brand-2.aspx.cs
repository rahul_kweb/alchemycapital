﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class portfolio_management_services_pms_landing_brand_2 : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        BindHomeStatistic();
        BindTopFiveMarketViews();
    }

    public void BindTopFiveMarketViews()
    {
        //int Id = 6;
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'BindMarketsTopThree'");
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["Type"].ToString() == "Pdf")
            {
                strbuild.Append("<div class='col mb-4 mb-lg-0 animateThis fade-in'><a href='Content/uploads/Pdf/" + dr["Pdf"].ToString() + "' target='_blank' class='btn rounded-pill d-block'>" + dr["Month"].ToString() + " " + dr["Year"].ToString() + "</a></div>");
            }
            else
            {

                strbuild.Append("<div class='col mb-4 mb-lg-0 animateThis fade-in'><a href=" + dr["Url"].ToString() + "' target='_blank' class='btn rounded-pill d-block'>" + dr["Month"].ToString() + " " + dr["Year"].ToString() + "</a></div>");
            }


        }

        ltrTopFiveMarketViews.Text = strbuild.ToString();
    }

    public void BindHomeStatistic()
    {
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_HomeStatistic 'get'");
        foreach (DataRow dr in dt.Rows)
        {
            //strbuild.Append("<div class='col-md-4'>");
            //strbuild.Append("<div class='ts-facts'><span class='facts-icon'><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /></span>");
            //strbuild.Append("<div class='ts-facts-content'>");
            //strbuild.Append("<h4 class='ts-facts-num'><span class='count1'>" + dr["Counting"].ToString() + "</span></h4>");
            //strbuild.Append("<p class='facts-desc'>" + dr["Description"].ToString() + "</p>");
            //strbuild.Append("</div>");
            //strbuild.Append("</div>");
            //strbuild.Append("</div>");

            strbuild.Append("<div class='col-auto mb-3 mb-md-0 animateThis fade-in'>");
            strbuild.Append("<img src='/Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' class='d-block mx-auto mb-3'> ");
            strbuild.Append("<p class='mb-0'><strong class='number font-weight-bold d-block'>" + dr["Counting"].ToString() + "</strong>" + dr["Description"].ToString() + "</p>");
            strbuild.Append("</div>");

        }
        ltrHomeStatistic.Text = strbuild.ToString();
    }

}