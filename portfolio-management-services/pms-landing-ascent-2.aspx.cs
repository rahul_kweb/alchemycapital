﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class portfolio_management_services_pms_landing_ascent_2 : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        BindTopFiveMarketViews();
    }


    public void BindTopFiveMarketViews()
    {
        //int Id = 6;
        StringBuilder strbuild = new StringBuilder();
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'BindMarketsTopThree'");
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["Type"].ToString() == "Pdf")
            {
                strbuild.Append("<div class='col mb-4 mb-lg-0 animateThis fade-in'><a href='Content/uploads/Pdf/" + dr["Pdf"].ToString() + "' target='_blank' class='btn rounded-pill d-block'>" + dr["Month"].ToString() + " " + dr["Year"].ToString() + "</a></div>");
            }
            else
            {

                strbuild.Append("<div class='col mb-4 mb-lg-0 animateThis fade-in'><a href=" + dr["Url"].ToString() + "' target='_blank' class='btn rounded-pill d-block'>" + dr["Month"].ToString() + " " + dr["Year"].ToString() + "</a></div>");
            }


        }

        ltrTopFiveMarketViews.Text = strbuild.ToString();
    }

}