﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class portfolio_management_services_pms_landing_brand_3 : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnutm_source.Value = Request.QueryString["utm_source"];

        hdnutm_medium.Value = Request.QueryString["utm_medium"];

        hdnutm_campaign.Value = Request.QueryString["utm_campaign"];

        hdnutm_term.Value = Request.QueryString["utm_term"];

        hdnutm_content.Value = Request.QueryString["utm_content"];

        hdnutm_device.Value = Request.QueryString["utm_device"];

        hdnutm_referrer.Value = Request.QueryString["utm_referrer"];
    }



    protected void btn_Req_Submit_Click(object sender, EventArgs e)
    {


        if (CheckSave())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_UtmParameter"))
            {


                //string referrerVal = string.Empty;
                //HttpCookie nameCookie = Request.Cookies["referrer"];
                //if (nameCookie != null)
                //{
                //    referrerVal = nameCookie["referrer"].ToString();
                //}

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", txtName.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@Email", txtEmail.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@Phone", txtPhone.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@Interested", ddlIntrest.SelectedItem.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@City", txtCity.Text.ToString().Trim());
                cmd.Parameters.AddWithValue("@utm_source", hdnutm_source.Value);
                cmd.Parameters.AddWithValue("@utm_medium", hdnutm_medium.Value);
                cmd.Parameters.AddWithValue("@utm_campaign", hdnutm_campaign.Value);
                cmd.Parameters.AddWithValue("@utm_content", hdnutm_content.Value);
                cmd.Parameters.AddWithValue("@utm_device", hdnutm_device.Value);
                cmd.Parameters.AddWithValue("@utm_term", hdnutm_term.Value);

                cmd.Parameters.AddWithValue("@Referrer", hdnutm_referrer.Value);

                //if (hdnutm_source.Value != "" || hdnutm_source.Value != string.Empty || hdnutm_medium.Value != "" || hdnutm_medium.Value != string.Empty || hdnutm_campaign.Value != "" || hdnutm_campaign.Value != string.Empty || hdnutm_content.Value != "" || hdnutm_content.Value != string.Empty || hdnutm_device.Value != "" || hdnutm_device.Value != string.Empty || hdnutm_term.Value != "" || hdnutm_term.Value != string.Empty)
                //{
                //cmd.Parameters.AddWithValue("@Referrer", referrerVal);
                //}

                if (utility.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Request a callback </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + txtName.Text.ToString().Trim() + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email Address : </strong></td> <td> " + txtEmail.Text.ToString().Trim() + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Phone : </strong></td> <td> " + txtPhone.Text.ToString().Trim() + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Interest : </strong></td> <td> " + ddlIntrest.SelectedItem.Text.ToString().Trim() + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>City : </strong></td> <td> " + txtCity.Text.ToString().Trim() + " </td> </tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["RequestUsEmail"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    //utility.SendEmail(sb.ToString(), emailid, "Request A Callback", "", null);


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "success", "window.location ='/thank-you.aspx';", true);


                    //Response.Redirect("/thank-you.aspx");



                }
                else
                {

                }
            }
        }

    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        string email = txtEmail.Text;
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match matchemail = regex.Match(email);


        string Phone = txtPhone.Text;

        Regex regex1 = new Regex("^[1-9]{1}[0-9]{0,11}$");
        Match matchphone = regex1.Match(Phone);

        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Name, ";
        }

        if (txtPhone.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Phone, ";
        }

        if (!matchphone.Success)
        {
            isOK = false;
            message += "Valid Phone no, ";
        }


        if (txtEmail.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Email, ";
        }
        if (!matchemail.Success)
        {
            isOK = false;
            message += "Valid Email, ";
        }


        if (txtCity.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "City, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "CheckSave", "alert('Please fill following fields')", true);
            lblMsg.Text = "Please fill following fields <br />" + message;
            lblMsg.ForeColor = System.Drawing.Color.Red;
        }
        return isOK;
    }
}