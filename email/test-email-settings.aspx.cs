﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Drawing;

public partial class test_email_settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ResetResult();
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        try
        {
            string[] to = txtTo.Text.Split(',');
            string from = txtFrom.Text;
            string[] cc = txtCC.Text.Split(',');
            string[] bcc = txtBCC.Text.Split(',');
            string subject = "TEST MAIL";
            string body = txtBody.Text;

            string email_user = txtUsername.Text;
            string email_pwd = txtPassword.Text;

            MailMessage message = new MailMessage();
            message.From = new MailAddress(from);
            foreach (string to_item in to)
            {
                if (!string.IsNullOrEmpty(to_item))
                {
                    message.To.Add(to_item.Trim());
                }
            }

            foreach (string cc_item in cc)
            {
                if (!string.IsNullOrEmpty(cc_item))
                {
                    message.CC.Add(cc_item.Trim());
                }
            }

            foreach (string bcc_item in bcc)
            {
                if (!string.IsNullOrEmpty(bcc_item))
                {
                    message.Bcc.Add(bcc_item.Trim());
                }
            }

            message.Subject = subject;
            message.Body = chkIsBodyHtml.Checked ? string.Format("<b>{0}</b>", body) : body;
            message.IsBodyHtml = chkIsBodyHtml.Checked;
            SmtpClient client = new SmtpClient(txtServerName.Text);
            client.Port = int.Parse(txtPort.Text);
            client.UseDefaultCredentials = chkUseDefaultCredentials.Checked;
            client.EnableSsl = chkEnableSsl.Checked;
            client.Credentials = new NetworkCredential(email_user, email_pwd);


            if (chkIsWriteToDisk.Checked)
            {
                client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                client.PickupDirectoryLocation = Server.MapPath("~/emails");
            }
            else
            {
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
            }

            client.Send(message);

            SetResult("Success", Color.Green);
        }
        catch (Exception ex)
        {
            //lblResponse.Text = "Failed due to!!<br />" + ex.Message + "<br />" + ex.StackTrace + "<br />" + ex.Source + "<br />";
            SetResult("Failed due to!!<br />" + GetErrorMessage(ex), Color.Red);
        }
    }

    public void SetResult(string text, Color color)
    {
        lblResult.Text = string.Format("Result : {0}", text);
        lblResult.ForeColor = color;
    }

    public void ResetResult()
    {
        lblResult.Text = "Result : ";
        lblResult.ForeColor = Color.Black;
    }

    public string GetErrorMessage(Exception ex)
    {
        Exception exp = ex;
        string error = string.Empty;

        if (ex != null)
        {
            error += exp.Message;
            error += "<br />=========================================";
            error += exp.StackTrace;

            exp = exp.InnerException;
            while (exp != null)
            {
                error += "<br />=========================================";
                error += exp.Message;
                error += "<br />=========================================";
                error += exp.StackTrace;
            }
        }

        return error;
    }
}