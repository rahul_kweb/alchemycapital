﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="sitemap.aspx.cs" Inherits="Sitemap" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" Runat="Server">

    <link rel="alternate" href="https://www.alchemycapital.com/sitemap.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/sitemap.aspx" hreflang="x-default" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="banner-area" id="banner-area" style="background-image: url(Content/images/banner/banner2.jpg); background-repeat: no-repeat;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col">
                    <div class="banner-heading">
                        <h1 class="banner-title">Sitemap</h1>
                        <ol class="breadcrumb">
                            <li><a rel="canonical" href="/">Home</a></li>
                            <li>Sitemap</li>
                        </ol>
                    </div>
                </div>
                <!-- Col end-->
            </div>
            <!-- Row end-->
        </div>
        <!-- Container end-->
    </div>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">

        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 mrt-30">
                    <h2 class="section-title">Sitemap</h2>
                </div>
            </div>
           <!-- Title row end-->
            <div class="row justify-content-between">
                <div class="col-md-6 col-lg-auto mb-3">
                    <div class="sitemap_wrp">
                        <h2>PEOPLE & CULTURE</h2>
                        <div class="">
                            <ul>
                            <li><a href="/people-and-culture/hiren-ved.aspx">Hiren Ved</a></li>
                                <li><a href="/people-and-culture/lashit-sanghvi.aspx">Lashit Sanghvi</a></li>                              
                                <li><a href="/people-and-culture/rakesh-jhunjhunwala.aspx">Rakesh Jhunjhunwala</a></li>
                                <li><a href="/people-and-culture/ashwin-kedia.aspx">Ashwin Kedia</a></li>
                                <li><a href="/people-and-culture/neeraj-roy.aspx">Neeraj Roy</a></li>
                                <li><a href="/people-and-culture/manu-parpia.aspx">Manu Parpia</a></li>
                                <li><a href="/people-and-culture/rajiv-agarwal.aspx">Rajiv Agarwal</a></li>
                                <li><a href="/people-and-culture/pooja-keswani.aspx">Pooja Keswani</a></li>
                                <li><a href="/people-and-culture/sameer-tirani.aspx">Sameer Tirani</a></li>
                                <li><a href="/people-and-culture/chirag-pandya.aspx">Chirag Pandya</a></li>
                                <li><a href="/people-and-culture/mohit-bhagat.aspx">Mohit Bhagat</a></li>
                                <li><a href="/people-and-culture/amit-nadekar.aspx">Amit Nadekar</a></li>
                               <%-- <li><a href="/people-and-culture/vikas-kumar.aspx">Vikas Kumar</a></li>--%>
                                <li><a href="/people-and-culture/seshadri-sen.aspx">Seshadri Sen</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-auto mb-3">
                    <div class="sitemap_wrp">
                        <h2>PRODUCTS</h2>
                        <div class="">
                            <ul>                               
                                <li><a href="/portfolio-management-services/alchemy-high-growth.aspx">Alchemy High Growth</a></li>
                                <li><a href="/portfolio-management-services/alchemy-high-growth-select-stock.aspx">Alchemy High Growth Select Stock</a></li>
                                <li><a href="/portfolio-management-services/alchemy-leaders.aspx">Alchemy Leaders</a></li>
                                <li><a href="/portfolio-management-services//alchemy-ascent.aspx">Alchemy Ascent</a></li>
                                <li><a href="/aif/alchemy-leaders-of-tomorrow.aspx">Alchemy Leaders of Tomorrow – (AIF CAT – III)</a></li>                               
                                <li><a href="/portfolio-management-services.aspx">Our Products - PMS</a></li>
                            </ul>
                        </div>
                    </div>
                      <div class="sitemap_wrp sitemap_content">
                        <h2>WHO WE ARE</h2>
                        <div class="">
                            <ul>
                                <li><a href="/overview.aspx">Overview</a></li>
                                <li><a href="/mission-and-principles.aspx">Mission & Principles</a></li>
                                <li><a href="/people-and-culture.aspx">People & Culture</a></li>
                                <li><a href="/csr.aspx">CSR</a></li>
                                <li><a href="http://www.alchemysingapore.com/" target="_blank">Alchemy Singapore</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-auto mb-3">
                    <div class="sitemap_wrp">
                        <h2>SUPPORT</h2>
                        <div>
                            <ul>
                                <li><a href="https://www.alchemycapital.com/login_alchemy.aspx" target="_blank">Login</a></li>
                                <li><a href="/contact.aspx">Contact Us</a></li>
                                <li><a href="/our-process.aspx">Our Process</a></li>
                                <li><a href="/why-us.aspx">Why Us</a></li>
                                <li><a href="/career.aspx">Career</a></li>
                                <li><a href="/disclaimer.aspx">Disclaimer</a></li>
                            </ul>
                        </div>
                    </div>
                      <div class="sitemap_wrp sitemap_content">
                        <h2>INSIGHTS</h2>
                        <div class="">
                            <ul>
                                <li><a href="/thought-leadership.aspx">Thought & Leadership</a></li>
                                <li><a href="/knowledge-center.aspx">Knowledge Center</a></li>
                                  <li><a href="/market-views.aspx">Market Views</a></li>
                                <li><a href="/media-center.aspx">Media Center</a></li>  
                            </ul>
                        </div>
                    </div>
                </div>
 
            </div>

        </div>
    </section>
    <div class="gap-60"></div>
        <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
        <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
        <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

        <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Sitemap",
    "item": "https://www.alchemycapital.com/sitemap.aspx"  
  }]
}
</script>


</asp:Content>

