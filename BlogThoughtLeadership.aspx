﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="BlogThoughtLeadership.aspx.cs" Inherits="BlogThoughtLeadership" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
 <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="banner-heading">
                    <h1 class="banner-title">Thought Leadership</h1>
                    <ol class="breadcrumb">
                        <li><a rel="canonical" href="~/">Home</a></li>
                        <li>Thought Leadership</li>
                        <%--<li><asp:Label ID="lblHead" runat="server"></asp:Label></li>--%>
                    </ol>
                </div>
            </div>
            <!-- Col end-->
        </div>
        <!-- Row end-->
    </div>
    <!-- Container end-->
</div>
           </ItemTemplate>
    </asp:Repeater>

<!-- Banner area end-->
<section class="main-container mrt-40" id="main-container">
    <asp:HiddenField ID="hdnId"  runat="server"/>
    <div class="container" id="blog">


        <h2 class="column-title title-small text-center"><asp:Literal ID="ltrHeading" runat="server"></asp:Literal></h2>
        <!-- Title row end-->
        <div class="row">
            <div class="col-sm-12">
               <asp:Literal ID="ltrBlogDetail" runat="server"></asp:Literal>
            </div>
        </div>
    </div>

</section>

<link rel="stylesheet" type="text/css" href="~/Content/Resource/website/css/inner_page.css">


</asp:Content>

