﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="pooja-keswani.aspx.cs" Inherits="people_and_culture_pooja_keswani" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
            <title>Pooja Keswani – Chief Operating Officer at Alchemy Capital</title>
    <meta name="description" content="Ms. Pooja Keswani is a Chartered Accountant with three decades of experience across sectors such as FMCG, BPO & Financial Services."/>

    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description" content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/people-and-culture/pooja-keswani.aspx" />

    <link rel="canonical" href="https://www.alchemycapital.com/people-and-culture/pooja-keswani.aspx" />

    <link rel="alternate" href="https://www.alchemycapital.com/people-and-culture/pooja-keswani.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/people-and-culture/pooja-keswani.aspx" hreflang="x-default" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h2 class="banner-title">Who We Are</h2>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                      <li><a href="/people-and-culture.aspx"> People &amp; Culture</a></li>
                                    <li>Pooja Keswani</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">
        <div class="container">
            <!-- Title row end-->
            <div class="row justify-content-center team_box">

                <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>


            </div>
    </section>
        <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

       <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "People & Culture",
    "item": "https://www.alchemycapital.com/people-and-culture.aspx"  
  },
        {
    "@type": "ListItem", 
    "position": 3, 
    "name": "Pooja Keswani",
    "item": "https://www.alchemycapital.com/people-and-culture/pooja-keswani.aspx"  
  }]
}
    </script>



</asp:Content>



