﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class people_and_culture_hiren_ved : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindInnerBanner(3);
        BindDescription();
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindDescription()
    {
        StringBuilder strBuild = new StringBuilder();
        StringBuilder strHeading = new StringBuilder();

        DataTable dt = new DataTable();
        string SlugUrl = "hiren-ved";

        dt = utility.Display("Exec Proc_PeopleCulture 'bindPeopleBySlugUrl',0,0,'','','','',0,'','" + SlugUrl + "'");

        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["Image"].ToString() != string.Empty)
            {
                strBuild.Append("<div class='col-md-auto col-sm-auto col-12 text-center'>");
                strBuild.Append("<img class='img-fluid' src='/Content/uploads/Team/" + dt.Rows[0]["Image"].ToString() + "'>");
                strBuild.Append("</div>");
            }

            strBuild.Append("<div class='col'><h2 class='team_name'>" + dt.Rows[0]["Name"].ToString() + " - <span class='b-team-2__category'> " + dt.Rows[0]["Designation"].ToString() + " </span></h2>");
            strBuild.Append(dt.Rows[0]["Description"].ToString());

            strHeading.Append("</div>");


            ltrDescription.Text = strBuild.ToString();
        }


    }

}