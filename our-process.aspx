﻿<%@ Page  Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="our-process.aspx.cs" Inherits="OurProcess" %>


<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Portfolio Management Process | Alchemy Capital</title>
   <meta name="description" content="Alchemy Capital follows a process that begins with ideation, continues with management liaison, financial research, and concludes with frequent evaluations and exit strategies."/>

<meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/our-process.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/our-process.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/our-process.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/our-process.aspx" hreflang="x-default" />


</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h2 class="banner-title">Our Process</h2>
                                <ol class="breadcrumb">
                                    <li><a  href="/">Home</a></li>
                                    <li>Our Process</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">

        <div class="container">


            <h1 class="column-title title-small text-center">Our Process</h1>
            <!-- Title row end-->
            <div class="row processBox">
                <asp:Literal ID="ltrOverview" runat="server"></asp:Literal>
                <%--                 <div class="col-md-4 mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                         <i class="icon icon-chart-bars"></i>
                        </div>-->
                        
                        <div class="ts-service-content">
                           <h3 class="service-title"> <small class="proicon"><i class="icon icon-lightbulb"></i> </small>Idea Generation</h3>
                           <p>While a typical universe consists of about 800 stocks, only 250 of them are covered by the sell side. Alchemy filters the appropriate stocks, depending on investment patterns and inputs from industry experts, thereby designing a parallel universe of only the potent stocks.
							</p>
                        </div>
                        
                        <div class="circleDiv ">
                        1
                        </div>
                        
                        <div class="arrow upArrow">
                        <div>
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 277.5 39" enable-background="new 0 0 277.5 39" xml:space="preserve">
<defs>
	<path id="SVGID_1_" d="M4,16.429h243.374c-3.807-5.678-5.703-9.381-5.896-14.428c8.658,8.701,19.914,13.234,33.516,17.496
		c-13.602,4.08-24.361,9.064-33.516,17.502c0.584-5.486,2.162-8.799,6.014-14.838H4V16.429L4,16.429z"/>
</defs>
<use xlink:href="#SVGID_1_"  overflow="visible" fill="#ffc000"/>
<clipPath id="SVGID_2_">
	<use xlink:href="#SVGID_1_"  overflow="visible"/>
</clipPath>
</svg>
					</div>
                        </div>
                        
                        
                     </div>
                     <!-- Service1 end-->
                  </div>
                  <!-- Col 1 end-->
                  <div class="col-md-4 mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                         <i class="icon icon-chart-bars"></i>
                        </div>-->
                        <div class="ts-service-content">
                           
                           <h3 class="service-title"><small class="proicon"><i class="icon icon-users"></i>  </small>Company / Management Meets </h3>
                           <p>The next step after having zeroed in on companies with distinctive competitive advantage and scalability is to touch base with their managerial boards to discuss preliminary financial models and interact with their stakeholders. </p>
                           <p>Feedback is then collected through a network of banking channels and business experts. The idea is to see things from a vantage point to get a more rounded perspective on potential association and business dynamics.</p>
                        </div>
                        
                        <div class="circleDiv ">
                        2
                        </div>
                        
                        <div class="arrow upArrow">
                        <div>
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 277.5 39" enable-background="new 0 0 277.5 39" xml:space="preserve">
<defs>
	<path id="SVGID_1_" d="M4,16.429h243.374c-3.807-5.678-5.703-9.381-5.896-14.428c8.658,8.701,19.914,13.234,33.516,17.496
		c-13.602,4.08-24.361,9.064-33.516,17.502c0.584-5.486,2.162-8.799,6.014-14.838H4V16.429L4,16.429z"/>
</defs>
<use xlink:href="#SVGID_1_"  overflow="visible" fill="#ffc000"/>
<clipPath id="SVGID_2_">
	<use xlink:href="#SVGID_1_"  overflow="visible"/>
</clipPath>
</svg>
					</div>
                        </div>
                        
                     </div>
                     <!-- Service2 end-->
                  </div>
                  <!-- Col 2 end-->
                  <div class="col-md-4 mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                          <i class="icon icon-chart-bars"></i>
                        </div>-->
                        <div class="ts-service-content">
                           <h3 class="service-title">
                           <small class="proicon"><i class="icon icon-pie-chart"></i></small>In-Depth Financial Analysis & Evaluation</h3>
                           <p>The intelligence is now pitched to the core investment team. Experience and insight help to gauge whether the findings collected through company meets and industry analysts complement Alchemy’s investment patterns and philosophy.</p>
                           <p>Should the equities satisfy both quantitative and qualitative standards and prove conducive to a favourable risk-return model for the investor, the company will be included in Alchemy’s Investment Universe.</p>
                        </div>
                        
                        <div class="circleDiv ">
                        3
                        </div>
                        
                        <div class="arrow downArrow">
                        <div>
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 277.5 39" enable-background="new 0 0 277.5 39" xml:space="preserve">
<defs>
	<path id="SVGID_1_" d="M4,16.429h243.374c-3.807-5.678-5.703-9.381-5.896-14.428c8.658,8.701,19.914,13.234,33.516,17.496
		c-13.602,4.08-24.361,9.064-33.516,17.502c0.584-5.486,2.162-8.799,6.014-14.838H4V16.429L4,16.429z"/>
</defs>
<use xlink:href="#SVGID_1_"  overflow="visible" fill="#ffc000"/>
<clipPath id="SVGID_2_">
	<use xlink:href="#SVGID_1_"  overflow="visible"/>
</clipPath>
</svg>
					</div>
                        </div>
                        
                     </div>
                     <!-- Service3 end-->
                  </div>
                  <!-- Col 3 end-->
                  <div class="col-md-4 order-md-9 mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                          <i class="icon icon-chart-bars"></i>
                        </div>-->
                        <div class="ts-service-content ">
                           <h3 class="service-title"><small class="proicon"><i class="icon icon-comments"></i> </small>Qualitative Feedback</h3>
                           <p>Professionals at Alchemy conduct quarterly assessment of the companies through different trade and industry channels to ensure security on investments and the investor’s long term wealth creation goals are never compromised.  </p>
                          
                        </div>
                        
                        <div class="circleDiv ">
                        4
                        </div>
                        
                        <div class="arrow leftArrow">
                        <div>
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 277.5 39" enable-background="new 0 0 277.5 39" xml:space="preserve">
<defs>
	<path id="SVGID_1_" d="M4,16.429h243.374c-3.807-5.678-5.703-9.381-5.896-14.428c8.658,8.701,19.914,13.234,33.516,17.496
		c-13.602,4.08-24.361,9.064-33.516,17.502c0.584-5.486,2.162-8.799,6.014-14.838H4V16.429L4,16.429z"/>
</defs>
<use xlink:href="#SVGID_1_"  overflow="visible" fill="#ffc000"/>
<clipPath id="SVGID_2_">
	<use xlink:href="#SVGID_1_"  overflow="visible"/>
</clipPath>
</svg>
					</div>
                        </div>
                        
                     </div>
                     <!-- Service3 end-->
                  </div>
                  <!-- Col 4 end-->
                  
                  <div class="col-md-4 order-md-4  mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                          <i class="icon icon-chart-bars"></i>
                        </div>-->
                        <div class="ts-service-content">
                           <h3 class="service-title"><small class="proicon"><i class="icon icon-star2"></i> </small>Portfolio Construction & Ongoing Review</h3>
                           <p>Alchemy has integrated the old adage of “Better be safe than sorry” in its framework. Alchemy monitors all the companies in its portfolio on a regular basis to tap into recent market developments and avert any risk or financial adversity. </p>
                        </div>
                        
                        <div class="circleDiv ">
                        5
                        </div>
                        
                        <div class="arrow leftArrow">
                        <div>
                      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 277.5 39" enable-background="new 0 0 277.5 39" xml:space="preserve">
<defs>
	<path id="SVGID_1_" d="M4,16.429h243.374c-3.807-5.678-5.703-9.381-5.896-14.428c8.658,8.701,19.914,13.234,33.516,17.496
		c-13.602,4.08-24.361,9.064-33.516,17.502c0.584-5.486,2.162-8.799,6.014-14.838H4V16.429L4,16.429z"/>
</defs>
<use xlink:href="#SVGID_1_"  overflow="visible" fill="#ffc000"/>
<clipPath id="SVGID_2_">
	<use xlink:href="#SVGID_1_"  overflow="visible"/>
</clipPath>
</svg>
					</div>
                        </div>
                     </div>
                     <!-- Service3 end-->
                  </div>
                  <!-- Col 5 end-->
                  
                  <div class="col-md-4 order-md-1 mrb-60">
                     <div class="ts-service-box">
                        <!--<div class="proicon">
                          <i class="icon icon-chart-bars"></i>
                        </div>-->
                        <div class="ts-service-content">
                           <h3 class="service-title"><small class="proicon"><i class="icon icon-power-switch"></i> </small>Exit Strategy</h3>
                           <p>Though Alchemy doesn’t cap a specific exit price on stocks, the company will have to cede shares should the risk-reward balance not favour Alchemy or better prospects come up on the horizon</p>
                           <p>Despite an impeccable track record and a strong foundation built on knowledge and excellence, Alchemy is committed to on-going growth and learning. This ensures sound investment processes geared towards performance that regularly deliver on customers’ expectations.</p>
                        </div>
                        
                        <div class="circleDiv ">
                        6
                        </div>
                     </div>
                     <!-- Service3 end-->
                  </div>
                  <!-- Col 6 end-->
                --%>
            </div>

        </div>

      <a class="to_bottom "  id="callback" style="display: block;">
       <i class="fa fa-phone" aria-hidden="true"></i>
        <%--<button class="contact_us_btn" data-spy="affix" data-offset-top="10" title="Request a Callback"><i class="fa fa-phone" aria-hidden="true"></i></button>--%>
    </a>

    </section>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->


    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Our Process",
    "item": "https://www.alchemycapital.com/our-process.aspx"  
  }]
}
</script>

        <%-- Added on 25-06_21--%>
    <script>

        $('.ts-service-content').css('height','250px');
        
        $('.link-more').click(function (e) {
            e.preventDefault();
            

            if ($(this).children('span').text() == "Read More") {
                var newHeight = $(this).prev('.ts-service-content').children('div').height();

                $(this).prev('.ts-service-content').animate({ height: newHeight + 'px' });
                $(this).children('span').text('Read Less');
            } else {
                $(this).prev('.ts-service-content').animate({ height: '250px' });
                $(this).children('span').text('Read More');
            }
            


        })
    </script>

</asp:Content>

