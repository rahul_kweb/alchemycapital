﻿<%@ Application Language="C#" %>

<script RunAt="server">



    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

        //        if (!Request.IsLocal && !Request.IsSecureConnection)
        //{
        //    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
        //    Response.Redirect(redirectUrl, false);
        //    HttpContext.Current.ApplicationInstance.CompleteRequest();
        //}

    }

    //static void RegisterRoutes(RouteCollection routes)
    //{
    //    routes.MapPageRoute("Detailpage2", "{slug}", "~/details.aspx");
    //}

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown




    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

        var serverError = Server.GetLastError() as HttpException;


    }



    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

</script>
