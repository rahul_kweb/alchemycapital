﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Overview : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindOverviewContent();
            BindInnerBanner(1);
        }
    }


    public void BindInnerBanner(int Id)
    {
        
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }
     

    }


    public string BindOverviewContent()
    {
       
        StringBuilder strBuild = new StringBuilder();
        DataSet ds = new DataSet();
        ds = utility.Display1("Exec Proc_Milestones 'bindOverviewPage'");
        if (ds.Tables.Count > 0)
        {
            ltrOverview.Text = ds.Tables[0].Rows[0]["Description"].ToString();

            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                strBuild.Append("<div class='timeline__item'>");
                strBuild.Append("<div class='timeline__content'>");
                strBuild.Append("<h2>" + dr["Year"].ToString() + "</h2>");
                strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                strBuild.Append("</div>");
                strBuild.Append("</div>");
            }
            ltrMilestones.Text = strBuild.ToString();
        }

        return ltrMilestones.Text;
    }
}