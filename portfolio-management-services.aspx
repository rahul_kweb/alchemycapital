﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="portfolio-management-services.aspx.cs" Inherits="portfolio_management_services" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeaderContent">
    <title>Trusted PMS Services in India | Alchemy Capital</title>
    <meta name="description" content="Alchemy Capital is a leading Portfolio Management Services Company in India, assisting clients with PMS investments as well as the development of equity portfolios in large-cap, mid-cap, and small-cap companies." />


    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description" content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/portfolio-management-services.aspx" />

    <link rel="canonical" href="https://www.alchemycapital.com/portfolio-management-services.aspx" />

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services.aspx" hreflang="x-default" />

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Portfolio Management Services & AIF </h1>
                                <ol class="breadcrumb">
                                    <li><a rel="canonical" href="/">Home</a></li>
                                    <li>Our Products</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">
        <div class="container mrb-40">
            <%--        <h2 class="column-title title-small text-center">Portfolio Management Services & AIF</h2>--%>
            <asp:Literal ID="ltrOverview" runat="server"></asp:Literal>

            <h2 class="column-title title-small text-center">Our Products &amp; Services</h2>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTab" runat="server"></asp:Literal>
            <%--        <li>
                        <img src="Content/uploads/Icon/Regulation.png" alt='' aria-hidden='true' />
                        <span>REGULATORY INFORMATION</span></li>--%>
                    <%--          <li><i class="fa fa-bar-chart" aria-hidden="true"></i> <span>PMS</span></li>
          <li><i class="fa fa-line-chart" aria-hidden="true"></i> <span>AIF CAT-III</span></li>--%>
                </ul>
                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrdescription" runat="server"></asp:Literal>

            <%--        <div class="resp-tab-content" aria-labelledby="tab_item-2">
                        <ul>
                            <li>PMS  Disclosure Document <a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132478220119620140.pdf" target="_blank">
                                <img src="Content/uploads/Icon/pdf_icon.png.png" /></a></li>
                            <li>AIF Stewardship Code<a href="https://www.alchemycapital.com/content/uploads/pdf/pdf--132381505372076079.pdf" target="_blank">
                                <img src="Content/uploads/Icon/pdf_icon.png.png" /></a></li>
                            <li>Annual Return<a href="https://www.alchemycapital.com/content/uploads/pdf/Annual_Return_Form_Mgt_7.pdf" target="_blank">
                                <img src="Content/uploads/Icon/pdf_icon.png.png" /></a></li>
                            <li>AIF Stewardship Code Compliance Disclosure<a href="https://www.alchemycapital.com/content/uploads/pdf/AIF Stewardship Code Compliance Status July 20- June 21.pdf" target="_blank">
                                <img src="Content/uploads/Icon/pdf_icon.png.png" /></a></li>
                        </ul>



                    </div>--%>
                </div>


            </div>

            <div class="clearfix"></div>
        </div>

        <a class="to_bottom " id="callback" style="display: block;">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <%--<button class="contact_us_btn" data-spy="affix" data-offset-top="10" title="Request a Callback"><i class="fa fa-phone" aria-hidden="true"></i></button>--%>
        </a>
    </section>


    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Our Products",
    "item": "https://www.alchemycapital.com/portfolio-management-services.aspx"  
  }]
}

    </script>

</asp:Content>

