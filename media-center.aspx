﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="media-center.aspx.cs" Inherits="MediaCenter" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Latest News and Updates on Asset Management | Alchemy Capital</title>
   <meta name="description" content="Visit our media centre to see the most recent coverage from a variety of sources and publications. Take a look at what our experts have to say about Alchemy Capital."/>
  
     <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/media-center.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/media-center.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/media-center.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/media-center.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                               <h1 class="banner-title">Media Center</h1>
                                <ol class="breadcrumb">
                                    <li><a  href="/">Home</a></li>
                                     <li><a href="javascript:void(0);">Insights</a></li>
                                    <li>Media Center</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->

    <section class="main-container mrt-40" id="main-container">
        <div class="container mrb-40">

            <%--<h1 class="column-title title-small text-center">Media Center</h1>--%>
            <div class="clearfix mrb-40"></div>
           <%-- <p style="text-align: center;">
                All the latest news updates, market happenings, expert insights, videos and more to help you stay abreast with the industry and make an informed decision when it comes to asset management and expansion of your wealth.
            </p>--%>
             <asp:Literal ID="ltrUpperDisc" runat="server"></asp:Literal>

            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <%--<li><i class="fa fa-youtube-play" aria-hidden="true"></i> <span>TV Interview</span></li>
<li><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <span>Print Media</span></li>
<li><i class="fa fa-globe" aria-hidden="true"></i> <span>Online Media</span></li>--%>
                    <asp:Literal ID="ltrMediaTab" runat="server"></asp:Literal>
                </ul>
                <div class="resp-tabs-container">
                   
                    <%--<div>--%>
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                    <%--   <h3>TV Interviews</h3>
   <div class="currentVid">
      <iframe  src="https://www.youtube.com/embed/8xNcFvL-LuY?controls=1&rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
   </div>
   <h2 class="accordionHead">2015</h2>
   <ul class="newsLetter">
      <li class="newsData">
         <a href="https://www.youtube.com/watch?v=8xNcFvL-LuY" target="_blank">
            <div class="newsDate">10th SEPT 2015</div>
            <span> <strong>Hiren Ved in an interview with ET now on 25 08 2015   –   10-Sep-2015</strong></span>
         </a>
      </li>
      
      
      <li class="newsData">
         <a href="https://www.youtube.com/watch?v=h3ovqVwd6Dg&feature=youtu.be" target="_blank">
            <div class="newsDate">20th JUL 2015</div>
            <span> <strong>Hiren in an interview with ET now on 14-07-2015   –   20-Jul-2015</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="https://www.youtube.com/watch?v=XBMVsRXumPE" target="_blank">
            <div class="newsDate">16th JUL 2015</div>
            <span> <strong>Hiren in an interview with Bloomberg on 16th Jul’15   –   16-Jul-2015</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="https://www.youtube.com/watch?v=r7hh4a6DuFQ" target="_blank">
            <div class="newsDate">25th FEB 2015</div>
            <span> <strong>Budget Special | Market Makers With Hiren Ved on ET NOW   –   25-Feb-2015</strong></span>
         </a>
      </li>
      
      
   </ul>
   <h2 class="accordionHead">2014</h2>
   <ul class="newsLetter">
      <li class="newsData">
         <a href="https://www.youtube.com/watch?v=8xNcFvL-LuY" target="_blank">
            <div class="newsDate">22 DEC 2014</div>
            <span> <strong>Expect 2015 To Be A Strong Year For Indian Markets: Alchemy Capital   –   22-Dec-2014</strong></span>
         </a>
      </li>
      
      
      
      
   </ul>--%>

                    <%--  
</div>
<div>
   <h3>Print Media</h3>
   <h2 class="accordionHead">2011</h2>
   <ul class="newsLetter">
      <li class="newsData">
         <a href="pdf/2011/24website_17_nov_2011_mint_bloomberg_utv_hiren_ved.pdf" target="_blank">
            <div class="newsDate">17th NOV 2011</div>
            <span> <strong>Mint | Narrow down your investment thesis to a set of few sectors: Hiren Ved</strong></span>
         </a>
      </li>
      
      
      <li class="newsData">
         <a href="pdf/2011/20apw_may_25_economictimes.pdf" target="_blank">
            <div class="newsDate">25 MAY 2011</div>
            <span> <strong>Economic Times: Mohit Batra on PMS Profit's Cap gains not Business Income   –   25-May-2011</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="pdf/2011/196th_april_2011_cnbc_moneycontrol_hiren.pdf"  target="_blank">
            <div class="newsDate"> 06 APR 2011</div>
            <span> <strong>CNBC TV18: Midcaps, Small Caps likely to further scale up: Alchemy Capital: Hiren Ved   –   06-Apr-2011
</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="pdf/2011/18march17th2011_et_mohit.pdf" target="_blank">
            <div class="newsDate">17 MAR 2011</div>
            <span> <strong>Economic Times: Don't Allow Greed and Fear to Impact Your Stock Picks: Mohit Batra   –   17-Mar-2011</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="pdf/2011/19cnbc_march2011_lashit%20(2).pdf" target="_blank">
            <div class="newsDate">07 MAR 2011</div>
            <span> <strong>CNBC TV18: Experts explain trend identification tactics featuring Lashit Sanghvi   –   07-Mar-2011</strong></span>
         </a>
      </li>
      
      <li class="newsData">
         <a href="pdf/2011/19et%20now_13%20feb_hiren.pdf" target="_blank">
            <div class="newsDate">13 FEB 2011</div>
            <span> <strong>ET NOW: 2011 going to be a tough year for stock markets: Hiren Ved   –   13-Feb-2011
</strong></span>
         </a>
      </li>
            <li class="newsData">
         <a href="pdf/2011/19feb2011_institutional_investor_indian%20market%20too%20volatile%20_%20february%202011.pdf" target="_blank">
            <div class="newsDate">11 FEB 2011</div>
            <span> <strong>Institutional Investor: Is The Indian Market Too Volatile? Featuring Hiren Ved   –   11-Feb-2011

</strong></span>
         </a>
      </li>
                 <li class="newsData">
         <a href="pdf/2011/19jan_2011_outlookprofit.pdf" target="_blank">
            <div class="newsDate">11 JAN 2011</div>
            <span> <strong>Outlook Profit : India's Biggest Private Wealth Advisor's on Investment Strategy   –   11-Jan-2011

</strong></span>
         </a>
      </li>
   </ul>
</div>
<div>
   <h3>Online Media</h3>
   <h2 class="accordionHead">2014</h2>
   <ul class="newsLetter">
      <li class="newsData">
         <a href="http://www.btvin.com/videos/watch/10225/expect-2015-to-be-a-strong-year-for-indian-markets:-alchemy-capital" target="_blank">
            <div class="newsDate">22-Dec-2014</div>
            <span> <strong>Expect 2015 To Be A Strong Year For Indian Markets: Alchemy Capital </strong></span>
         </a>
      </li>
   </ul>
   
</div>--%>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

    </section>
    <div class="gap-60"></div>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->
    <div class="gap-60"></div>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <!--
      Javascript Files
      ==================================================
      -->
    <!-- initialize jQuery Library-->



    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Media Center",
    "item": "https://www.alchemycapital.com/media-center.aspx"  
  }]
}
</script>


</asp:Content>

