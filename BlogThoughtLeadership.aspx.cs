﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BlogThoughtLeadership : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BlogThoughtLeadershipBind();
            BindInnerBanner(8);
        }
        
    }


    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }
 
    public void BlogThoughtLeadershipBind()
    {
        StringBuilder strBuild = new StringBuilder();
        StringBuilder strHeading = new StringBuilder();
        DataSet ds = new DataSet();

        string pageName = "india-markets-post-election-outlook";

        ds = utility.Display1("Exec Proc_ThoughtLeadership 'bindThoughtLeadershipbyId',0,'','','','','','" + pageName + "'");
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["Image"].ToString() != string.Empty)
            {
                strBuild.Append("<div class='floatDiv'>");
                strBuild.Append("<img class='img-fluid' src='/Content/uploads/ThoughtLeadership/" + ds.Tables[0].Rows[0]["Image"].ToString() + "' alt=''>");
                strBuild.Append("</div>");
            }
            strBuild.Append("<div class='date'><i class='fa fa-calendar-o' aria-hidden='true'></i> " + ds.Tables[0].Rows[0]["PostDate"].ToString() + "</div>");
            strBuild.Append(ds.Tables[0].Rows[0]["Description"].ToString());

            strHeading.Append(ds.Tables[0].Rows[0]["Heading"].ToString());

            
            ltrHeading.Text = strHeading.ToString();
            ltrBlogDetail.Text = strBuild.ToString();
            


        }
       
    }
}