﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="Contact" %>


<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeaderContent">
    <title>Get in Touch With Us - Alchemy Capital</title>
    <meta name="description" content="Are you in need of a PMS or Asset Management Service? Contact Alchemy Capital for detailed information on money investment methods." />

    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description" content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/contact.aspx" />

    <link rel="canonical" href="https://www.alchemycapital.com/contact.aspx" />

    <link rel="alternate" href="https://www.alchemycapital.com/contact.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/contact.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Contact Us</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li>Contact us</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->

    <section class="main-container  mrt-40" id="main-container">
        <div class="container">
            <!-- <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title">Reach Us</h2>
               </div>
            </div>-->
            <!-- Title row end-->
            <div class="row">
                <asp:HiddenField ID="hdnutm_source" runat="server" />
                <asp:HiddenField ID="hdnutm_medium" runat="server" />
                <asp:HiddenField ID="hdnutm_campaign" runat="server" />
                <asp:HiddenField ID="hdnutm_device" runat="server" />
                <asp:HiddenField ID="hdnutm_content" runat="server" />
                <asp:HiddenField ID="hdnutm_term" runat="server" />

                <asp:HiddenField ID="hdncontactreferrer" runat="server" />

                <div class="col-lg-6 mrb-20">
                    <div class="ts-col-inner h-100">
                        <div class="ts-contact-info box-border h-100">
                            <div class="row">
                                <div class="col-md-6 col-6 p-0">
                                    <span class="ts-contact-icon float-left" style="line-height: 0.5;"><i class="icon" style="line-height: 0;">
                                        <img src="Content/uploads/Icon/contact_location.png" style="width: 30px;" /></i></span>
                                    <div class="ts-contact-content" style="margin-left: 50px;">
                                        <h3 class="ts-contact-title">Find Us</h3>
                                        <p>
                                            <asp:Literal ID="ltrFindUs" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                </div>

                                <div class="col-md-6 col-6 p-0">
                                    <span class="ts-contact-icon float-left">
                                        <img src="Content/uploads/Icon/contact_call.png" style="width: 30px" class="icon" alt='' aria-hidden='true' /></span>
                                    <div class="ts-contact-content" style="margin-left: 50px;">
                                        <h3 class="ts-contact-title">Call Us</h3>
                                        <p>
                                            +91-22-6617 1700<br />
                                            Fax : +91-22-6617 1701
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-lg-6 mrb-20">
                    <div class="ts-col-inner h-100">
                        <div class="ts-contact-info box-border h-100">
                            <span class="ts-contact-icon float-left"><i class="">
                                <img src="Content/uploads/Icon/contact_mail.png" class="icon" alt='' aria-hidden='true' /></i></span>
                            <div class="ts-contact-content">
                                <h3 class="ts-contact-title">Invest with Us</h3>
                                <p>
                                    <asp:Literal ID="ltrClient" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <!-- Contact content end-->

                        </div>
                        <!-- End Contact info 1-->
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-6 mailUs mrb-20">
                    <div class="ts-col-inner h-100">
                        <div class="ts-contact-info box-border h-100">
                            <span class="ts-contact-icon float-left">
                                <img src="Content/uploads/Icon/contact_mail.png" class="icon" alt='' aria-hidden='true' /></span>
                            <div class="ts-contact-content">
                                <h3 class="ts-contact-title">Existing Clients</h3>
                                <asp:Literal ID="ltrCallUs" runat="server"></asp:Literal>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="col-lg-6 mailUs mrb-20">
                    <div class="ts-col-inner h-100">
                        <div class="ts-contact-info box-border h-100">
                            <span class="ts-contact-icon float-left"><i class="">
                                <img src="Content/uploads/Icon/contact_mail.png" class="icon" alt='' aria-hidden='true' /></i></span>
                            <div class="ts-contact-content">
                                <h3 class="ts-contact-title">Partner with Us</h3>
                                <p>
                                    <asp:Literal ID="ltrPartners" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <!-- Contact content end-->
                        </div>
                        <!-- End Contact info 1-->
                    </div>
                </div>

            </div>
        </div>

        <!-- container end-->
        <div class="gap-60"></div>
        <div class="ts-form" id="ts-form">
            <div class="container" id="quickconnect">
                <div class="row">
                    <div class="col-lg-6 mrb-20">
                        <h3>Quick Connect</h3>
                        <div id="contactForm">
                            <div class="contact-form">
                                <div class="error-container"></div>
                                <div class="row">
                                    <asp:Label ID="lblMsgcontact" runat="server"></asp:Label>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control form-name" placeholder="Full Name" autocomplete="off"></asp:TextBox>
                                            <span id="ErrorName" class="ErrorValidation">Name is required.</span>
                                        </div>

                                    </div>

                                    <!-- Col end-->
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                            <asp:TextBox ID="txtContact" runat="server" CssClass="form-control form-website" autocomplete="off" placeholder="Phone" MinLength="10" MaxLength="12" onkeypress="return isNumberKey(event)"></asp:TextBox>
                                            <span id="ErrorPhone" class="ErrorValidation">Phone is required.</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">

                                            <asp:TextBox ID="txtContactEmail" runat="server" CssClass="form-control form-email" autocomplete="off" placeholder="Email"></asp:TextBox>
                                            <span id="ErrorEmail1" class="ErrorValidation">Email is required.</span>
                                            <span id="ErrorEmail2" class="ErrorValidation">Invalid email address.</span>
                                        </div>
                                    </div>
                                    <%--    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlContactIntrest" runat="server">
                                                <asp:ListItem Value="I’m Interested">I am Interested</asp:ListItem>
                                                <asp:ListItem Value="In Investing">In Investing</asp:ListItem>
                                                <asp:ListItem Value="IFA / Channel Partner Relation">IFA / Channel Partner Relation</asp:ListItem>
                                                <asp:ListItem Value="I’m an Existing Client">I am an Existing Client</asp:ListItem>
                                                   <asp:ListItem Value="Others">Others</asp:ListItem>
                                            </asp:DropDownList>
                                            <span id="ErrorIntrest" class="ErrorValidation">Please Select Interest.</span>

                                        </div>
                                    </div>--%>
                                    <%--  <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlProduct" runat="server">
                                                <asp:ListItem Value="Products">Products</asp:ListItem>
                                                <asp:ListItem Value="Alchemy High Growth">Alchemy High Growth</asp:ListItem>
                                                <asp:ListItem Value="Alchemy High Growth- Select Stock">Alchemy High Growth- Select Stock</asp:ListItem>
                                                <asp:ListItem Value="Alchemy Leaders">Alchemy Leaders</asp:ListItem>
                                                 <asp:ListItem Value="Alchemy Ascent">Alchemy Ascent</asp:ListItem>
                                                <asp:ListItem Value="Alchemy Leaders of Tomorrow">Alchemy Leaders of Tomorrow</asp:ListItem>
                                                <asp:ListItem Value="All">All</asp:ListItem>
                                            </asp:DropDownList>
                                            <span id="ErrorProduct" class="ErrorValidation">Please Select Products.</span>
                                        </div>
                                    </div>--%>


                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtContactCity" CssClass="form-control form-control-lg" placeholder="City" autocomplete="off" runat="server"></asp:TextBox>
                                            <span id="ErrorContactCity" class="ErrorValidation">City is required.</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlContactIntrest" runat="server">
                                                <asp:ListItem Value="I am Interested in">I am Interested in</asp:ListItem>
                                                <asp:ListItem Value="Investing">Investing </asp:ListItem>
                                                <asp:ListItem Value="Become a channel partner">Becoming a Channel Partner</asp:ListItem>
                                                <asp:ListItem Value="Others">Others</asp:ListItem>
                                            </asp:DropDownList>
                                            <span id="ErrorContactIntrest" class="ErrorValidation">Select field is required.</span>
                                        </div>
                                    </div>


                                    <div class="col-lg-12" id="divContactInvesting" style="display: none;">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddlContactInvesting" runat="server">
                                                <asp:ListItem Value="How did you find out about Alchemy Capital">How did you find out about Alchemy Capital?</asp:ListItem>
                                                <asp:ListItem Value="Web search">Web search</asp:ListItem>
                                                <asp:ListItem Value="Referred by family/friends">Referred by family/friends</asp:ListItem>
                                                <asp:ListItem Value="Referred by Alchemy Channel Partner/Distributor">Referred by Alchemy Channel Partner/Distributor</asp:ListItem>
                                                <asp:ListItem Value="Attended an event">Attended an event</asp:ListItem>
                                                <asp:ListItem Value="Saw an advertisement">Saw an advertisement</asp:ListItem>
                                                <asp:ListItem Value="Media - Print/Online/Podcast">Media - Print/Online/Podcast</asp:ListItem>
                                                <asp:ListItem Value="Received an email">Received an email</asp:ListItem> 
                                            </asp:DropDownList>
                                            <span id="ErrorContactInvesting" class="ErrorValidation">Select field is required.</span>
                                        </div>
                                    </div>

                                    <div class="col-lg-12" id="divContactOthers" style="display: none;">
                                        <div class="form-group">

                                            <asp:TextBox ID="txtComments" TextMode="MultiLine" CssClass="form-control form-message required-field" autocomplete="off" runat="server" placeholder="Comments"></asp:TextBox>
                                            <span id="ErrorComment" class="ErrorValidation">Comment is required.</span>

                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <label class="container">
                                            I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.
                                       <input class="" id="checkcontact" type="checkbox" />
                                            <span class="checkmark">
                                                <small id="errorcheckcontact" style="display: none">*</small>
                                            </span>
                                        </label>
                                    </div>
                                    <!-- Col 12 end-->
                                </div>
                                <!-- Form row end-->
                                <div>
                                    <asp:Button ID="btnConSubmit" CssClass="btn btn-primary tw-mt-30" OnClick="btnConSubmit_Click" OnClientClick="javascript:return validation();" type="submit" Text="Submit" runat="server" />
                                    <%-- <button class="btn btn-primary tw-mt-30" type="submit">Submit</button>--%>
                                </div>
                            </div>
                        </div>
                        <!-- Form end-->
                    </div>
                    <div class="col-lg-6 mrb-20">
                        <div class="map" id="map"></div>
                        <%--<div id="dvMap" style="width: 500px; height: 500px">
                        </div>--%>
                        <%--                        <div id="map_canvas" style="width: 500px; height: 400px"></div>--%>
                    </div>
                </div>
            </div>
        </div>



    </section>


    <div class="gap-60"></div>
    <asp:HiddenField ID="hdnMap" runat="server" />
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <%--    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>--%>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATYqIiFlRf4zUg5c40VoEygfehb4Bb-sQ&callback=initMap"></script>


    <script type="text/javascript">
        window.onload = function () {

            var markers = JSON.parse('<%=ConvertDataTabletoString() %>');
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon: "Content/images/map-icon.png"
                });
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.description);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>


    <style>
        /*.ErrorValidation {

        display: none;
        color: #dc3545!important;
       margin-bottom: 10px;
      
    }*/
        /*#ErrorName {
            position: absolute;
            padding-top: 33px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorEmail1 {
            position: absolute;
            padding-top: 33px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorEmail2 {
            position: absolute;
            padding-top: 33px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorPhone {
            position: absolute;
            padding-top: 33px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorComment {
            position: absolute;
            padding-top: 100px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }*/
    </style>


    <!--
      Javascript Files
      ==================================================
      -->
    <!-- initialize jQuery Library-->





    <script type="text/javascript">
        //$(document).ready(function () {
        //$('input').focusout(function () {
        //    validation();
        //    //});
        //});


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            var textBox = document.getElementById('<%=txtContact.ClientID%>').value.length;
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
                return false;

            return true;
        }


        function validation() {
            var Name = $('#<%=txtFullName.ClientID%>').val();
            var Email = $('#<%=txtContactEmail.ClientID%>').val();
            var Phone = $('#<%=txtContact.ClientID%>').val();
            var Comment = $('#<%=txtComments.ClientID%>').val();

            var CityContact = $('#<%=txtContactCity.ClientID%>').val();

            var IntrestContact = $('#<%=ddlContactIntrest.ClientID%>').val();

            var blank = false;

            if (Name == '') {
                $('#ErrorName').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorName').css('display', 'none');

            }


            if (Email == '') {
                $('#ErrorEmail2').css('display', 'none');
                $('#ErrorEmail1').css('display', 'block');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $('#ErrorEmail1').css('display', 'none');
                    $('#ErrorEmail2').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorEmail2').css('display', 'none');


                }
                $('#ErrorEmail1').css('display', 'none');


            }

            if (Phone == '') {
                $('#ErrorPhone').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorPhone').css('display', 'none');

            }

            <%--      if ($('#<%=ddlContactIntrest.ClientID%>').val() == 'I’m Interested') {
                $('#ErrorIntrest').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorIntrest').css('display', 'none');

            }

            if ($('#<%=ddlProduct.ClientID%>').val() == 'Products') {
                $('#ErrorProduct').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorProduct').css('display', 'none');

            }

            if (Comment == '') {
                $('#ErrorComment').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorComment').css('display', 'none');

            }--%>

            if (CityContact == '') {
                $('#ErrorContactCity').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorContactCity').css('display', 'none');
            }

            if (IntrestContact == 'I am Interested in') {
                $('#ErrorContactIntrest').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorContactIntrest').css('display', 'none');
            }


            if (IntrestContact == 'Investing') {

                var InvestingContact = $('#<%=ddlContactInvesting.ClientID%>').val();

                if (InvestingContact == 'How did you find out about Alchemy Capital') {
                    $('#ErrorContactInvesting').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorContactInvesting').css('display', 'none');
                }

            }

            else {
                $('#ErrorContactInvesting').css('display', 'none');
            }

            if (IntrestContact == "Others") {

                if (Comment == '') {
                    $('#ErrorComment').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorComment').css('display', 'none');
                }
            }
            else {
                $('#ErrorComment').css('display', 'none');
            }

            if (checkcontact.checked == false) {
                $('#errorcheckcontact').css('display', 'block');
                blank = true;
            }
            else {
                $('#errorcheckcontact').css('display', 'none');
            }



            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }



        var Value1 = $('#<%=ddlContactIntrest.ClientID%>').val();
        if (Value1 == 'Investing') {
            $('#divContactInvesting').show();
            $('#divContactOthers').hide();
        }
        else if (Value1 == 'Others') {
            $('#divContactOthers').show();
            $('#divContactInvesting').hide();
        }
        else {
            $('#divContactInvesting').hide();
            $('#divContactOthers').hide();

        }


        $(document).ready(function () {
            $('#<%=ddlContactIntrest.ClientID%>').change(function () {

                var Value1 = $('#<%=ddlContactIntrest.ClientID%>').val();
                if (Value1 == 'Investing') {
                    $('#divContactInvesting').show();
                    $('#divContactOthers').hide();
                }
                else if (Value1 == 'Others') {
                    $('#divContactOthers').show();
                    $('#divContactInvesting').hide();
                }
                else {
                    $('#divContactInvesting').hide();
                    $('#divContactOthers').hide();

                }
            });
        });



        //ContactEventTrack


        function ContactEventTracking() {
            "gtag('event', 'Submit', { 'event_category': 'Form',  'event_label': 'Quick Connect Form'});";
        }
    </script>

    <script>
        ///  store referrer value

        document.getElementById("<%=hdncontactreferrer.ClientID%>").value = document.referrer;
          
    </script>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Contact Us",
    "item": "https://www.alchemycapital.com/contact.aspx"  
  }]
}
    </script>





</asp:Content>

