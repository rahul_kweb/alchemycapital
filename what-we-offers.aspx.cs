﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WhatWeOffers : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        BindString();
    }

    public void BindString()
    {
        if (Request.QueryString["PageName"] != null && Request.QueryString["PageName"] != "")
        {
          
                hdnId.Value = Request.QueryString["PageName"];
                getwhatweoffers();

         

        }
    }

    public void getwhatweoffers()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_WhatWeOfferContents 'bindWhatWeOfferUrl',0,0,'','','',0,'','" + hdnId.Value + "'");
        if (dt.Rows.Count > 0)
        {
            int id = Convert.ToInt32(dt.Rows[0]["WhatOfferId"]);
            rptBanner.DataSource = dt;
            rptBanner.DataBind();

            BindLiteral(id);
        }
    }

    public void BindLiteral(int Id)
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();
        StringBuilder strPageName = new StringBuilder();
        StringBuilder strInnerBanner = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_WhatWeOfferInnerPages 'bindWhatWeOfferDetailPage',0,0," + Id + "");
        if (dt.Rows.Count > 0)
        {
            strPageName.Append(dt.Rows[0]["PageName"].ToString());
            strInnerBanner.Append(dt.Rows[0]["BannerImage"].ToString());
            

            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /> <span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append(dr["Description"].ToString());
                strDesc.Append("</div>");

            }

        }
        ltrPageName.Text = strPageName.ToString();
        ltrTabHeading.Text = strTitle.ToString();
        ltrDescription.Text = strDesc.ToString();
    }
}