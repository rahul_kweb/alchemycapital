﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CSR : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCsr(2);
            GetCsrPdf();
            BindInnerBanner(4);
        }
    }


    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindCsr(int Id)
    {

        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PageCMSMaster 'bindPageContent',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /><span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append(dr["Description"].ToString());
                strDesc.Append("</div>");

            }
            ltrHeading.Text = strTitle.ToString();
            ltrDescription.Text = strDesc.ToString();
        }
    }

    public void GetCsrPdf()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_CsrPdf 'Get'");
        if (dt.Rows.Count > 0)
        {

            StringBuilder strbind = new StringBuilder();
            strbind.Append(" <div class='social_resp'>");
            strbind.Append("<h3>Corporate Social Responsibility</h3>");
            strbind.Append("<p>Please <a href='Content/uploads/Csrpdf/" + dt.Rows[0]["CsrPdf"].ToString() + "' target='_blank'>Click here</a> to view Alchemy's CSR policy.</p>");
            strbind.Append("</div>");

          ltrCsrPdf.Text = strbind.ToString();
        }

    }
}