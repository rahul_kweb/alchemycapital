﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class People_and_Culture : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindOverview();
            BindInnerBanner(3);

        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindOverview()
    {
        StringBuilder strHeading = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();
        DataSet ds = new DataSet();
        ds = utility.Display1("Exec Proc_PeopleCultureMaster 'bindPeopleCulture'");
        if (ds.Tables.Count > 0)
        {
            ltrOverview.Text = ds.Tables[1].Rows[0]["Description"].ToString();

            //People Culture
            foreach (DataRow dr in ds.Tables[0].Rows)
            {

                strHeading.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /> <span>" + dr["Title"].ToString() + "</span></li>");

                DataTable dt0 = new DataTable();
                dt0 = utility.Display("Exec Proc_PeopleCulture 'bindPeopleCultureTeamTypeById',0,'" + dr["PcmId"].ToString() + "'");


                strDesc.Append("<div>");
                strDesc.Append("<h3>" + dr["Title"].ToString() + "</h3>");
                if (dt0.Rows.Count > 0)
                {
                    foreach (DataRow dr0 in dt0.Rows)
                    {
                        DataTable dt1 = new DataTable();
                        dt1 = utility.Display("Exec Proc_PeopleCulture 'bindPeopleCultureByTeam',0,'" + dr["PcmId"].ToString() + "','','','','',0,'" + dr0["TeamCategory"].ToString() + "'");

                        strDesc.Append("<h4 style='color:#ffae00;border-bottom: none;margin-bottom: 18px'>" + dr0["TeamCategory"].ToString() + ":</h4>");
                        if (dt1.Rows.Count > 0)
                        {
                            strDesc.Append("<div Id='People_Founder' class='row mrb-40 justify-content-between' style='position: relative; '>");
                            foreach (DataRow dr1 in dt1.Rows)
                            {

                                strDesc.Append("<div class='col-md-5 text-center'>");
                                strDesc.Append(" <div  class='w-100' id=" + dr1["Url"].ToString() + " style ='position: absolute; top: -100px; color:#ff0000'>");
                                strDesc.Append("</div>");
                                strDesc.Append("<div class='b-team-2__inner mb-3'>");
                                strDesc.Append("<img src='Content/uploads/Team/" + dr1["Image"].ToString() + "' class='img-fluid' alt='" + dr1["Name"].ToString() + "-" + dr1["Designation"].ToString() + "-Alchemy Capital" + "'>");
                                strDesc.Append("</div>");
                                strDesc.Append("<h4 class='b-team-2__name pb-2 mb-4'>" + dr1["Name"].ToString() + " <br/> <span class='b-team-2__category'> " + dr1["Designation"].ToString() + " </span> <br/><a href='/people-and-culture/"+ dr1["Slug"].ToString() + ".aspx' class='d-inline-block'>Read More</a></h4>");
                                strDesc.Append("</div>");

                            }
                            strDesc.Append("</div>");
                        }

                    }

                }
                else
                {
                    DataTable dt1 = new DataTable();
                    dt1 = utility.Display("Exec Proc_PeopleCultureMaster 'bindPeopleCultureById','" + dr["PcmId"].ToString() + "'");

                    if (dt1.Rows.Count > 0)
                    {
                        strDesc.Append("<div Id='People_Founder' class='row mrb-40 justify-content-between' style='position: relative;'>");
                        foreach (DataRow dr1 in dt1.Rows)
                        {
                           
                      
                            strDesc.Append("<div class='col-md-5 text-center'>");
                            strDesc.Append(" <div  class='w-100' id=" + dr1["Url"].ToString() + " style ='position: absolute; top: -100px; color:#ff0000'>");
                            strDesc.Append("</div>");
                            strDesc.Append("<div class='b-team-2__inner mb-3'>");
                            strDesc.Append("<img src='Content/uploads/Team/" + dr1["Image"].ToString() + "' class='img-fluid' alt='" + dr1["Name"].ToString() + "-" + dr1["Designation"].ToString() + "-Alchemy Capital" + "'>");
                            strDesc.Append("</div>");
                            strDesc.Append("<h4 class='b-team-2__name pb-2 mb-4'>" + dr1["Name"].ToString() + " <br/> <span class='b-team-2__category'> " + dr1["Designation"].ToString() + " </span> <br/><a href='/people-and-culture/" + dr1["Slug"].ToString() + ".aspx' class='d-inline-block'>Read More</a></h4>");
                            strDesc.Append("</div>");


                        }
                        strDesc.Append("</div>");

                    }

                }

                strDesc.Append("</div>");
            }
            ltrTabHeading.Text = strHeading.ToString();
            ltrFounder.Text = strDesc.ToString();
        }
    }

}