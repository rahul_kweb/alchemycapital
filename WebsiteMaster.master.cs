﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebsiteMaster : System.Web.UI.MasterPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        SocialMedia(1);
        //login_maintanance_Url();

        System.Web.HttpContext context = System.Web.HttpContext.Current;
        hdnIpforAscentPopUpclick.Value = context.Request.UserHostAddress;

        if (Request.QueryString["PageName"] == "alchemy-high-growth")
        {
            Response.Redirect("/portfolio-management-services/alchemy-high-growth.aspx");
        }
        else if (Request.QueryString["PageName"] == "alchemy-high-growth-select-stock")
        {
            Response.Redirect("/portfolio-management-services/alchemy-high-growth-select-stock.aspx");
        }
        else if (Request.QueryString["PageName"] == "alchemy-leaders")
        {
            Response.Redirect("/portfolio-management-services/alchemy-leaders.aspx");
        }
        else if (Request.QueryString["PageName"] == "alchemy-leaders-of-tomorrow")
        {
            Response.Redirect("/aif/alchemy-leaders-of-tomorrow.aspx");
        }


    }

    public void SocialMedia(int Id)
    {
        DataTable dt = new DataTable();
        StringBuilder strbuild = new StringBuilder();
        dt = utility.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
        if (dt.Rows.Count > 0)
        {

            ltrHeadOffice.Text = dt.Rows[0]["HeadOffice"].ToString();
            ltrCallus.Text = dt.Rows[0]["CallUs"].ToString();
            ltrMailUs.Text = dt.Rows[0]["MailUs"].ToString();

        }

    }


    //This method for alchemy ascent pop up Request click count
    protected void ascent_req_call_Click(object sender, EventArgs e)
    {
        Session["popup"] = "Alchemy Ascent Pop Up Request";


        //string APIKey = "fa5e68c09d11ca979c5788115b28fe55d2ba94c05177173fe4347e1ee72f1bde";
        //string url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, strUserIP);   //get client location using Ip Addresss
        using (WebClient client = new WebClient())
        {
            var json = client.DownloadString("https://geoip-db.com/json");
            IPLocation location = new JavaScriptSerializer().Deserialize<IPLocation>(json);


            using (SqlCommand cmd = new SqlCommand("Proc_AscentPopUpRequestHit"))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@IpAddress", location.IPv4);
                cmd.Parameters.AddWithValue("@CityName", location.city);
                cmd.Parameters.AddWithValue("@Region", location.state);
                cmd.Parameters.AddWithValue("@CountryName", location.country_name);


                utility.Execute(cmd);
                Response.Redirect("/contact.aspx#quickconnect");
            }


        }
    }


    public void login_maintanance_Url()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_Login_Maintenance 'get'");
        StringBuilder strbuild = new StringBuilder();
        if (dt.Rows.Count > 0)
        { 
            if (dt.Rows[0]["ActiveUrl"].ToString() == "Yes")
            {
                strbuild.Append("<li><a href='https://client.alchemycapital.com/login_alchemy.aspx' target='_blank' onclick='gtag('event','Click', { 'event_category': 'Lead Tracking',  'event_label': 'Login'});'>login</a></li>");
            }
            else
            {
                strbuild.Append("<li><a href='/not-found.aspx' target='_blank'>login</a></li>");

            }
        }
        ltrLoginUrl.Text = strbuild.ToString();
    }


}
