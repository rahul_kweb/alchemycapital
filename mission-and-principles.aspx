﻿<%@ Page  Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="mission-and-principles.aspx.cs" Inherits="Mission_and_Principles" %>


<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Our Mission and Principles of Work | Alchemy Capital</title>
   <meta name="description" content="Our mission at Alchemy Capital is to provide better services to our stakeholders in order to achieve long-term goals through exceptional performance and make them satisfied."/>

    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/mission-and-principles.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/mission-and-principles.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/mission-and-principles.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/mission-and-principles.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                               <h1 class="banner-title">Mission &amp; Principles</h1>
                                <ol class="breadcrumb">
                                     <li><a href="/">Home</a></li>
                                    <li><a href="javascript:void(0);">Who We Are</a></li>
                                    <li>Mission &amp; Principles</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->

    <section class="main-container mrt-40" id="main-container">
        <div class="container mrb-40">

           <%-- <h1 class="column-title title-small text-center">Mission &amp; Principles of Work</h1>--%>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
                    <%-- <li><i class="fa fa-eye" aria-hidden="true"></i><span>Our Vision</span></li>
                    <li><i class="fa fa-bullseye" aria-hidden="true"></i><span>Our Mission</span></li>
                    <li><i class="fa fa-star" aria-hidden="true"></i><span>Ethos</span></li>
                    <li><i class="fa fa-certificate" aria-hidden="true"></i><span>Values</span></li>--%>
                </ul>
                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>

                    <%--   <div>
                        <h3>Our Mission</h3>

                        <p>To meet the long-term objectives of all our stakeholders through superior service, absolute transparency and executional efficiency.</p>
                    </div>

                    <div>

                        <h3>Ethos</h3>

                        <p>Where the mind is without fear and the head is held high.</p>
                        <p>Alchemy believes that knowledge is power. The firm stands on a culture of learning, excellence, integrity and insight that come together to produce results which meet and exceed the clientele’s expectations.</p>

                    </div>

                    <div>
                        <h3>Alchemy’s foundation is based on:</h3>

                        <ul>
                            <li>Working tirelessly to meet and ultimately exceed client expectations</li>
                            <li>The realization that there could be no limit to knowledge</li>
                            <li>Being resolute in achieving long term goals, yet being dynamic and original in our approach</li>
                            <li>Pursuing excellence through highest quality practices</li>
                            <li>Leading by example and nurturing teamwork</li>
                            <li>Marrying integrity and transparency in all transactions and relationships</li>
                        </ul>

                    </div>--%>
                </div>
            </div>


            <div class="clearfix"></div>
        </div>
    </section>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <!--
      Javascript Files
      ==================================================
      -->

    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Mission & Principles",
    "item": "https://www.alchemycapital.com/mission-and-principles.aspx"  
  }]
}
</script>


</asp:Content>

