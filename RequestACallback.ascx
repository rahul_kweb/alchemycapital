﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RequestACallback.ascx.cs" Inherits="RequestACallback" %>

<div class="col-lg-5 col-md-12W footer-widget">
    <asp:HiddenField ID="hdnutm_source" runat="server" />
    <asp:HiddenField ID="hdnutm_medium" runat="server" />
    <asp:HiddenField ID="hdnutm_campaign" runat="server" />
    <asp:HiddenField ID="hdnutm_device" runat="server" />
    <asp:HiddenField ID="hdnutm_content" runat="server" />
    <asp:HiddenField ID="hdnutm_term" runat="server" />

<asp:HiddenField ID="hdnreferrer" runat="server"/>

    <div id="RequestACallBack">
    <h3 class="widget-title">Request a Callback </h3>
    <%--<form class="newsletter-form" id="newsletter-form" action="#" method="post">--%>
    <div class="newsletter-form">
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
        <div class="form-group newsletter-form">

<%--            <asp:TextBox ID="txtNametest" type="text" class="form-control form-name" placeholder="Full Name" autocomplete="off" runat="server"/>--%>

            <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Name" autocomplete="off" runat="server"></asp:TextBox>
            <span id="ErrorName1" class="ErrorValidation">Name is required.</span>


            <asp:TextBox ID="txtPhone" CssClass="form-control form-control-lg" placeholder="Phone" runat="server" autocomplete="off" MinLength="10" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
            <span id="ErrorPhone1" class="ErrorValidation">Phone is required.</span>

            <asp:TextBox ID="txtEmail" CssClass="form-control form-control-lg" autocomplete="off" placeholder="Email Address" runat="server"></asp:TextBox>
            <span id="ErrorEmail3" class="ErrorValidation">Email is required.</span>
            <span id="ErrorEmail4" class="ErrorValidation">Invalid email address.</span>

            <asp:TextBox ID="txtCity" CssClass="form-control form-control-lg" placeholder="City" autocomplete="off" runat="server"></asp:TextBox>
            <span id="ErrorCity" class="ErrorValidation">City is required.</span>


            <asp:DropDownList ID="ddlIntrest" runat="server">
                <asp:ListItem Value="I am Interested in">I am Interested in </asp:ListItem>
                  <asp:ListItem Value="Investing">Investing </asp:ListItem>
                <asp:ListItem Value="Become a channel partner">Becoming a Channel Partner</asp:ListItem>
                 <asp:ListItem Value="Others">Others</asp:ListItem>
            </asp:DropDownList>     
            <span id="ErrorIntrest" class="ErrorValidation">Select field is required.</span>  

            <div id="divInvesting" style="display: none;">
            <asp:DropDownList ID="ddlInvesting" runat="server">
                <asp:ListItem Value="How did you find out about Alchemy Capital">How did you find out about Alchemy Capital?</asp:ListItem>
                 <asp:ListItem Value="Web search">Web search</asp:ListItem>
                <asp:ListItem Value="Referred by family/friends">Referred by family/friends</asp:ListItem>
                <asp:ListItem Value="Referred by Alchemy Channel Partner/Distributor">Referred by Alchemy Channel Partner/Distributor</asp:ListItem>
                <asp:ListItem Value="Attended an event">Attended an event</asp:ListItem>
                <asp:ListItem Value="Saw an advertisement">Saw an advertisement</asp:ListItem>
                <asp:ListItem Value="Media - Print/Online/Podcast">Media - Print/Online/Podcast</asp:ListItem>
                <asp:ListItem Value="Received an email">Received an email</asp:ListItem>    
            </asp:DropDownList>
                 <span id="ErrorInvesting" class="ErrorValidation">Select field is required.</span>
                </div>

            <div id="divOthers" style="display: none;">
            <asp:TextBox ID="txtReqComments" TextMode="MultiLine" CssClass="form-control form-control-lg" autocomplete="off" runat="server" placeholder="Comments" Style="background:white"></asp:TextBox>
                <span id="ErrorReqComment" class="ErrorValidation">Comment is required.</span>
          </div>

            <label class="container">
                I authorize Alchemy Capital Management to contact me. This will override registry on the NDNC.
                 <input class="" id="check" type="checkbox"/>          
                <span class="checkmark">
                    <small id="errorcheck" style="display:none">*</small>
                </span>
            </label>
            <asp:Button ID="btn_Req_Submit" CssClass="btn btn-primary" Text="Submit" runat="server" OnClick="btn_Req_Submit_Click" OnClientClick="javascript:return Checkfields();" Style="padding:10px 25px; width:auto; color: white" />
        </div>
    </div>
</div>
    </div>
<style>
    .ErrorValidation {
        display: none;
        color: #dc3545 !important;
        margin-bottom: 10px;
    }

    /*#ErrorEmail3 {
        position: absolute;
        padding-top: 21px;
        padding-left: 0px;
        text-align: left;
        display: none;
        color: red;
        line-height: 50px;
    }

    #ErrorEmail4 {
        position: absolute;
        padding-top: 21px;
        padding-left: 0px;
        text-align: left;
        display: none;
        color: red;
        line-height: 50px;
    }

    #ErrorPhone1 {
        position: absolute;
        padding-top: 21px;
        padding-left: 0px;
        text-align: left;
        display: none;
        color: red;
        line-height: 50px;
    }

    #ErrorCity {
        position: absolute;
        padding-top: 21px;
        padding-left: 0px;
        text-align: left;
        display: none;
        color: red;
        line-height: 50px;
    }*/

</style>

<script>
    ///  store referrer value

    document.getElementById("<%=hdnreferrer.ClientID%>").value = document.referrer;

</script>

<script type="text/javascript">

    function isNumberKey1(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;
        var textBox = document.getElementById('<%=txtPhone.ClientID%>').value.length;
        if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
            return false;

        return true;
    }

    function Checkfields() {

        var Name1 = $('#<%=txtName.ClientID%>').val();
        var Phone1 = $('#<%=txtPhone.ClientID%>').val();
        var Email1 = $('#<%=txtEmail.ClientID%>').val();
        var City = $('#<%=txtCity.ClientID%>').val();

        var Intrest = $('#<%=ddlIntrest.ClientID%>').val();
        var ReqComment = $('#<%=txtReqComments.ClientID%>').val(); 

        var Blank1 = false;
        var message = "";

        if (Name1 == '') {
            $('#ErrorName1').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorName1').css('display', 'none');

        }


        if (Phone1 == '') {
            $('#ErrorPhone1').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorPhone1').css('display', 'none');
        }


        if (Email1 == '') {
            $('#ErrorEmail3').css('display', 'block');
            $('#ErrorEmail4').css('display', 'none');
            Blank1 = true;
        }
        else {
            var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            if (!EmailText.test(Email1)) {
                $('#ErrorEmail3').css('display', 'none');
                $('#ErrorEmail4').css('display', 'block');
                Blank1 = true;
            }
            else {
                $('#ErrorEmail4').css('display', 'none');


            }
            $('#ErrorEmail3').css('display', 'none');


        }

        if (City == '') {
            $('#ErrorCity').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorCity').css('display', 'none');
        }


        if (check.checked == false) {
            $('#errorcheck').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#errorcheck').css('display', 'none');
        }

        if (Intrest == 'I am Interested in') {
            $('#ErrorIntrest').css('display', 'block');
            Blank1 = true;
        }
        else {
            $('#ErrorIntrest').css('display', 'none');
        }


        if (Intrest == 'Investing') {

            var Investing = $('#<%=ddlInvesting.ClientID%>').val();

            if (Investing == 'How did you find out about Alchemy Capital') {
                $('#ErrorInvesting').css('display', 'block');
             
                Blank1 = true;
            }
            else {
                $('#ErrorInvesting').css('display', 'none');
             
            }

        }    
        else {
            $('#ErrorInvesting').css('display', 'none');
          

        }


        if (Intrest == "Others") {

            if (ReqComment == '') {
                $('#ErrorReqComment').css('display', 'block');
             
                Blank1 = true;
            }
            else {
                $('#ErrorReqComment').css('display', 'none');
        
            }
        }
        else {
            $('#ErrorReqComment').css('display', 'none');
          
        }



        if (Blank1) {

            return false;

        }
        else {
            return true;
        }
    }



      var Value = $('#<%=ddlIntrest.ClientID%>').val();
    if (Value == 'Investing') {
        $('#divInvesting').show();
        $('#divOthers').hide();
    }
  
    else if (Value == 'Others')
    {
        $('#divOthers').show();
        $('#divInvesting').hide();
    }
    else {
        $('#divInvesting').hide();
        $('#divOthers').hide();
    }


         $(document).ready(function () {
            $('#<%=ddlIntrest.ClientID%>').change(function () {
                var Value = $('#<%=ddlIntrest.ClientID%>').val();
                if (Value == 'Investing') {
                    $('#divInvesting').show();
                    $('#divOthers').hide();
                }
                else if (Value == 'Others') {
                    $('#divOthers').show();
                    $('#divInvesting').hide();
                }
                else {
                    $('#divInvesting').hide();
                    $('#divOthers').hide();

                }
            });
        });

    //RequestEventTrack


    function EventTracking() {
        "gtag('event', 'Submit', { 'event_category': 'Form',  'event_label': 'Request A Callback Form'});";
    }
</script>


