﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // store client ip address

        System.Web.HttpContext context = System.Web.HttpContext.Current;

        using (WebClient client = new WebClient())
        {
            var json = client.DownloadString("https://geoip-db.com/json");
            IPLocation location = new JavaScriptSerializer().Deserialize<IPLocation>(json);

            Response.Write("my City:" + location.city + " state:" + location.state);
            lblcity.Text = location.country_name;
            lblstate.Text = location.postal;

        }
    }
}