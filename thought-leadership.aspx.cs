﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thought_leadership : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            thoughtleadershipOverview();
            BindThoughtLeadership();
            BindInnerBanner(8);
        }

    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public string thoughtleadershipOverview()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS',4");
        if (dt.Rows.Count > 0)
        {
            ltrOverview.Text = dt.Rows[0]["Description"].ToString();
        }
        return ltrOverview.Text;
    }



    public string BindThoughtLeadership()
    {
        StringBuilder strBuild = new StringBuilder();
        DataSet ds = new DataSet();
        ds = utility.Display1("Exec Proc_ThoughtLeadership 'bindThoughtLeadership'");
        if (ds.Tables.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                strBuild.Append("<div class='col-lg-4 col-md-12 mrb-40'>");
                strBuild.Append("<div class='ts-service-box h-100'>");
                if (dr["Image"].ToString() != string.Empty)
                {
                    strBuild.Append("<div class='ts-service-image-wrapper'>");
                    strBuild.Append("<img class='img-fluid' src='/Content/uploads/ThoughtLeadership/" + dr["Image"].ToString() + "' alt=''>");
                    strBuild.Append("</div>");
                }
                strBuild.Append("<div class='ts-service-content h-100'>");
                //strBuild.Append("<img class='ts-service-icon' src='/Content/uploads/ThoughtLeadership/" + dr["Icon"].ToString() + "' alt=''>");
                //strBuild.Append("<span class='ts-service-icon'><i class='" + dr["Icon"].ToString() + "'></i></span>");
                strBuild.Append("<h3 class='service-title'><a class='link-more' href='/thought-leadership/" + dr["Slug"].ToString() + ".aspx' style='color:#343a40'>" + dr["Heading"].ToString() + "</a></h3>");
                strBuild.Append("<small class='yellowtitle'>" + dr["PostDate"].ToString() + "</small>");
                strBuild.Append(dr["Description"].ToString());
                strBuild.Append("<p><a class='link-more' href='/thought-leadership/" + dr["Slug"].ToString() + ".aspx'>Read More <i class='icon icon-right-arrow2'></i></a></p>");
                strBuild.Append("</div>");

                strBuild.Append("</div>");
                strBuild.Append("</div>");

            }
            ltrBlog.Text = strBuild.ToString();
        }
        return ltrBlog.Text;
    }

}