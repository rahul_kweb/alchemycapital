﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="whatsapp-optin.aspx.cs" Inherits="whatsapp_optin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="banner-area" id="banner-area" style="background-image: url(/Content/uploads/InnerBanner/whatsappOptinBanner.jpg); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title" style="font-size:60px">Stay Updated With <br />ALCHEMY CAPITAL INVESTMENTS</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li>Whatsapp Optin</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 text-center mb-4">
                <img src="/Content/images/whatsapp.png" class="img-fluid mb-3" />
                <h2 class="mb-4" style="color:#2f5498;">Stay updated on Whatsapp</h2>
                <h4 class="mb-4" style="color:#2f5498;">Would you like to receive notifications on market views, investment updates, and your portfolio strategy,<br /> from our CIO’s desk.</h4>
                <a href="#" class="btn btn-primary text-white" style="margin:15px;">Yes</a>
                <a href="#" class="btn btn-primary text-white" style="margin:15px;">No</a>
            </div>
  <%--          <div class="col-md-6 mb-4">
                <div class="row text-center">
                    <h3 class="col-12" style="color:#2f5498;">What are you looking for?</h3>
                    <div class="col-md-6">
                        <a href="/portfolio-management-services.aspx"><img src="/Content/images/2.jpg" class="img-fluid mb-3" /></a>
                        <a href="/portfolio-management-services.aspx"><h5 style="color:#2f5498;">Our Products</h5></a>
                    </div>
                    <div class="col-md-6">
                        <a href="/people-and-culture.aspx"><img src="/Content/images/1.jpg" class="img-fluid mb-3" /></a>
                        <a href="/people-and-culture.aspx"><h5 style="color:#2f5498;">People and Culture</h5></a>
                    </div>
                </div>

                </div>--%>
            </div>
        </div>

</asp:Content>

