﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MediaCenter : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindUpperDesc(5);
            BindMediaCenter();
            BindInnerBanner(10);
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }


    public void BindUpperDesc(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_CMS 'bindCMS','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            ltrUpperDisc.Text = dt.Rows[0]["Description"].ToString();
        }

    }

    public void BindMediaCenter()
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_Insights 'MediaCenterTitle'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /> <span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append("<h3>" + dr["Title"].ToString() + "</h3>");

                if (dr["Video"].ToString() != "")
                {
                    strDesc.Append("<div class='currentVid'>");
                    strDesc.Append("<iframe src='" + dr["Video"].ToString() + "' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>");
                    strDesc.Append("</div>");

                }

                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_Insights 'MedianCenterYearbyTitle',0,'" + dr["Id"].ToString() + "'");
                int i = 0;
                if (dt1.Rows.Count > 0)
                {
                    
                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        if (i == 0)
                        {
                            // if (dr["Title"].ToString() == "TV Interview")
                            //{
                            //    strDesc.Append("<h2 class='accordionHead active'>" + dr1["Year"].ToString() + "</h2>");
                            //}
                            //else
                            //{
                            //    strDesc.Append("<h2 class='accordionHead'>" + dr1["Year"].ToString() + "</h2>");
                            //}

                            strDesc.Append("<h2 class='accordionHead active'>" + dr1["Year"].ToString() + "</h2>");

                            strDesc.Append("<ul class='newsLetter'>");

                            DataTable dt2 = new DataTable();
                            dt2 = utility.Display("Exec Proc_Insights 'MediaCenterbyTitleAndYear',0,'" + dr["Id"].ToString() + "','" + dr1["Year"].ToString() + "'");
                            if (dt2.Rows.Count > 0)
                            {
                                foreach (DataRow dr2 in dt2.Rows)
                                {
                                    strDesc.Append("<li class='newsData'>");
                                    if (dr2["Type"].ToString() == "Url")
                                    {
                                        strDesc.Append("<a href='" + dr2["Url"].ToString() + "' target='_blank'>");
                                    }
                                 
                                    else if(dr2["Type"].ToString() == "Pdf")
                                    {
                                        strDesc.Append("<a href='Content/uploads/Pdf/" + dr2["Pdf"].ToString() + "' target='_blank'>");
                                    }

                                    else
                                    {
                                        strDesc.Append("<a href='Content/uploads/MediaImages/" + dr2["Image"].ToString() + "' target='_blank'>");
                                    }
                                    strDesc.Append("<div class='newsDate'>" + dr2["PostDate"].ToString() + "</div>");
                                    strDesc.Append("<span> <strong>" + dr2["Title"].ToString() + "</strong></span>");
                                    strDesc.Append("</a>");
                                    strDesc.Append("</li>");
                                }

                            }
                            strDesc.Append("</ul>");
                            i++;
                        }
                        else
                        {
                            strDesc.Append("<h2 class='accordionHead'>" + dr1["Year"].ToString() + "</h2>");
                            strDesc.Append("<ul class='newsLetter'>");

                            DataTable dt2 = new DataTable();
                            dt2 = utility.Display("Exec Proc_Insights 'MediaCenterbyTitleAndYear',0,'" + dr["Id"].ToString() + "','" + dr1["Year"].ToString() + "'");
                            if (dt2.Rows.Count > 0)
                            {
                                foreach (DataRow dr2 in dt2.Rows)
                                {
                                    strDesc.Append("<li class='newsData'>");
                                    if (dr2["Type"].ToString() == "Url")
                                    {
                                        strDesc.Append("<a href='" + dr2["Url"].ToString() + "' target='_blank'>");
                                    }

                                    else if (dr2["Type"].ToString() == "Pdf")
                                    {
                                        strDesc.Append("<a href='Content/uploads/Pdf/" + dr2["Pdf"].ToString() + "' target='_blank'>");
                                    }

                                    else
                                    {
                                        strDesc.Append("<a href='Content/uploads/MediaImages/" + dr2["Image"].ToString() + "' target='_blank'>");
                                    }
                                    strDesc.Append("<div class='newsDate'>" + dr2["PostDate"].ToString() + "</div>");
                                    strDesc.Append("<span> <strong>" + dr2["Title"].ToString() + "</strong></span>");
                                    strDesc.Append("</a>");
                                    strDesc.Append("</li>");
                                }

                            }
                            strDesc.Append("</ul>");
                        }

                    }

                }
                strDesc.Append("</ul>");
                strDesc.Append("</div>");

            }

        }
        ltrMediaTab.Text = strTitle.ToString();
        ltrDescription.Text = strDesc.ToString();
    }
}