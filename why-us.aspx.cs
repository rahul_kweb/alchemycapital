﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WhyUs : referer
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
         if(!IsPostBack)
        {
            BindWhyUs(3);
            BindInnerBanner(7);
        }
    }


    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }

    public void BindWhyUs(int Id)
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_PageCMSMaster 'bindPageContent',0,'" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /><span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append(dr["Description"].ToString());
                strDesc.Append("</div>");

            }
          ltrTitle.Text= strTitle.ToString();
           ltrDescription.Text= strDesc.ToString();

        }

    }
}