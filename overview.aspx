﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="overview.aspx.cs" Inherits="Overview" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="HeaderContent">
    <title>Know Everything About Alchemy Capital | Asset Management Company</title>
    <meta name="description" content="Alchemy Capital aspires to be the largest portfolio and asset management company in India. Offering best-in-class services, as well as the greatest advice and investment possibilities." />

    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description" content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/overview.aspx" />

    <link rel="canonical" href="https://www.alchemycapital.com/overview.aspx" />

    <link rel="alternate" href="https://www.alchemycapital.com/overview.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/overview.aspx" hreflang="x-default" />


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Overview</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="javascript:void(0);">Who We Are</a></li>
                                    <li>Overview</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">

        <div class="container mrb-30">

           <%-- <h1 class="column-title title-small text-center">Overview</h1>--%>
            <div class="row">

                <div class="col-lg-12">

                    <asp:Literal ID="ltrOverview" runat="server"></asp:Literal>

                    <!-- container row end-->
                </div>
                <!-- Col end
               <div class="col-lg-6">
                  <img class="img-fluid" src="images/pages/about_us.jpg" class="img-responsive" alt="">
               </div>-->
                <!-- Col end-->
            </div>
            <!-- Main row end-->
        </div>



        <!--<div class="dashedBorder mrb-60"></div>-->

        <!--<div class="gap-60" ></div>-->



        <div class="container mrb-40" id="milestones">

            <h2 class="column-title title-small text-center">Milestones
            </h2>

            <div class="timeline mrt-30">
                <div class="timeline__wrap">
                    <div class="timeline__items">
                        <asp:Literal ID="ltrMilestones" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>




    </section>
    <div class="gap-60"></div>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/timeline.min.css" />

    <!-- Template custom-->

    <script src="Content/js/timeline.min.js"></script>

    <script>
        timeline(document.querySelectorAll('.timeline'), {
            forceVerticalMode: 700,
            mode: 'horizontal',
            verticalStartPosition: 'left',
            visibleItems: 4
        });
    </script>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Overview",
    "item": "https://www.alchemycapital.com/overview.aspx"  
  }]
}
    </script>


</asp:Content>



