﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegulatoryInformation : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMediaCenter();
            BindInnerBanner(10);
        }
    }

    public void BindInnerBanner(int Id)
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
        if (dt.Rows.Count > 0)
        {
            rptBanner.DataSource = dt;
            rptBanner.DataBind();
        }
        else
        {
            rptBanner.DataSource = null;
            rptBanner.DataBind();
        }


    }



    public void BindMediaCenter()
    {
        StringBuilder strTitle = new StringBuilder();
        StringBuilder strDesc = new StringBuilder();

        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_RegulatoryInformation 'RegulatoryInformationTitle'");
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                strTitle.Append("<li><img src='Content/uploads/Icon/" + dr["Icon"].ToString() + "' alt='' aria-hidden='true' /> <span>" + dr["Title"].ToString() + "</span></li>");

                strDesc.Append("<div>");
                strDesc.Append("<h3>" + dr["Title"].ToString() + "</h3>");
 
                DataTable dt1 = new DataTable();
                dt1 = utility.Display("Exec Proc_RegulatoryInformation 'RegulatoryInformationTitleById',0," + dr["Id"].ToString() + "");


                strDesc.Append("<ul class='newsLetter'>");

                if (dt1.Rows.Count > 0)
                {
                    if (dt1.Rows[0]["Type"].ToString() != "Description")
                    {

                        foreach (DataRow dr1 in dt1.Rows)
                        {
                            //strDesc.Append("<li class='newsData'>");
                            //if (dr1["Type"].ToString() == "Url")
                            //{
                            //    strDesc.Append("<a href='" + dr1["Url"].ToString() + "' target='_blank'>");
                            //}

                            //else if (dr1["Type"].ToString() == "Pdf")
                            //{
                            //    strDesc.Append("<a href='Content/uploads/Pdf/" + dr1["Pdf"].ToString() + "' target='_blank'>");
                            //}

                            //else
                            //{
                            //    strDesc.Append("<a href='Content/uploads/MediaImages/" + dr1["Image"].ToString() + "' target='_blank'>");
                            //}

                            //strDesc.Append("<span> <strong>" + dr1["Heading"].ToString() + "</strong><img src='Content/uploads/Icon/pdf_icon.png.png'></span>");
                            //strDesc.Append("</a>");
                            //strDesc.Append("</li>");

                            strDesc.Append("<li class='newsData'>");
                            if (dr1["Type"].ToString() == "Pdf")
                            {
                                strDesc.Append("<a href='Content/uploads/Pdf/" + dr1["Pdf"].ToString() + "' target='_blank'>");
                            }
                            else if (dr1["Type"].ToString() == "Image")
                            {
                                strDesc.Append("<a href='Content/uploads/MediaImages/" + dr1["MediaImages"].ToString() + "' target='_blank'>");
                            }
                            else
                            {
                                strDesc.Append("<a href='" + dr1["Url"].ToString() + "' target='_blank'>");
                            }

                            strDesc.Append("<div class='newsDate'><i class='fa fa-file-pdf-o'aria-hidden='true'></i></div>");
                            strDesc.Append("<span><strong>" + dr1["Heading"].ToString() + "</strong></span>");
                            strDesc.Append("</a>");
                            strDesc.Append("</li>");
                        }
                        strDesc.Append("</ul>");
                    }
                    else
                    {
                        foreach (DataRow dr1 in dt1.Rows)
                        {
                            strDesc.Append("<div>");
                            strDesc.Append(dr1["Description"].ToString());
                            strDesc.Append("</div>");
                        }
                    }
                    

                }

                strDesc.Append("</ul>");
                strDesc.Append("</div>");

            }

        }
        ltrTab.Text = strTitle.ToString();
        ltrDescription.Text = strDesc.ToString();
    }
}