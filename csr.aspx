﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="csr.aspx.cs" Inherits="CSR" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Corporate Social Responsibility | Alchemy Capital</title>
   <meta name="description" content="Alchemy Capital is dedicated to providing contributions in a variety of areas of society with the goal of improving the living conditions of the destitute and oppressed and assisting them in living with dignity."/>

     <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/csr.aspx" />


    <link rel="canonical" href="https://www.alchemycapital.com/csr.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/portfolio-management-services/alchemy-ascent.aspx" hreflang="x-default" />


    <style>
        .social_resp {
            text-align:center;
        }
              .social_resp p{
            text-align:center!important;
        }
    </style>


</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Corporate Social Responsibility</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li><a href="javascript:void(0);">Who We Are</a></li>
                                    <li>CSR</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40" id="main-container">


        <div class="container mrb-40">

         <%--   <h1 class="column-title title-small text-center">Corporate Social Responsibility</h1>--%>
            <div class="clearfix mrb-40"></div>
            <div id="verticalTab">
                <ul class="resp-tabs-list">
                    <asp:Literal ID="ltrHeading" runat="server"></asp:Literal>
                </ul>
                <div class="resp-tabs-container">
                    <asp:Literal ID="ltrDescription" runat="server"></asp:Literal>
                </div>
            </div>


            <div class="clearfix"></div>
            <asp:Literal ID="ltrCsrPdf" runat="server"></asp:Literal>
        </div>


    </section>
    <!-- #BeginLibraryItem "/Library/footer.lbi" -->

    <!--
      Javascript Files
      ==================================================
      -->
   

    

    <link rel="stylesheet" type="text/css" href="Content/css/inner_page.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/extra.css" />
    <link rel="stylesheet" type="text/css" href="Content/css/easy-responsive-tabs.css" />


    
    	<script>
		$(document).ready(function() {
			var owl = $('.csr-carousel');
			owl.owlCarousel({
				margin: 0,
				nav: true,
				loop: true,
				autoplay:true,
				autoplayTimeout:2000,
				autoplayHoverPause:true,
				responsive: {
					0: {
						items: 1
					},
				}
			})
		})
	</script>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "CSR",
    "item": "https://www.alchemycapital.com/csr.aspx"  
  }]
}
</script>


</asp:Content>

