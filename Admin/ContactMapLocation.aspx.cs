﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ContactMapLocation : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindGrid();
        }
       
    }

    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_ContactMapLocation 'GetLocation'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ContactMapLocation"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "Add");
                    cmd.Parameters.AddWithValue("@Latitude", txtLatitude.Text);
                    cmd.Parameters.AddWithValue("@Longitude", txtLongitude.Text);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);

                    if (utility.Execute(cmd))
                    {
                        BindGrid();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }


                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ContactMapLocation"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "Update");
                    cmd.Parameters.AddWithValue("@Id", hdnMapId.Value);
                    cmd.Parameters.AddWithValue("@Latitude", txtLatitude.Text.Trim());
                    cmd.Parameters.AddWithValue("@Longitude", txtLongitude.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        BindGrid();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully Updated");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }

        }
    }



    public bool CheckSave()
    {
        bool isOK = true;
        string Message = string.Empty;
        if (txtLatitude.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message = "Latitude, ";
        }
         if (txtLongitude.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message += "Longitude, ";
        }
         if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message += "Description, ";
        }
         if (Message.Length>0)
        {
            Message = Message.Substring(0, Message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + Message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string Message = string.Empty;
        if (txtLatitude.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message = "Latitude, ";
        }
         if (txtLongitude.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message += "Longitude, ";
        }
         if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            Message += "Description, ";
        }
         if (Message.Length > 0)
        {
            Message = Message.Substring(0, Message.Length - 2);
        }
         if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + Message);
        }
        return isOK;
    }


    private void Reset()
    {
        txtLatitude.Text = string.Empty;
        txtLongitude.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";
    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        DataTable dt = new DataTable();
        int mapId = Convert.ToInt32(gdView.DataKeys[e.RowIndex].Values[0]);
        using (SqlCommand cmd = new SqlCommand("Proc_ContactMapLocation"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Para", "Delete");
            cmd.Parameters.AddWithValue("@Id", mapId);
            if (utility.Execute(cmd))
            {
                Reset();
                BindGrid();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DataTable dt = new DataTable();
        int mapId = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

        dt = utility.Display("Exec Proc_ContactMapLocation 'GetLocationById','"+mapId+"'");
        try
        {
            if (dt.Rows.Count > 0)
            {
                hdnMapId.Value = dt.Rows[0]["Id"].ToString();
               txtLatitude.Text = dt.Rows[0]["Latitude"].ToString();
                txtLongitude.Text = dt.Rows[0]["Longitude"].ToString();
                txtDescription.Text = dt.Rows[0]["Description"].ToString();

                btnsumbit.Text = "Update";
            }
        }
        catch(Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void btnreset_Click1(object sender, EventArgs e)
    {
        Reset();
    }
}