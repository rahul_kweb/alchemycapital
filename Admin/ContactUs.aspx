﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="Admin_ContactUs" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Contact us Enquiry List</h3>
                </div>


                <div class="title_right">
                    <div class="col-xs-12 form-group text-right ">
                        <asp:Button ID="btnDirectExport" Text="Export All to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnDirectExport_Click" />

                    </div>
                    <div class="col-xs-12 text-right ">

                        <div class="row">
                            <div class="col-xs-3 form-group">
                                <asp:TextBox ID="txtFrom" placeholder="From" runat="server" autocomplete="off" CssClass="calender form-control"></asp:TextBox>
                            </div>

                            <div class="col-xs-3 form-group">
                                <asp:TextBox ID="txtTo" placeholder="To" runat="server" autocomplete="off" CssClass="calender form-control"></asp:TextBox>
                            </div>

                            <%--   <div class="col-xs-4 form-group">
                                <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnExport_Click" /></div>
                        </div>--%>

                            <div class="col-xs-3 form-group">
                                <asp:Button ID="btnFilterGrid" Text="Filter Data" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnFilterGrid_Click" />
                            </div>

                            <div class="col-xs-3 form-group">
                                <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnExport_Click" />
                            </div>

                        </div>
                        <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                    </div>

                </div>
                <%--     <div class="x_content">
                         
                            <br>
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                                <asp:HiddenField ID="hdnId" runat="server" />
                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Staus <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control col-md-7 col-xs-12 type">
                                            <asp:ListItem Value="Select Status" Text="Select Status"></asp:ListItem>
                                            <asp:ListItem Value="Converted" Text="Converted">Converted</asp:ListItem>
                                            <asp:ListItem Value="Follow Up" Text="Follow Up">Follow Up</asp:ListItem>
                                            <asp:ListItem Value="False Lead" Text="False Lead">False Lead</asp:ListItem>
                                            <asp:ListItem Value="Not Eligible" Text="Not Eligible">Not Eligible</asp:ListItem>
                                            <asp:ListItem Value="Pending update" Text="Pending update">Pending update</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Status Comment <span class="required" style="color: red">*</span>
                                    </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">                                  
                                         <asp:TextBox ID="txtStatusComment" runat="server" TextMode="MultiLine"  Width="100%" Height="150px"></asp:TextBox>                           
                                </div>
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="submit" Text="Update" value="Submit" class="btn btn-success" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                <%--                <div class="title_right">
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group pull-right top_search">
                        <div class="input-group pull-right">

                            <asp:TextBox ID="txtFrom" placeholder="From" autocomplete="off" runat="server" CssClass="calender"></asp:TextBox>
                            <asp:TextBox ID="txtTo" placeholder="To" autocomplete="off" runat="server" CssClass="calender"></asp:TextBox>

                            <span class="input-group-btn"></span>
                   
                                <asp:Button ID="btnExport" Text="Export to Excel" runat="server" CssClass="fa-hover col-md-12 col-sm-12 btn btn-success exporttoexcel" OnClick="btnExport_Click" />
                            </div>
                          
                        </div>

                    </div>--%>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" DataKeyNames="Id"
                                OnRowEditing="gdView_RowEditing" OnRowCancelingEdit="gdView_RowCancelingEdit"
                                OnRowUpdating="gdView_RowUpdating" class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="true" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." ReadOnly="true" />
                                    <asp:BoundField DataField="FullName" HeaderText="Full Name" ReadOnly="true" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="true" />
                                    <asp:BoundField DataField="ContactNo" HeaderText="Phone" ReadOnly="true" />
                                    <asp:BoundField DataField="Interest" HeaderText="I'm Intrested in" ReadOnly="true" />
                                    <asp:BoundField DataField="City" HeaderText="City" ReadOnly="true" />
                                    <asp:BoundField DataField="Investing" HeaderText="How did you find out about Alchemy Capital" ReadOnly="true" />
                                    <%-- <asp:BoundField DataField="Product" HeaderText="Product" />--%>
                                    <asp:BoundField DataField="Comment" HeaderText="Comment" ReadOnly="true" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_source" HeaderText="utm_source" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_medium" HeaderText="utm_medium" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_campaign" HeaderText="utm_campaign" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_device" HeaderText="utm_device" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_content" HeaderText="utm_content" ReadOnly="true" />
                                    <asp:BoundField DataField="utm_term" HeaderText="utm_term" ReadOnly="true" />
                                    <asp:BoundField DataField="Referrer_Website_SEO" HeaderText="Referrer (Website & SEO)" ReadOnly="true" />
                                    <asp:BoundField DataField="Request_from_Ascent_Pop_up" HeaderText="Request from Ascent Pop-up" ReadOnly="true" />
                                    <asp:BoundField DataField="CityName" HeaderText="City" ReadOnly="true" />
                                    <asp:BoundField DataField="Region" HeaderText="Region" ReadOnly="true" />
                                    <asp:BoundField DataField="CountryName" HeaderText="Country" ReadOnly="true" />
                                    <asp:TemplateField HeaderText="Status">
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlStatus" runat="server" SelectedValue='<%# Bind("Status") %>'>
                                                <asp:ListItem Value="Select Status" Text="Select Status"></asp:ListItem>
                                                <asp:ListItem Value="Converted" Text="Converted">Converted</asp:ListItem>
                                                <asp:ListItem Value="Follow Up" Text="Follow Up">Follow Up</asp:ListItem>
                                                <asp:ListItem Value="False Lead" Text="False Lead">False Lead</asp:ListItem>
                                                <asp:ListItem Value="Not Eligible" Text="Not Eligible">Not Eligible</asp:ListItem>
                                                <asp:ListItem Value="Pending update" Text="Pending update">Pending update</asp:ListItem>
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status Comment">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtStatusComment" runat="server" autocomplete="off" Text='<%# Bind("Status_Comment") %>'>  
                                            </asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStatusComment" runat="server" Text='<%# Bind("Status_Comment") %>'>  
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ShowEditButton="True" />
                                    <asp:CommandField ShowDeleteButton="True" />
                                    <%--<asp:BoundField DataField="Status" HeaderText="Status" />
                                     <asp:BoundField DataField="Status_Comment" HeaderText="Status Comment" />--%>


                                    <%--     <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--  --%>
    </div>

    <script>
        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>

    <style>
        /* MODIFIED DATE PICKER */
        .ui-datepicker {
            width: 250px !important;
            padding: 2px !important;
            font-size: 14px !important;
        }

            .ui-datepicker table {
                margin: 0 !important;
            }

            .ui-datepicker .ui-widget-header {
                background: #04869a !important;
                /*color: #fff;*/
                font-weight: normal;
            }

        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }

        .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
            margin: 0px 4px !important;
            font-size: 13px !important;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }
    </style>
</asp:Content>

