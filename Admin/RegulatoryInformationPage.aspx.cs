﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_RegulatoryInformationPage : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Id"] != null && Request.QueryString["Id"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["Id"], out testInt))
            {
                hdnId.Value = Request.QueryString["Id"];
                if (!Page.IsPostBack)
                {
                    BindGrid();

                }
            }
        }
    }




    public void BindGrid()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_RegulatoryInformation 'get',0,'" + hdnId.Value + "'");
        if (ds.Tables.Count > 0)
        {
            lblHeading.Text = ds.Tables[1].Rows[0]["Title"].ToString();
            //ViewBag.Id = ds.Tables[1].Rows[0]["Id"].ToString();
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();


        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Pdf/";

        string extImage = string.Empty;
        string MainImage_1 = string.Empty;
        string VirtualPartImages = "~/Content/uploads/MediaImages/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }


                else if (fileUploadImage.HasFile)
                {
                    extImage = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extImage))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage_1 = utility.GetUniqueName(VirtualPartImages, "MediaImage-", extImage, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPartImages + MainImage_1 + extImage));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_RegulatoryInformation"))
                {
                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);

                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());
                    cmd.Parameters.AddWithValue("@Type", ddlType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Status", val);
                    cmd.Parameters.AddWithValue("@Image", MainImage_1 + extImage);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadPdf.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                    if (!utility.IsValidPDFFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Pdf file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Pdf-", ext, this, false);
                    fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                else if (fileUploadImage.HasFile)
                {
                    extImage = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extImage))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage_1 = utility.GetUniqueName(VirtualPartImages, "MediaImage-", extImage, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPartImages + MainImage_1 + extImage));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_RegulatoryInformation"))
                {


                    int val;
                    if (ckstatus.Checked)
                    {
                        val = 1;
                    }
                    else
                    {
                        val = 0;
                    }

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@RInfoId", hdnItsId.Value);

                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Pdf", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());

                    cmd.Parameters.AddWithValue("@Type", ddlType.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Status", val);
                    cmd.Parameters.AddWithValue("@Image", MainImage_1 + extImage);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());

                    if (utility.Execute(cmd))
                    {

                        Reset();
                        BindGrid();

                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        //if (!fileUploadImage.HasFile)
        //{
        //    message = "Banner,";
        //    isOK = false;
        //}



        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Heading , ";
        }

        if (ddlType.SelectedValue != "select Type")
        {

            if (ddlType.SelectedValue == "Pdf")
            {
                if (!fileUploadPdf.HasFile)
                {
                    message += "pdf ,";
                    isOK = false;
                }
            }

            else if (ddlType.SelectedValue == "Image")
            {
                if (!fileUploadImage.HasFile)
                {
                    message += "Image ,";
                    isOK = false;
                }
            }

            else if (ddlType.SelectedValue == "Url")
            {
                if (txtUrl.Text.Trim().Equals(string.Empty))
                {
                    message += "Url ,";
                    isOK = false;
                }
            }
            else
            {
                if (txtDescription.Text.Trim().Equals(string.Empty))
                {
                    message += "Description ,";
                    isOK = false;
                }
            }
        }

        if (ddlType.SelectedValue == "select Type")
        {
            message += "Select Type ,";
        }



        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }



        if (ddlType.SelectedValue == "select Type")
        {
            isOK = false;
            message += "Select Type, ";

        }
        else
        {
            if (ddlType.SelectedValue == "Url")
            {
                if (txtUrl.Text.Trim().Equals(string.Empty))
                {
                    message += "Url, ";
                    isOK = false;
                }
            }
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            message += "Display Order, ";
            isOK = false;
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnItsId.Value = string.Empty;
        txtHeading.Text = string.Empty;
        //ddlYear.SelectedIndex = -1;
        txtUrl.Text = string.Empty;
        ddlType.SelectedValue = "select Type";
        ckstatus.Checked = false;
        //PdfPreview.Visible = false;
        ImagePreview.Visible = false;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";


    }
    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_RegulatoryInformation"))
            {
                int Id = (int)gdView.DataKeys[e.RowIndex].Value;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "delete");
                cmd.Parameters.AddWithValue("@ItsId", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }

    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_RegulatoryInformation 'getbyId', '" + Id + "'");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            hdnItsId.Value = dt.Rows[0]["RInfoId"].ToString();
            txtHeading.Text = dt.Rows[0]["Heading"].ToString();
            txtUrl.Text = dt.Rows[0]["Url"].ToString();
            ddlType.Text = dt.Rows[0]["Type"].ToString();
            PdfPreview.NavigateUrl = string.Format("../Content/uploads/Pdf/" + dt.Rows[0]["Pdf"].ToString());
            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/Content/uploads/MediaImages/" + dt.Rows[0]["Image"].ToString());
            txtDescription.Text = dt.Rows[0]["Description"].ToString();
            txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();

            if (int.Parse(dt.Rows[0]["Status"].ToString()) == 1)
            {
                ckstatus.Checked = true;
            }
            else
            {
                ckstatus.Checked = false;
            }


            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }

    }
}