﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="SocialMedia.aspx.cs" Inherits="Admin_SocialMedia" ValidateRequest="false" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <% if (hdnId.Value == "1")
                                {%>
                            <h2>Reach Us</h2>
                            <%}
                                else
                                {%>
                            <h2>Contact Us</h2>
                            <%}%>

                            <asp:Label ID="lblHeading" runat="server"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                                <asp:HiddenField ID="hdnId" runat="server" />

                                <% if (hdnId.Value == "1")
                                    {%>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Head Office<span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtHeadOffice" runat="server" Width="100%" Height="100px"></CKEditor:CKEditorControl>
                                        <%-- <asp:TextBox ID="txtHeadOffice" TextMode="MultiLine" CssClass="form-control col-md-7 col-xs-12" runat="server" ></asp:TextBox>--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Call Us  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtCallUs" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>
                                        <%--                                        <asp:TextBox ID="txtCallUs" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Mail Us  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtMailUs" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>
                                        <%--                                        <asp:TextBox ID="txtMailUs" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <%}
                                    else
                                    {%>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Find Us  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtFindUs" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>
                                        <%--                                        <asp:TextBox ID="txtFindUs" TextMode="MultiLine" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Existing Clients  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtCallUs2" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>

                                        <%-- <asp:TextBox ID="txtCallUs2" TextMode="MultiLine" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                       Invest With Us   <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <CKEditor:CKEditorControl ID="txtClient" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>
<%--                                        <asp:TextBox ID="txtClient" TextMode="MultiLine" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Partners  <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <CKEditor:CKEditorControl ID="txtPartners" runat="server" Width="100%" Height="150px"></CKEditor:CKEditorControl>
<%--                                        <asp:TextBox ID="txtPartners" TextMode="MultiLine" CssClass="form-control col-md-7 col-xs-12" runat="server"></asp:TextBox>--%>
                                    </div>
                                </div>
                                <%}%>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Update" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

