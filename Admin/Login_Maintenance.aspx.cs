﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Login_Maintenance : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }


    public void BindGrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("select * from Login_Maintenance");
        if (dt.Rows.Count > 0)
        {
            //gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        if (CheckUpdate())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Login_Maintenance"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                //cmd.Parameters.AddWithValue("@Login_Url", ddlActive.Text);
                cmd.Parameters.AddWithValue("@ActiveUrl", ddlActive.SelectedItem.Text);


                if (utility.Execute(cmd))
                {
                    Reset();
                    BindGrid();
                    MyMessageBox1.ShowSuccess("Successfully updated");

                }
                else
                {
                    MyMessageBox1.ShowWarning("Unable to update");
                }
            }
        }
    }



    public bool CheckUpdate()
    {

        bool isOK = true;
        string message = string.Empty;

        if (ddlActive.SelectedValue == "Select Active Status")
        {
            isOK = false;
            message = "Select Active Status, ";

        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }




    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }


    public void Reset()
    {
        txtUrl.Text = string.Empty;
        hdnId.Value = string.Empty;
        ddlActive.SelectedValue = "Select Active Status";
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = (int)gdView.DataKeys[e.NewSelectedIndex].Values[0];
            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_Login_Maintenance 'getbyId',"+Id+"");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            txtUrl.Text = dt.Rows[0]["Login_Url"].ToString();
            ddlActive.SelectedValue = dt.Rows[0]["ActiveUrl"].ToString();


        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }



    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }
}