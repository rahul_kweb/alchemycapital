﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_InnerBanner : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindInnerBanner();
        }
    }


    public void BindInnerBanner()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_InnerBanner 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }



    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/InnerBanner/";

        if (fileUploadImage.HasFile)
        {
            ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
            if (!utility.IsValidImageFileExtension(ext))
            {
                MyMessageBox1.ShowError("Only Image file allowed.");
                return; // STOP FURTHER PROCESSING
            }

            MainImage = utility.GetUniqueName(VirtualPart, "InnerBanner-", ext, this, false);
            fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
    }


        using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
        {
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", hdnId.Value);
            cmd.Parameters.AddWithValue("@Image", MainImage + ext);
            cmd.Parameters.AddWithValue("@PageName", txtPageName.Text.Trim());


            if (utility.Execute(cmd))
            {
                Reset();
                    BindInnerBanner();
                MyMessageBox1.ShowSuccess("Successfully updated");

            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to update");
            }
        }
        }




    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindInnerBanner();
    }


    public void Reset()
    {
        txtPageName.Text = string.Empty;
        hdnId.Value = string.Empty;
        ImagePreview.Visible = false;
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = (int)gdView.DataKeys[e.NewSelectedIndex].Values[0];       
            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_InnerBanner 'getbyId','" + Id + "'");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            txtPageName.Text = dt.Rows[0]["PageName"].ToString();
            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/Content/uploads/InnerBanner/" + dt.Rows[0]["Image"].ToString());

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }



    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }
}