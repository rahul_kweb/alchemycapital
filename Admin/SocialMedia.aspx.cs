﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_SocialMedia : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["Id"] != null && Request.QueryString["Id"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["Id"], out testInt))
            {
                hdnId.Value = Request.QueryString["Id"];
                if (!Page.IsPostBack)
                {
                    Bind();
                }
            }
        }
    }

    public void Bind()
    {
        DataTable dt = new DataTable();
        try
        {
            hdnId.Value = Request.QueryString["Id"].ToString();


            dt = utility.Display("Exec Proc_SocialMedia 'getbyId','" + hdnId.Value + "'");

            if (dt.Rows.Count > 0)
            {

                hdnId.Value = dt.Rows[0]["Id"].ToString();
                txtHeadOffice.Text = dt.Rows[0]["HeadOffice"].ToString();
                txtCallUs.Text = dt.Rows[0]["CallUs"].ToString();
                txtMailUs.Text = dt.Rows[0]["MailUs"].ToString();
               txtFindUs.Text = dt.Rows[0]["FindUs"].ToString();
                txtCallUs2.Text= dt.Rows[0]["CallUs2"].ToString();
                txtClient.Text = dt.Rows[0]["Clients"].ToString();
               txtPartners.Text = dt.Rows[0]["Partners"].ToString();

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

            dt = null;

        }
    }
    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        try
        {
            if (hdnId.Value == "1")
            {
                if (CheckSave1())
                {

                    try
                    {
                        using (SqlCommand cmd = new SqlCommand("Proc_SocialMedia"))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@para", "update");
                            cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                            cmd.Parameters.AddWithValue("@HeadOffice", txtHeadOffice.Text.Trim());
                            cmd.Parameters.AddWithValue("@CallUs", txtCallUs.Text.Trim());
                            cmd.Parameters.AddWithValue("@MailUs", txtMailUs.Text);
                            cmd.Parameters.AddWithValue("@FindUs", txtFindUs.Text);
                            cmd.Parameters.AddWithValue("@CallUs2", txtCallUs2.Text);
                            cmd.Parameters.AddWithValue("@Clients", txtClient.Text);
                            cmd.Parameters.AddWithValue("@Partners", txtPartners.Text);

                            dt = utility.Display(cmd);
                           
                            MyMessageBox1.ShowSuccess("Successfully updated");
                        }
                    }
                    catch 
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                    finally
                    {
                        utility = null;
                    }
                }
            }
            else {

                if (CheckSave2())
                {

                    try
                    {
                        using (SqlCommand cmd = new SqlCommand("Proc_SocialMedia"))
                        {
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@para", "update");
                            cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                            cmd.Parameters.AddWithValue("@HeadOffice", txtHeadOffice.Text.Trim());
                            cmd.Parameters.AddWithValue("@CallUs", txtCallUs.Text.Trim());
                            cmd.Parameters.AddWithValue("@MailUs", txtMailUs.Text);
                            cmd.Parameters.AddWithValue("@FindUs", txtFindUs.Text);
                            cmd.Parameters.AddWithValue("@CallUs2", txtCallUs2.Text);
                            cmd.Parameters.AddWithValue("@Clients", txtClient.Text);
                            cmd.Parameters.AddWithValue("@Partners", txtPartners.Text);

                            dt = utility.Display(cmd);
                            MyMessageBox1.ShowSuccess("Successfully updated");

                        }
                    }
                    catch
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                    finally
                    {
                        utility = null;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {


        }
    }

    public bool CheckSave1()
    {
        bool isOK = true;
        string message = string.Empty;
      
        if (txtHeadOffice.Text.Trim() == string.Empty)
        {
            isOK = false;
            message = "Head Office,";
        }

        if (txtCallUs.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Call Us,";
        }

        if (txtMailUs.Text.Trim() == string.Empty)
        {
            isOK = false;
            message +="Mail Us,";
        }
        

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    public bool CheckSave2()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFindUs.Text.Trim() == string.Empty)
        {
            isOK = false;
            message = "Find Us,";
        }


        if (txtCallUs2.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Call Us,";

        }


        if (txtClient.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Client,";
        }


        if (txtPartners.Text.Trim() == string.Empty)
        {
            isOK = false;
            message += "Partners,";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnreset_Click(object sender, EventArgs e)
{
        Bind();
}

}