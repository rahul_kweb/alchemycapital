﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class Admin_WhatWeOfferContent : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.QueryString["TabId"] != null && Request.QueryString["TabId"] != "")
            {
                int testInt;
                if (int.TryParse(Request.QueryString["TabId"], out testInt))
                {
                    hdnTabId.Value = Request.QueryString["TabId"];

                    BindContent();

                }
            }
        }
    }

    public void BindContent()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_WhatWeOfferContents 'get',0,'" + hdnTabId.Value + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    

    protected void btnsumbit_Click (object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/InnerBanner/";

        string extIcon = string.Empty;
        string MainImageIcon = string.Empty;
        string VirtualPartIcon = "~/Content/uploads/Icon/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "InnerBanner-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

               else  if (fileUploadIconImage.HasFile)
                {
                    extIcon = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extIcon))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageIcon = utility.GetUniqueName(VirtualPartIcon, "Icon-", extIcon, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPartIcon + MainImageIcon + extIcon));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_WhatWeOfferContents"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Id", hdnTabId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());
                    cmd.Parameters.AddWithValue("@BannerImage", MainImage + ext);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindContent();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "InnerBanner-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

               else if (fileUploadIconImage.HasFile)
                {
                    extIcon = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extIcon))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageIcon = utility.GetUniqueName(VirtualPartIcon, "Icon-", extIcon, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPartIcon + MainImageIcon + extIcon));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_WhatWeOfferContents"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@Id", hdnTabId.Value);
                    cmd.Parameters.AddWithValue("@WhatOfferId", hdnContentId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());
                    cmd.Parameters.AddWithValue("@BannerImage", MainImage + ext);

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindContent();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

      

        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }

        if (!fileUploadIconImage.HasFile)
        {
            isOK = false;
            message += "Icon, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (!fileUploadImage.HasFile)
        {
            message += "Banner Image, ";
            isOK = false;
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "DisplayOrder, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "DisplayOrder, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnContentId.Value = string.Empty;
        txtTitle.Text = string.Empty;
        IconImagePreview.Visible = false;
        txtDescription.Text = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;

    }


    protected void btnreset_Click (object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_WhatWeOfferContents"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@Id", hdnTabId.Value);
            cmd.Parameters.AddWithValue("@WhatOfferId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindContent();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int WhatOfferId = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_WhatWeOfferContents 'getbyId','" + WhatOfferId + "'");
            if (dt.Rows.Count > 0)
            {
                hdnTabId.Value = dt.Rows[0]["Id"].ToString();
                hdnContentId.Value = dt.Rows[0]["WhatOfferId"].ToString();
                txtTitle.Text = dt.Rows[0]["Title"].ToString();
                IconImagePreview.Visible = true;
                IconImagePreview.ImageUrl = string.Format("~/Content/uploads/Icon/" + dt.Rows[0]["Icon"].ToString());
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();
                ImagePreview.Visible = true;
                ImagePreview.ImageUrl = string.Format("~/Content/uploads/InnerBanner/" + dt.Rows[0]["BannerImage"].ToString());

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindContent();
    }

}