﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ForgetPasswod : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string response = "";
        DataTable dt = new DataTable();   
            dt = utility.Display("Exec Proc_SuperAdminLogin 'FORGOT_PASSWORD','','','','" + txtEmailId.Text.ToString().Trim() + "'");
        try
        {
            if (dt.Rows.Count > 0)
            {

                response = SendEmail(dt.Rows[0]["Name"].ToString(), dt.Rows[0]["Username"].ToString(), dt.Rows[0]["Password"].ToString(), "SuperAdmin", dt.Rows[0]["Emailid"].ToString());
                lblStatus.Text = "Password sent to your email address.";
                lblStatus.Attributes["style"] = "color:green; font-weight:bold;";
                lblStatus.Visible = true;

            }
            else
            {
                response = "";
            }
        }
            catch
            {
                response = "";
                lblStatus.Text = "Invalid username or password";
                lblStatus.Visible = true;
            }

           

    }

    public string SendEmail(string name, string userid, string password, string usertype, string ToEmailid)
    {
        string response = string.Empty;
        if (name != "")
        {
            StringBuilder sb = new StringBuilder();
            string Name = name;

            sb.Append("<table border='0' cellspacing='0' cellpadding='10' width='500' align='center' style='border: 1px solid #ccc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; line-height: 20px;'>");
            sb.Append("<tr><td colspan='2' align='center'><img src='../Content/images/alchemy_logo.png' style='width: auto;max-height:100px;' /><br /><hr color='#04869a' /></td></tr>");
            sb.Append("<tr><td colspan='2'>Dear " + Name + ",<br /> Find your login details below :</td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td width='90'>User Name:</td><td><strong> " + userid + " </strong> </td> </tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td>Password :</td><td><strong> " + password + " </strong> </td></tr>");
            sb.Append("<tr bgcolor='#f4f4f4'><td colspan='2' align='center' height='5'></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("<tr><td colspan='2'>Regards,<br /><strong><font color='#04869a'>Alchemy</font></strong></td></tr>");
            sb.Append("<tr><td colspan='2' align='center' height='1'></td></tr>");
            sb.Append("</table>");

            String[] emailid = new String[] { ToEmailid };


            bool result = utility.SendEmail(sb.ToString(), emailid, "Alchemy Forgot Password", "", null);

            if (result)
            {
                response = "sent";
                Reset();
            }
            else
            {
                response = "";
            }


        }
        else
        {
            response = "";
        }
        return response;
    }


    private void Reset()
    {
        txtEmailId.Text = "";
        lblStatus.Visible = false;
    }

}