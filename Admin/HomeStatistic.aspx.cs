﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_HomeStatistic : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindHomeStatistic();
        }
    }


    public void BindHomeStatistic()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_HomeStatistic 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Icon/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "Add");
                    cmd.Parameters.AddWithValue("@Counting", txtCount.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                   

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindHomeStatistic();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else {
            if (CheckUpdate())
                {

                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "Update");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Counting", txtCount.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindHomeStatistic();
                        MyMessageBox1.ShowSuccess("Successfully Updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to Update");
                    }
                }
            }

        }


    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

      


        if (!fileUploadIconImage.HasFile)
        {
            message = "Icon, ";
            isOK = false;
        }

        if (txtCount.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Count, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtCount.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Count, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {

        hdnId.Value = string.Empty;
        txtCount.Text = string.Empty;
        IconImagePreview.Visible = false;
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";

    }


    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }




    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

            dt = utility.Display("Exec Proc_HomeStatistic 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["Id"].ToString();
                txtCount.Text = dt.Rows[0]["Counting"].ToString();
                IconImagePreview.Visible = true;
                IconImagePreview.ImageUrl = string.Format("~/Content/uploads/Icon/" + dt.Rows[0]["Icon"].ToString());
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                btnsumbit.Text = "Update";

            }

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;

        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
           
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "Delete");
            cmd.Parameters.AddWithValue("@Id", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindHomeStatistic();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindHomeStatistic();
    }
}