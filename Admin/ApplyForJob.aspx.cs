﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Admin_ApplyForJob : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindApplyForJob();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }

    public void BindApplyForJob()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_ApplyForJob 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }
    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_ApplyForJob"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@Id", Id);
            if (utility.Execute(cmd))
            {
                BindApplyForJob();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindApplyForJob();
             if (check1())
        {
            GetFilterDatafromDatabase();
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        //if (check())
        //{
        //    Response.ClearContent();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Applyforjob.xls"));
        //    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    //Response.ContentType = "application/ms-excel";
        //    DataTable dt = GetDatafromDatabase();

        //    string str = string.Empty;
        //    foreach (DataColumn dtcol in dt.Columns)
        //    {
        //        Response.Write(str + dtcol.ColumnName);
        //        str = "\t";
        //    }
        //    Response.Write("\n");
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        str = "";
        //        for (int j = 0; j < dt.Columns.Count; j++)
        //        {
        //            Response.Write(str + Convert.ToString(dr[j]));
        //            str = "\t";
        //        }
        //        Response.Write("\n");
        //    }
        //    Response.End();
        //}

        if (check())
        {
        
            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = GetDatafromDatabase();
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= Applyforjob.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
               
            }
        }
    }

    protected DataSet GetDatafromDatabase()
    {
        //DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_ApplyForJob 'GetForExportToExcel','','','','','','','" + postDatefrom + "','" + postDateto + "'");
        return ds;
    }

    public bool check()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "From Date, ";
        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "To Date, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public void Reset()
    {
        txtFrom.Text = "";
        txtTo.Text = ""; 
    }


    protected void btnDirectExport_Click(object sender, EventArgs e)
    {
        using (XLWorkbook wb = new XLWorkbook())
        {
            DataSet ds = new DataSet();
            ds = utility.Display1("Execute Proc_ApplyForJob 'GetForExportToExcelDirect'");
            wb.Worksheets.Add(ds);
            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            wb.Style.Font.Bold = true;

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename= Applyforjob.xlsx");

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
    

    protected void GetFilterDatafromDatabase()
    {
        //DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_ApplyForJob 'GetForExportToExcel','','','','','','','" + postDatefrom + "','" + postDateto + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnFilterGrid_Click(object sender, EventArgs e)
    {
        if (check())
        {        
            GetFilterDatafromDatabase();
         
        }
    }

    public bool check1()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
          
        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
         
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
     
        }
        return isOK;
    }

}