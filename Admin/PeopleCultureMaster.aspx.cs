﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_PeopleCultureMaster : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPeopleCultureMaster();
        }
    }


    public void BindPeopleCultureMaster()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_PeopleCultureMaster 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {

        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Icon/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }


                using (SqlCommand cmd = new SqlCommand("Proc_PeopleCultureMaster"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        BindPeopleCultureMaster();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }

            }
        }
        else
        {
            if (CheckUpdate())
            {
                if (fileUploadIconImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_PeopleCultureMaster"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@PcmId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Title", txtTitle.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImage + ext);
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        BindPeopleCultureMaster();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully updated");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }

                }
            }
        }
    }


    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }


        if (!fileUploadIconImage.HasFile)
        {
            message += "Icon, ";
            isOK = false;
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtTitle.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Title, ";
        }


        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {
        txtTitle.Text = string.Empty;
        IconImagePreview.Visible = false;
        hdnId.Value = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";

    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindPeopleCultureMaster();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PeopleCultureMaster"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@PcmId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindPeopleCultureMaster();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DataTable dt = new DataTable();
        int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
        dt = utility.Display("Exec Proc_PeopleCultureMaster 'getbyId','" + Id + "'");
        try
        {
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["PcmId"].ToString();
                txtTitle.Text = dt.Rows[0]["Title"].ToString();
                IconImagePreview.Visible = true;
                IconImagePreview.ImageUrl = string.Format("~/Content/uploads/Icon/" + dt.Rows[0]["Icon"].ToString());
                txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();
                btnsumbit.Text = "Update";
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }



    }


}