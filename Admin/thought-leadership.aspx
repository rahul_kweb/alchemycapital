﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="thought-leadership.aspx.cs" Inherits="Admin_thought_leadership" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Thought Leadership</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <div class="form-group">
                                    <asp:HiddenField ID="hdnId" runat="server" />
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Heading <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtHeding" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                            <%--    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Icon <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadIcon" CssClass="form-control col-md-7 col-xs-12" runat="server" />
                                        <asp:Image ID="IconPreview" runat="server" Text="View Icon" Visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
                                    </div>
                                </div>--%>

                                <div class="form-group">                           
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Image <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadImage" CssClass="form-control col-md-7 col-xs-12" runat="server" />
                                        <asp:Image ID="ImagePreview" runat="server" Text="View Image" Visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Post Date <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtDate" autocomplete="off" CssClass="form-control col-md-7 col-xs-12 calender" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Description <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <ckeditor:ckeditorcontrol id="txtDescription" runat="server" cssclass="form-control col-md-7 col-xs-12"></ckeditor:ckeditorcontrol>

                                    </div>
                                </div>
                                <%--   <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Display Order
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <button type="button" class='down_count btn btn-info' title='Down'><i class='fa fa-minus icon-minus'></i></button>
                                        <asp:TextBox ID="txtDisplayOrder" CssClass="counter" runat="server" Style="width: 45px" autocomplete="off"></asp:TextBox>
                                        <button type="button" class='up_count btn btn-info' title='Up'><i class='fa fa-plus icon-plus'></i></button>

                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="ThoughtId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="ThoughtId" HeaderText="Id" />
                                   <%-- <asp:BoundField DataField="Sr" HeaderText="Sr No." />--%>
                                    <asp:BoundField DataField="Heading" HeaderText="Heading" />
                                <%--    <asp:TemplateField HeaderText="Icon" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <img src='<%# Eval("Icon", "../Content/uploads/ThoughtLeadership/{0}") %>' height="150px" width="150px"></img>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Image" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <img src='<%# Eval("Image", "../Content/uploads/ThoughtLeadership/{0}") %>' height="150px" width="150px"></img>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PostDate" HeaderText="PostDate" />
                                       <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <%#Eval("Description")%>                                          
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--          <asp:BoundField DataField="DisplayOrder" HeaderText="Order" />--%>

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="pagination"></PagerStyle>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>

        <script src="../Content/Admin/datepicker/jquery-ui.js" type="text/javascript"></script>




    <script>

        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });

        </script>

</asp:Content>

