﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CMS : AdminPage
{
    Utility utility = new Utility();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["cms"] != null && Request.QueryString["cms"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["cms"], out testInt))
            {
                hdnId.Value = Request.QueryString["cms"];
                if (!Page.IsPostBack)
                {
                    Bind();
                }
            }
        }
    }

    private void Bind()
    {

        try
        {
            hdnId.Value = Request.QueryString["cms"].ToString();
           

            dt = utility.Display("Execute Proc_CMS 'get','" + hdnId.Value + "'");

            if (dt.Rows.Count > 0)
            {

                txtDescription.Text = dt.Rows[0]["Description"].ToString();

                lblHeading.Text = dt.Rows[0]["Heading"].ToString();

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {

            dt = null;

        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {

            if (CheckSave())
            {

                try
                {

                    using (SqlCommand cmd = new SqlCommand("Proc_CMS"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PARA", "update");
                        cmd.Parameters.AddWithValue("@CmsId", hdnId.Value);
                        cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());

                        dt = utility.Display(cmd);
                      
                            MyMessageBox1.ShowSuccess("Successfully updated");
                        
                    }
                }
                catch 
                {
                    MyMessageBox1.ShowWarning("Unable to update");
                                      
                }
                finally
                {
                    utility = null;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {


        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;
        //if (txtDescription.Text.Trim() == string.Empty)
        //{
        //    isOK = false;
        //    message += "Content,";
        //}



        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }
}