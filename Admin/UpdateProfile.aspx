﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="UpdateProfile.aspx.cs" Inherits="Admin_UpdateProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <div class="right_col" role="main" style="min-height: 1500.99px;">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Update Profile</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br>
                        <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left">



                            <div class="col-md-offset-2 col-md-6">
                                  <%-- <form runat="server">--%>
                                <div class="form-group">
                                 
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                        Name <span class="required" style="color: red;">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtname" CssClass="form-control col-md-7 col-xs-12" runat="server" required="" autocomplete="off"></asp:TextBox>
                                        <span id="ErrorName">Name is required.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Emailid <span class="required" style="color: red;">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                     <asp:TextBox ID="txtemailid" CssClass="form-control col-md-7 col-xs-12" runat="server" required="" autocomplete="off"></asp:TextBox>
                                     <span id="ErrorEmail1">Email is required.</span>
                                            <span id="ErrorEmail2">Invalid email address.</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Mobileno<span class="required" style="color: red;">*</span>
                                    </label>
                                    <div class="col-md-9 col-sm-6 col-xs-12">
                                         <asp:TextBox ID="txtmobileno" CssClass="form-control col-md-7 col-xs-12" runat="server" MaxLength="10" required="" autocomplete="off"></asp:TextBox>
                                   <span id="ErrorPhone">Phone is required.</span>
                                         </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style="text-align: center;">
                                        <asp:HiddenField ID="hdnid" runat="server" />
                                        <asp:Button ID="btnUpdateDetails" Text="Update" class="btn btn-success" OnClick="btnUpdateDetails_Click" OnClientClick="javascript:return validation()"  runat="server"/>
                                    </div>
                                </div>
                                       <br />
                                       <br />
                                       <asp:Label ID="lblstatus" runat="server" Visible="false"></asp:Label>
                                       <%--</form>--%>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

     <style>
        #ErrorName {
           
            padding-top: 20px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorEmail1 {
            
            padding-top: 20px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorEmail2 {
           
            padding-top: 20px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

        #ErrorPhone {
            
            padding-top: 20px;
            padding-left: 0px;
            text-align: left;
            display: none;
            color: red;
            line-height: 50px;
        }

</style>

      <script type="text/javascript">
          //$(document).ready(function () {
          $('input').focusout(function () {
              validation();
              //});
          });


          function isNumberKey(evt) {

              var charCode = (evt.which) ? evt.which : event.keyCode;
              var textBox = document.getElementById('<%=txtmobileno.ClientID%>').value.length;
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
                return false;

            return true;
        }

        function validation() {
            var Name = $('#<%=txtname.ClientID%>').val();
            var Email = $('#<%=txtemailid.ClientID%>').val();
            var Phone = $('#<%=txtmobileno.ClientID%>').val();
          
            var blank = false;

            if (Name == '') {
                $('#ErrorName').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorName').css('display', 'none');

            }


            if (Email == '') {
                $('#ErrorEmail2').css('display', 'none');
                $('#ErrorEmail1').css('display', 'block');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $('#ErrorEmail1').css('display', 'none');
                    $('#ErrorEmail2').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorEmail2').css('display', 'none');


                }
                $('#ErrorEmail1').css('display', 'none');


            }

            if (Phone == '') {
                $('#ErrorPhone').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorPhone').css('display', 'none');

            }

        

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    
     <script src="../Content/Admin/vendors/jquery/dist/jquery.min.js"></script>
</asp:Content>

