﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_thought_leadership : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bindthoughtleadership();
        }
    }


    public void Bindthoughtleadership()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_ThoughtLeadership 'get'");

            if (dt.Rows.Count > 0)
            {
                gdView.DataSource = dt;
                gdView.DataBind();
                gdView.Columns[0].Visible = false;
            }
            else
            { 
            gdView.DataSource = null;
            gdView.DataBind();
            }
        }
        catch(Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }



    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/ThoughtLeadership/";



        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Image-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

         

                using (SqlCommand cmd = new SqlCommand("Proc_ThoughtLeadership"))
                {
                    string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "add");
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    //cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                    cmd.Parameters.AddWithValue("@Heading", txtHeding.Text.Trim());
                    cmd.Parameters.AddWithValue("@PostDate", postDate);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                   cmd.Parameters.AddWithValue("@Slug", Generateslg(txtHeding.Text.Trim()));


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        Bindthoughtleadership();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }

        else
        {
            if (fileUploadImage.HasFile)
            {
                ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                if (!utility.IsValidImageFileExtension(ext))
                {
                    MyMessageBox1.ShowError("Only Image file allowed.");
                    return; // STOP FURTHER PROCESSING
                }

                MainImage = utility.GetUniqueName(VirtualPart, "Icon-", ext, this, false);
                fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
            }


            using (SqlCommand cmd = new SqlCommand("Proc_ThoughtLeadership"))
            {
                string postDate = DateTime.ParseExact(txtDate.Text, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");

                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@ThoughtId", hdnId.Value);
                cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                //cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                cmd.Parameters.AddWithValue("@Heading", txtHeding.Text.Trim());
                cmd.Parameters.AddWithValue("@PostDate", postDate);
                cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                cmd.Parameters.AddWithValue("@Slug", Generateslg(txtHeding.Text.Trim()));


                if (utility.Execute(cmd))
                {
                    Reset();
                    Bindthoughtleadership();
                    MyMessageBox1.ShowSuccess("Successfully updated");

                }
                else
                {
                    MyMessageBox1.ShowWarning("Unable to updated");
                }
            }
        }
    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        //if (!fileUploadImage.HasFile)
        //{
        //    message = "Image, ";
        //    isOK = false;
        //}

        if (txtHeding.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }



        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Date, ";
        }

        //if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        //{
        //    isOK = false;
        //    message += "DisplayOrder, ";
        //}


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtHeding.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Date, ";
        }

        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {

        hdnId.Value = string.Empty;
        txtHeding.Text = string.Empty;
        txtDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;
      

    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        Bindthoughtleadership();
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_ThoughtLeadership 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["ThoughtId"].ToString();
                txtHeding.Text = dt.Rows[0]["Heading"].ToString();
                txtDate.Text = dt.Rows[0]["PostDate"].ToString();
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                ImagePreview.Visible = true;
                ImagePreview.ImageUrl = string.Format("~/Content/uploads/ThoughtLeadership/" + dt.Rows[0]["Image"].ToString());

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_ThoughtLeadership"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@ThoughtId",Id);
            if (utility.Execute(cmd))
            {
                Reset();
                Bindthoughtleadership();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    public string Generateslg(string strtext)
    {
        string str = strtext.ToString().ToLower();
        str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
        return str;
    }
}