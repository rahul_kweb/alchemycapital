﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Admin_Login" %>
<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <link rel="icon" type="image/png" sizes="16x16" href="../Content/images/favicon.ico">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <title>Super Admin Login</title>


    <!-- Bootstrap -->
    <link href="../Content/Admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="../Content/Admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../Content/Admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../Content/Admin/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../Content/Admin/build/css/custom.min.css" rel="stylesheet">

          <!-- Custom Theme Style -->
    <link href="../Content/Admin/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
      
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
           <form runat="server">
              <h1>Super Admin Login</h1>
              <div>
                  <asp:TextBox ID="txtUsername" class="form-control" placeholder="Username" required="" runat="server" autocomplete="off"></asp:TextBox>
              </div>
              <div>
                  <asp:TextBox ID="txtPassword" class="form-control" placeholder="Password" required="" TextMode="Password"  runat="server"></asp:TextBox>
              </div>
                <div>
                    <asp:Label ID="lblStatus" style="color:red" runat="server" Visible="false"></asp:Label>
                </div>
              <div>
                  <asp:Button ID="btnLogin" class="btn btn-default submit" Value="Log in" Text="Log in" OnClick="btnLogin_Click" Style="float: inherit;margin-left:0px" runat="server"  />
              </div>
              <div class="clearfix"></div>

                
              <div class="separator">
                <p class="change_link">Forget Password ?
                  <a href="ForgetPassword.aspx" class="to_register"> Click here </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="../Content/Admin/images/alchemy_logo.png" /></h1>
                  <p>© <%# DateTime.Now.Year.ToString() %> All Rights Reserved.</p>
                </div>
              </div>
           </form>
        </section>
      </div>         
    </div>
        </div>
  </body>
</html>

