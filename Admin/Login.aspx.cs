﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Login : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private bool CheckPage()
    {
        if (txtUsername.Text.Trim() == "")
        {
            return false;
        }

        if (txtPassword.Text.Trim() == "")
        {
            return false;
        }

        return true;
    }


    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (CheckPage())
        {
            using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                DataTable dTable = new DataTable();
                dTable = utility.Display("Exec Proc_SuperAdminLogin 'GET_LOGIN','','" + txtUsername.Text.ToString().Trim() + "','" + txtPassword.Text.ToString().Trim() + "'");
                if (dTable != null && dTable.Rows.Count > 0)
                {
                    lblStatus.Visible = false;
                    Session[AppKeys.SESSION_ADMIN_USERID_KEY] = dTable.Rows[0]["Id"].ToString();
                    Session[AppKeys.SESSION_ADMIN_USERNAME_KEY] = dTable.Rows[0]["Username"].ToString();

                    Response.Redirect("~/Admin/Index.aspx");
                }
                else
                {
                    lblStatus.Text = "Invalid username or password";
                    lblStatus.Visible = true;
                }
            }
        }

    }


}