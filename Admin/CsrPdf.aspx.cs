﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CsrPdf : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Getpdf();
        }
    }

    public void Getpdf()
    {

        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_CsrPdf 'Get'");
        if (dt.Rows.Count > 0)
        {
            hdnId.Value = dt.Rows[0]["PdfId"].ToString();
            PdfPreview.NavigateUrl = string.Format("/Content/uploads/Csrpdf/" + dt.Rows[0]["CsrPdf"].ToString());
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Csrpdf/";

        if (btnsumbit.Text == "Update")
        {
            if (fileUploadPdf.HasFile)
            {
                ext = System.IO.Path.GetExtension(fileUploadPdf.FileName).ToLower();
                if (!utility.IsValidPDFFileExtension(ext))
                {
                    MyMessageBox1.ShowError("Only Pdf file allowed.");
                    return; // STOP FURTHER PROCESSING
                }

                MainImage = utility.GetUniqueName(VirtualPart, "Pdf", ext, this, false);
                fileUploadPdf.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
            }

            using (SqlCommand cmd = new SqlCommand("Proc_CsrPdf"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@PdfId", hdnId.Value);
                cmd.Parameters.AddWithValue("@CsrPdf", MainImage + ext);

                if (utility.Execute(cmd))
                {
                    Getpdf();
                    MyMessageBox1.ShowSuccess("Successfully updated");
                }

            }
        }
    }
}