﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Admin_ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main" style="min-height: 1602.99px;">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Change Password</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                           <br/>
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left">
                                  <asp:Label ID="lblstatus" runat="server" class="alert alert-success alert-dismissable" Visible="false" Style="text-align:center"></asp:Label>
                                 <br />
                                  <br />

                       
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                    Old Password <span class="required" style="color: red;">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtOldPassword" CssClass="form-control col-md-7 col-xs-12" TextMode="Password" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="requireOldPassword" runat="server" ControlToValidate="txtOldPassword" ErrorMessage="Password is required." ></asp:RequiredFieldValidator>
                                    <label id="lblold"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                    New Password <span class="required" style="color: red;">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtNewPassword" CssClass="form-control col-md-7 col-xs-12" TextMode="Password" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="requireNewPassword" runat="server" ControlToValidate="txtNewPassword" ErrorMessage="New Password is required."></asp:RequiredFieldValidator>
                                    <label id="lblnew"></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                    Confirm Password <span class="required" style="color: red;">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtConfirmPassword" CssClass="form-control col-md-7 col-xs-12" TextMode="Password" runat="server"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="requireConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required."></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="compareConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword" ControlToCompare="txtNewPassword" ErrorMessage="Please enter same password as above"></asp:CompareValidator>
                                   <label id="lblconfirm"></label>
                                </div>
                            </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                    <asp:Button ID="btnSubmit" value="Submit" CssClass="btn btn-success" Text="Submit" OnClick="btnSubmit_Click" runat="server" />

                                    <input type="reset" value="Cancel" id="btnreset" class="btn btn-primary" />
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

        </div>


</asp:Content>

