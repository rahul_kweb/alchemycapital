﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Milestone :AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDropdown();
            BindGrid();
        }

    }


    public void BindDropdown()
    {
        //for (int i = 0; i < 21; i++)
        //{
        //    if (ddlYear.Text == (DateTime.Now.AddYears(i).Year - 20).ToString())
        //    {
        //        ddlYear.Items.Insert(0,new ListItem((DateTime.Now.AddYears(i).Year - 20).ToString()));
        //    }
        //    else
        //    {
        //        ddlYear.Items.Add(new ListItem((DateTime.Now.AddYears(i).Year - 20).ToString()));
        //    }
        //}
        int currentYear = DateTime.Now.Year;
        ddlYear.Items.Insert(0, new ListItem("Select Year", "0"));
        for (int i = 1998; i <= currentYear; i++)
        {

            ddlYear.Items.Add(i.ToString());
        }

    }



    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_Milestones 'get'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = false;
                gdView.DataSource = dt;
                gdView.DataBind();


            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();


            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {

                using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }

        else
        {
            if (CheckUpdate())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@MilesId",hdnId.Value);
                    cmd.Parameters.AddWithValue("@Year", ddlYear.SelectedValue.Trim());
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }

        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlYear.SelectedValue == "0")
        {
            isOK = false;
            message = "Select Year , ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description ";
        }

    
        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;

        if (ddlYear.SelectedValue == "0")
        {
            isOK = false;
            message = "Select Year, ";
        }


        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {

        //DateTime myDate = DateTime.Now;

        hdnId.Value = string.Empty;
        //int year = DateTime.Now.AddYears(-20).Year;
        //ddlYear.SelectedItem.Text = year.ToString();
        txtDescription.Text = string.Empty;
        btnsumbit.Text = "Save";
        ddlYear.SelectedValue = "0";

    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
            {
                int Id = (int)gdView.DataKeys[e.RowIndex].Value;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "delete");
                cmd.Parameters.AddWithValue("@MilesId", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            //int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_Milestones 'getbyId', '" + Id + "'");
            hdnId.Value = dt.Rows[0]["MilesId"].ToString();
            txtDescription.Text = dt.Rows[0]["Description"].ToString();
            ddlYear.SelectedValue = dt.Rows[0]["Year"].ToString();

            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }
}