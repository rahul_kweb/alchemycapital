﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_CurrentOpening : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindcurrentOpening();
        }
    }



    public void BindcurrentOpening()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Execute Proc_CurrentOpening 'get'");

        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }

        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Role", txtRole.Text.Trim());
                    cmd.Parameters.AddWithValue("@Location", txtLocation.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        BindcurrentOpening();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully saved");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "update");
                    cmd.Parameters.AddWithValue("@CurrId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Role", txtRole.Text.Trim());
                    cmd.Parameters.AddWithValue("@Location", txtLocation.Text.Trim());
                    if (utility.Execute(cmd))
                    {
                        BindcurrentOpening();
                        Reset();
                        MyMessageBox1.ShowSuccess("Successfully updated");
                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (txtRole.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Role, ";
        }



        if (txtLocation.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Location, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtRole.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Role, ";
        }



        if (txtLocation.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Location, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    private void Reset()
    {
        txtRole.Text = string.Empty;
        txtLocation.Text = string.Empty;
        hdnId.Value = string.Empty;
        btnsumbit.Text = "Save";

    }


    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindcurrentOpening();
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        DataTable dt = new DataTable();
        int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
        dt = utility.Display("Exec Proc_CurrentOpening 'getbyId','" + Id + "'");
        try
        {
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["CurrId"].ToString();
               txtRole.Text = dt.Rows[0]["Role"].ToString();
                txtLocation.Text = dt.Rows[0]["Location"].ToString();
               
                btnsumbit.Text = "Update";
            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@CurrId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindcurrentOpening();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

  
}