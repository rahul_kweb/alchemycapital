﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="RegulatoryInformationPage.aspx.cs" Inherits="Admin_RegulatoryInformationPage" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <asp:Label ID="lblHeading" runat="server" Style="color: #73879B"></asp:Label></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <asp:HiddenField ID="hdnId" runat="server" />
                                <asp:HiddenField ID="hdnItsId" runat="server" />



                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Heading <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtHeading" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Type <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control col-md-7 col-xs-12 type">
                                            <asp:ListItem Value="select Type" Text="Select Type"></asp:ListItem>
                                            <asp:ListItem Value="Pdf" Text="Pdf">Pdf</asp:ListItem>
                                            <asp:ListItem Value="Url" Text="Url">Url</asp:ListItem>
                                            <asp:ListItem Value="Image" Text="Image">Image</asp:ListItem>
                                             <asp:ListItem Value="Description" Text="Description">Description</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="form-group" id="divPdf" style="display: none;">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Pdf <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadPdf" runat="server" CssClass="form-control col-md-7 col-xs-12" />
                                        <asp:HyperLink ID="PdfPreview" runat="server" Text="View Pdf" Target="_blank"></asp:HyperLink>
                                    </div>
                                </div>

                                <div class="form-group" id="divUrl" style="display: none;">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Url <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtUrl" runat="server" CssClass="form-control col-md-7 col-xs-12" placeholder="https://www.google.co.in/" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                  <div class="form-group" id="divImages" style="display: none;">                           
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Image <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:FileUpload ID="fileUploadImage" CssClass="form-control col-md-7 col-xs-12" runat="server" />
                                        <asp:Image ID="ImagePreview" runat="server" Text="View Image" Visible="false" Target="_blank" Width="200" Height="150"></asp:Image>
                                    </div>
                                </div>

                                     <div class="form-group" id="divDescription" style="display: none;">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Description <span class="required" style="color: red">*</span>
                                    </label>
                                       <div class="col-md-6 col-sm-6 col-xs-12">                                  
                                         <CKEditor:CKEditorControl ID="txtDescription" runat="server"></CKEditor:CKEditorControl>                           
                                </div>
                                </div>

                                   <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Display Order
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">

                                        <button type="button" class='down_count btn btn-info' title='Down'><i class='fa fa-minus icon-minus'></i></button>
                                        <asp:TextBox ID="txtDisplayOrder" runat="server" class="counter" autocomplete="off"></asp:TextBox>
                                        <button type="button" class='up_count btn btn-info' title='Up'><i class='fa fa-plus icon-plus'></i></button><br /><br />
                                        <label>(Add 1 for PMS 2 For AIF / For annual keep it 1)</label>

                                    </div>
                                </div>

               

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visible <span class="required" style="color: red"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top: 15px;">
                                        <asp:CheckBox ID="ckstatus" CssClass="checkmark" runat="server"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    <input type="button" value="Back" id="btnBack" class="btn btn-dark" title="Back">
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="RInfoId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="RInfoId" HeaderText="RInfoId" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Heading" HeaderText="Heading" />

                                    <asp:TemplateField HeaderText="Pdf/Image/Url" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <a href='<%#Eval("Type").ToString()=="Pdf"?Eval("Pdf","../Content/uploads/Pdf/{0}").ToString():Eval("Type").ToString()=="Image"?Eval("Image","../Content/uploads/MediaImages/{0}").ToString():Eval("Url") %>' target="_blank">View</a>
                                            <br />
                                           <%-- <%#Eval("Type").ToString()=="Pdf"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Pdf","/Content/uploads/Pdf/{0}").ToString():Eval("Url") %>--%>
                                             <%--<%#Eval("Type").ToString()=="Pdf"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Pdf","/Content/uploads/Pdf/{0}").ToString():Eval("Url")%>:<%#Eval("Type").ToString()=="Image"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Image","/Content/uploads/MediaImages/{0}").ToString():Eval("Url")%>--%>
                                        
                                          <%#Eval("Type").ToString()=="Pdf"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Pdf","/Content/uploads/Pdf/{0}").ToString():Eval("Type").ToString()=="Image"?Request.ServerVariables["SERVER_NAME"] +""+ Eval("Image","/Content/uploads/MediaImages/{0}").ToString():Eval("Url") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>                                          
                                           <img src='<%#Eval("Status").ToString()=="1"?"/Content/uploads/checked.jpg":"/Content/uploads/button-cancel.png"%>' style="width:25px;height:25px"/>      
                                         <%--   <img src="/Content/uploads/checked.jpg" />     --%>                            
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <%#Eval("Description")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DisplayOrder" HeaderText="DisplayOrder" />
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>


    <script src="../Content/Admin/datepicker/jquery-ui.js" type="text/javascript"></script>




    <script>

        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });



        var type = $('#<%=ddlType.ClientID%>').val();
        if (type == 'Pdf') {
            $('#divPdf').show();
            $('#divUrl').hide();
            $('#divImages').hide();
            $('#divDescription').hide();
        }
        else if (type == 'Url') {
            $('#divUrl').show();
            $('#divPdf').hide();
            $('#divImages').hide();
            $('#divDescription').hide();
        }
        else if (type == 'Image')
        {
            $('#divImages').show();
            $('#divPdf').hide();
            $('#divUrl').hide();
            $('#divDescription').hide();
            
        }
        else if (type == 'Description')
        {
            $('#divDescription').show();
            $('#divPdf').hide();
            $('#divUrl').hide();
            $('#divImages').hide();
        }
        else {
            $('#divUrl').hide();
            $('#divPdf').hide();
            $('#divImages').hide();
            $('#divDescription').hide();
        }



        $(document).ready(function () {
            //var type = $('#<%=ddlType.ClientID%>').val();
            $('#<%=ddlType.ClientID%>').change(function () {
                var type = $('#<%=ddlType.ClientID%>').val();
                if (type == 'Pdf') {
                    $('#divPdf').show();
                    $('#divUrl').hide();
                    $('#divImages').hide();
                    $('#divDescription').hide();
                }
                else if (type == 'Url') {
                    $('#divUrl').show();
                    $('#divPdf').hide();
                    $('#divImages').hide();
                    $('#divDescription').hide();
                }
                else if (type == 'Image') {
                    $('#divImages').show();
                    $('#divPdf').hide();
                    $('#divUrl').hide();
                    $('#divDescription').hide();

                }
                else if (type == 'Description') {
                    $('#divDescription').show();
                    $('#divPdf').hide();
                    $('#divUrl').hide();
                    $('#divImages').hide();
                }
                else {
                    $('#divUrl').hide();
                    $('#divPdf').hide();
                    $('#divImages').hide();
                    $('#divDescription').hide();
                }
            });
        });


        $('#btnBack').click(function () {
            window.location.href = "RegulatoryInformation.aspx";
        })
    </script>

       <script>
        $(document).ready(function () {
            $('button').click(function (e) {
                var button_classes, value = +$('.counter').val();
                button_classes = $(e.currentTarget).prop('class');
                if (button_classes.indexOf('up_count') !== -1) {
                    value = (value) + 1;
                } else {
                    value = (value) - 1;
                }
                value = value < 0 ? 0 : value;
                $('.counter').val(value);
            });
            $('.counter').click(function () {
                $(this).focus().select();
            });
        });
    </script>


    <link href="../Content/Admin/datepicker/jquery-ui.css" rel="stylesheet" />


    <style>
        /* MODIFIED DATE PICKER */
        .ui-datepicker {
            width: 250px !important;
            padding: 2px !important;
            font-size: 14px !important;
        }

            .ui-datepicker table {
                margin: 0 !important;
            }

            .ui-datepicker .ui-widget-header {
                background: #04869a !important;
                /*color: #fff;*/
                font-weight: normal;
            }

        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }

        .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
            margin: 0px 4px !important;
            font-size: 13px !important;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }
    </style>

            <style>
    .counter {
        width: 45px;
        border-radius: 0px !important;
        text-align: center;
    }

    .up_count {
        margin-bottom: 10px;
        margin-left: -4px;
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
    }

    .down_count {
        margin-bottom: 10px;
        margin-right: -4px;
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;
    }
</style>

 

</asp:Content>


