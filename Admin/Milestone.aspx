﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Milestone.aspx.cs" Inherits="Admin_Milestone" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Milestones</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                                <div class="form-group">
                                    <asp:HiddenField ID="hdnId" runat="server" />
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Year <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <%--   <select name="year" class="form-control">
                                            @for (var i = 0; i < 21; i++)
                                            {

                                                if (Convert.ToInt32(Model.Year) == (DateTime.Now.AddYears(i).Year - 20))
                                                {
                                                    <option selected>@(DateTime.Now.AddYears(i).Year - 20)</option>
                                            }
                                                else
                                                {
                                                    <option>@(DateTime.Now.AddYears(i).Year - 20)</option>
                                            }

                                            }
                                        </select>--%>

                                        <asp:DropDownList ID="ddlYear" runat="server" class="form-control">
                                            <%--                                            <asp:ListItem Value=" " Selected="True" Text="Select Year"></asp:ListItem>--%>
                                        </asp:DropDownList>


                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Description <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" Height="150px" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" OnSelectedIndexChanging="gdView_SelectedIndexChanging" DataKeyNames="MilesId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">

                                <Columns>
                                    <asp:BoundField DataField="MilesId" HeaderText="MilesId" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Year" HeaderText="Year" />
                                    <asp:BoundField DataField="Description" HeaderText="Description" />

                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn" runat="server" CausesValidation="False"
                                                CommandName="Select">
                            <i class="icon-edit"></i> Edit
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>

