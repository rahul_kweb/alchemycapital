﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AscentPopUpRequestHit : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Bindgrid();
        }
    }


    // Display Alchemy Ascent Pop up Hit 
    public void Bindgrid()
    {
        DataTable dt = new DataTable();
        dt = utility.Display("Exec Proc_AscentPopUpHit 'get'");
        if (dt.Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = dt;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }


    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_AscentPopUpHit"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@Id", Id);
            if (utility.Execute(cmd))
            {
                Bindgrid();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }


    //Export All Alchemy Ascent Pop up Hit
    protected void btnDirectExport_Click(object sender, EventArgs e)
    {
        using (XLWorkbook wb = new XLWorkbook())
        {
            DataSet ds = new DataSet();
            ds = utility.Display1("Execute Proc_AscentPopUpHit 'GetAllExportToExcel'");
            wb.Worksheets.Add(ds);
            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            wb.Style.Font.Bold = true;

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename= Count_from_Pop_up.xlsx");

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }


    public bool check()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "From Date, ";
        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "To Date, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }


    //Export Alchemy Ascent Pop up Hit  Filter Grid
    protected void btnFilterExport_Click(object sender, EventArgs e)
    {

        if (check())
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = GetFilterDatafromDatabase();
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= Count_from_Pop_up.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }
    }

    /// Remove Id during export
    protected DataSet GetFilterDatafromDatabase()
    {
        DataSet ds = new DataSet();

        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_AscentPopUpHit 'GetForExportToExcelFromTo',0,'','','','','" + postDatefrom + "','" + postDateto + "'");

        return ds;

    }



    //Display Alchemy Ascent Pop up Request Hit  Filter Grid
    protected void btnFilterGrid_Click(object sender, EventArgs e)
    {
        if (check())
        {
            GetFilterDatafromDatabaseForGrid();

        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        Bindgrid();
        if (check1())
        {
            GetFilterDatafromDatabaseForGrid();
        }
    }


    protected void GetFilterDatafromDatabaseForGrid()
    {
        //DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_AscentPopUpHit 'GetForExportToExcelFromToForGrid',0,'','','','','" + postDatefrom + "','" + postDateto + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }


    public bool check1()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {

        }
        return isOK;
    }





}