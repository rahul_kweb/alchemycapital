﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_OurProduct : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }


    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = utility.Display("Execute Proc_OurProduct 'get'");

            if (dt.Rows.Count > 0)
            {
                gdView.Columns[0].Visible = false;
                gdView.DataSource = dt;
                gdView.DataBind();
               

            }
            else
            {
                gdView.DataSource = null;
                gdView.DataBind();
            

            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Some error occurred, While fetching records.");
        }
    }
    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/HomeBanner/";

        string extIcon = string.Empty;
        string MainImageIcon = string.Empty;
        string VirtualPartIcon = "~/Content/uploads/Icon/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "OurProduct-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                else if (fileUploadIconImage.HasFile)
                {
                    extIcon = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extIcon))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageIcon = utility.GetUniqueName(VirtualPartIcon, "Icon-", extIcon, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPartIcon + MainImageIcon + extIcon));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_OurProduct"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                    cmd.Parameters.AddWithValue("@Url",txtUrl.Text.Trim());


                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "OurProduct-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                else if (fileUploadIconImage.HasFile)
                {
                    extIcon = System.IO.Path.GetExtension(fileUploadIconImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(extIcon))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImageIcon = utility.GetUniqueName(VirtualPartIcon, "Icon-", extIcon, this, false);
                    fileUploadIconImage.SaveAs(Server.MapPath(VirtualPartIcon + MainImageIcon + extIcon));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_OurProduct"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@Id", hdnId.Value);
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@Heading", txtHeading.Text.Trim());
                    cmd.Parameters.AddWithValue("@Icon", MainImageIcon + extIcon);
                    cmd.Parameters.AddWithValue("@Url", txtUrl.Text.Trim());

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindGrid();
                        MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }

    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!fileUploadImage.HasFile)
        {
            message = "Background Image, ";
            isOK = false;
        }
      

        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Heading, ";
        }

        if (!fileUploadIconImage.HasFile)
        {
            isOK = false;
            message += "Icon, ";
        }
        if (txtUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Url, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;



        if (txtHeading.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Heading, ";
        }
        if (txtUrl.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Url, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

  

    private void Reset()
    {

        hdnId.Value = string.Empty;

        txtDescription.Text = string.Empty;
        txtHeading.Text = string.Empty;
        IconImagePreview.Visible = false;
        txtUrl.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;

    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindGrid();
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

        try
        {
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            //int Id = int.Parse(gdView.Rows[e.NewSelectedIndex].Cells[0].Text);
            DataTable dt = new DataTable();
            dt = utility.Display("Exec Proc_OurProduct 'getbyId', '" + Id + "'");
            hdnId.Value = dt.Rows[0]["Id"].ToString();
            txtDescription.Text = dt.Rows[0]["Description"].ToString();
            txtHeading.Text= dt.Rows[0]["Heading"].ToString();
            IconImagePreview.Visible = true;
            IconImagePreview.ImageUrl = string.Format("~/Content/uploads/Icon/" + dt.Rows[0]["Icon"].ToString());
            txtUrl.Text = dt.Rows[0]["Url"].ToString();
            ImagePreview.Visible = true;
            ImagePreview.ImageUrl = string.Format("~/Content/uploads/HomeBanner/" + dt.Rows[0]["Image"].ToString());

            btnsumbit.Text = "Update";

        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            using (SqlCommand cmd = new SqlCommand("Proc_OurProduct"))
            {
                int Id = (int)gdView.DataKeys[e.RowIndex].Value;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "delete");
                cmd.Parameters.AddWithValue("@Id", Id);
                utility.Execute(cmd);
                BindGrid();
                MyMessageBox1.ShowSuccess("Record Deleted Successfully.");
            }
        }
        catch (Exception ex)
        {
            MyMessageBox1.ShowError("Unable to delete record.");
        }
    }
}