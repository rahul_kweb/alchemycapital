﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ContactUs : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindContactUs();
        }
    }

    public void BindContactUs()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_ContactUs 'get'");
        if (ds.Tables[0].Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();


        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindContactUs();
           if (check1())
        {
            GetFilterDatafromDatabase();
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@Id", Id);
            if (utility.Execute(cmd))
            {
                BindContactUs();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
       
        if (check())
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = GetDatafromDatabase();
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= ContactUs.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

    }
    protected DataSet GetDatafromDatabase()
    {
        DataSet ds = new DataSet();

        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_ContactUs 'GetForExportToExcel','','','','','','','','','','','','','','','','','','','','','" + postDatefrom + "','" + postDateto + "'");

        return ds;

    }


    public bool check()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "From Date, ";
        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "To Date, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    protected void btnDirectExport_Click(object sender, EventArgs e)
    {
        using (XLWorkbook wb = new XLWorkbook())
        {
            DataSet ds = new DataSet();
            ds = utility.Display1("Execute Proc_ContactUs 'GetForExportToExcelDirect'");
            wb.Worksheets.Add(ds);
            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            wb.Style.Font.Bold = true;

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename= ContactUs.xlsx");

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
    
      protected void btnFilterGrid_Click(object sender, EventArgs e)
    {
        if (check())
        {
            GetFilterDatafromDatabase();

        }
    }

    // Display filter data in grid view
    protected void GetFilterDatafromDatabase()
    {
        //DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        string postDatefrom = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");
        string postDateto = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null).ToString("yyyy-MM-dd");

        ds = utility.Display1("execute Proc_ContactUs 'GetForExportToExcel','','','','','','','','','','','','','','','','','','','','','" + postDatefrom + "','" + postDateto + "'");
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }

    public bool check1()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtFrom.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (txtTo.Text.Trim().Equals(string.Empty))
        {
            isOK = false;

        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {

        }
        return isOK;
    }


    //protected void btnsumbit_Click(object sender, EventArgs e)
    //{


    //    if (btnsumbit.Text == "Save")
    //    {

    //    }
    //    else
    //    {
    //        if (CheckUpdate())
    //        {


    //            using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
    //            {
    //                cmd.CommandType = System.Data.CommandType.StoredProcedure;
    //                cmd.Parameters.AddWithValue("@para", "Update");
    //                cmd.Parameters.AddWithValue("@Id", hdnId.Value);
    //                cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedItem.Text.Trim());
    //                cmd.Parameters.AddWithValue("@Status_Comment", txtStatusComment.Text.Trim());


    //                if (utility.Execute(cmd))
    //                {
    //                    Reset();
    //                    BindContactUs();
    //                    MyMessageBox1.ShowSuccess("Successfully Updated");

    //                }
    //                else
    //                {
    //                    MyMessageBox1.ShowWarning("Unable to Update");
    //                }
    //            }
    //        }

    //    }

    //}

    //protected void btnreset_Click(object sender, EventArgs e)
    //{
    //    Reset();
    //}

    //private void Reset()
    //{
    //    ddlStatus.SelectedValue = "Select Status";
    //    hdnId.Value = string.Empty;
    //    txtStatusComment.Text = string.Empty;
    //    btnsumbit.Text = "Update";


    //}

    //public bool CheckUpdate()
    //{
    //    bool isOK = true;
    //    string message = string.Empty;


    //    if (ddlStatus.SelectedValue == "Select Status")
    //    {
    //        isOK = false;
    //        message = "Select Status, ";

    //    }


    //    if (txtStatusComment.Text.Trim().Equals(string.Empty))
    //    {
    //        isOK = false;
    //        message += "Status Comment, ";
    //    }


    //    if (message.Length > 0)
    //    {
    //        message = message.Substring(0, message.Length - 2);
    //    }
    //    if (!isOK)
    //    {
    //        MyMessageBox1.ShowError("Please fill following fields <br />" + message);
    //    }
    //    return isOK;
    //}


    //protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{
    //    try
    //    {
    //        int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);

    //        DataTable dt = new DataTable();
    //        dt = utility.Display("Exec Proc_ContactUs 'getbyId', '" + Id + "'");
    //        hdnId.Value = dt.Rows[0]["Id"].ToString();
    //        ddlStatus.Text = dt.Rows[0]["Status"].ToString();
    //        txtStatusComment.Text = dt.Rows[0]["Status_Comment"].ToString();

    //        btnsumbit.Text = "Update";

    //    }
    //    catch (Exception ex)
    //    {
    //        this.Title = ex.Message;
    //    }

    //}



    protected void gdView_RowEditing(object sender, GridViewEditEventArgs e)
    {

        gdView.EditIndex = e.NewEditIndex;
        BindContactUs();
    }

    protected void gdView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int id = Convert.ToInt16(gdView.DataKeys[e.RowIndex].Values["Id"].ToString());
        TextBox txtStatusComment = gdView.Rows[e.RowIndex].FindControl("txtStatusComment") as TextBox;
        DropDownList drpStatus = gdView.Rows[e.RowIndex].FindControl("ddlStatus") as DropDownList;

        using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
        {
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "Update");
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.Parameters.AddWithValue("@Status", drpStatus.SelectedItem.Text.Trim());
            cmd.Parameters.AddWithValue("@Status_Comment", txtStatusComment.Text.Trim());


            if (utility.Execute(cmd))
            {
                BindContactUs();
                MyMessageBox1.ShowSuccess("Successfully Updated");
                Response.Redirect("ContactUs.aspx");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Update");
            }
        }
    }

    protected void gdView_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gdView.Visible = true;

        gdView.EditIndex = -1;

        BindContactUs();
    }
}