﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_PeopleCulture : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.QueryString["PeopleCulture"] != null && Request.QueryString["PeopleCulture"] != "")
        {
            int testInt;
            if (int.TryParse(Request.QueryString["PeopleCulture"], out testInt))
            {
                hdnId.Value = Request.QueryString["PeopleCulture"];
              
                    BindPeopleCulture();
              
            }
        }
    }
    }


    public void BindPeopleCulture()
    {

        DataSet ds = new DataSet();
        ds = utility.Display1("Execute Proc_PeopleCulture 'get',0,'" + hdnId.Value + "'");
        ltrCategory.Text = ds.Tables[1].Rows[0]["Title"].ToString();
        if (ds.Tables.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource =null;
            gdView.DataBind();
        }
        }
    protected void btnsumbit_Click(object sender, EventArgs e)
    {
        string ext = string.Empty;
        string MainImage = string.Empty;
        string VirtualPart = "~/Content/uploads/Team/";

        if (btnsumbit.Text == "Save")
        {
            if (CheckSave())
            {
                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Team-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_PeopleCulture"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "add");
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);
                    cmd.Parameters.AddWithValue("@PcmId",hdnId.Value);
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Designation", txtDesignation.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description",txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder",txtDisplayOrder.Text.Trim());
                    cmd.Parameters.AddWithValue("@TeamCategory", txtTeamCategory.Text.Trim());
                    cmd.Parameters.AddWithValue("@Slug", Generateslg(txtName.Text.Trim()));

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindPeopleCulture();
                        MyMessageBox1.ShowSuccess("Successfully saved");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to save");
                    }
                }
            }
        }
        else
        {
            if (CheckUpdate())
            {

                if (fileUploadImage.HasFile)
                {
                    ext = System.IO.Path.GetExtension(fileUploadImage.FileName).ToLower();
                    if (!utility.IsValidImageFileExtension(ext))
                    {
                        MyMessageBox1.ShowError("Only Image file allowed.");
                        return; // STOP FURTHER PROCESSING
                    }

                    MainImage = utility.GetUniqueName(VirtualPart, "Team-", ext, this, false);
                    fileUploadImage.SaveAs(Server.MapPath(VirtualPart + MainImage + ext));
                }

                using (SqlCommand cmd = new SqlCommand("Proc_PeopleCulture"))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Para", "update");
                    cmd.Parameters.AddWithValue("@PcmId", hdnId.Value);
                    cmd.Parameters.AddWithValue("@PeopleCultId", hdnPeopleCultureId.Value);
                    cmd.Parameters.AddWithValue("@Image", MainImage + ext);                  
                    cmd.Parameters.AddWithValue("@Name", txtName.Text.Trim());
                    cmd.Parameters.AddWithValue("@Designation", txtDesignation.Text.Trim());
                    cmd.Parameters.AddWithValue("@Description",txtDescription.Text.Trim());
                    cmd.Parameters.AddWithValue("@DisplayOrder", txtDisplayOrder.Text.Trim());
                    cmd.Parameters.AddWithValue("@TeamCategory", txtTeamCategory.Text.Trim());
                    cmd.Parameters.AddWithValue("@Slug", Generateslg(txtName.Text.Trim()));

                    if (utility.Execute(cmd))
                    {
                        Reset();
                        BindPeopleCulture();
                         MyMessageBox1.ShowSuccess("Successfully updated");

                    }
                    else
                    {
                        MyMessageBox1.ShowWarning("Unable to update");
                    }
                }
            }
        }
    }


    public bool CheckSave()
    {
        bool isOK = true;
        string message = string.Empty;

        if (!fileUploadImage.HasFile)
        {
            message = "Profile Picture, ";
            isOK = false;
        }

        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Name, ";
        }

        if (txtDesignation.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Designation, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "DisplayOrder, ";
        }


        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    public bool CheckUpdate()
    {
        bool isOK = true;
        string message = string.Empty;


        if (txtName.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Name, ";
        }

        if (txtDesignation.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Designation, ";
        }

        if (txtDescription.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Description, ";
        }

        if (txtDisplayOrder.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message += "Display Order, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }

    private void Reset()
    {

        hdnPeopleCultureId.Value = string.Empty;
        txtName.Text = string.Empty;
        txtDesignation.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtDisplayOrder.Text = string.Empty;
        btnsumbit.Text = "Save";
        ImagePreview.Visible = false;
        txtTeamCategory.Text = string.Empty;
        //txtBigDescription.Text = string.Empty;

    }

    protected void btnreset_Click(object sender, EventArgs e)
    {
        Reset();
    }

    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindPeopleCulture();
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_PeopleCulture"))
        {
            int Id = (int)gdView.DataKeys[e.RowIndex].Value;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "delete");
            cmd.Parameters.AddWithValue("@PcmId", hdnId.Value);
            cmd.Parameters.AddWithValue("@PeopleCultId", Id);
            if (utility.Execute(cmd))
            {
                Reset();
                BindPeopleCulture();
                MyMessageBox1.ShowSuccess("Delete Successfully");
            }
            else
            {
                MyMessageBox1.ShowWarning("Unable to Delete");
            }
        }
    }

    protected void gdView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            int Id = Convert.ToInt32(gdView.DataKeys[e.NewSelectedIndex].Values[0]);
            dt = utility.Display("Exec Proc_PeopleCulture 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                hdnId.Value = dt.Rows[0]["PcmId"].ToString(); 
                hdnPeopleCultureId.Value = dt.Rows[0]["PeopleCultId"].ToString();
                txtName.Text = dt.Rows[0]["Name"].ToString();
               txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                txtDescription.Text = dt.Rows[0]["Description"].ToString();
                txtDisplayOrder.Text = dt.Rows[0]["DisplayOrder"].ToString();
                ImagePreview.Visible = true;
                ImagePreview.ImageUrl = string.Format("~/Content/uploads/Team/" + dt.Rows[0]["Image"].ToString());
                txtTeamCategory.Text = dt.Rows[0]["TeamCategory"].ToString();
                //txtBigDescription.Text = dt.Rows[0]["Big_Description"].ToString();

                btnsumbit.Text = "Update";

            }
        }
        catch (Exception ex)
        {
            this.Title = ex.Message;
        }
    }

    public string Generateslg(string strtext)
    {
        string str = strtext.ToString().ToLower();
        str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
        return str;
    }
}