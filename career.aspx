﻿<%@ Page Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="career.aspx.cs" Inherits="Career" %>

<asp:Content ID="Content2" runat="server" ContentPlaceholderID="HeaderContent">
    <title>Career Opportunities - Alchemy Capital</title>
   <meta name="description" content="Be a part of India's leading portfolio management company in India. Explore & learn from the industry's experts. Apply now!"/>

    <meta property="og:image" content="https://www.alchemycapital.com/content/images/Alchmey Logo_og.png" />
    <meta property="og:title" content="Alchemy Capital Management – Portfolio Management Services Company" />
    <meta property="og:description"  content="Managing PMS investments and building Equity Portfolios" />

    <meta property="og:url" content="https://www.alchemycapital.com/career.aspx" />

<link rel="canonical" href="https://www.alchemycapital.com/career.aspx"/>

    <link rel="alternate" href="https://www.alchemycapital.com/career.aspx" hreflang="en-in" />

    <link rel="alternate" href="https://www.alchemycapital.com/career.aspx" hreflang="x-default" />




</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Repeater ID="rptBanner" runat="server">
        <ItemTemplate>
            <div class="banner-area" id="banner-area" style="background-image: url(Content/uploads/InnerBanner/<%#Eval("Image") %>); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Career</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li>Career</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <!-- Banner area end-->
    <section class="main-container mrt-40 " id="main-container">

        <div class="container" id="career">
            <div class="row">


                <div class="col-sm-6 mrb-30">
                    <h2 class="column-title title-small">Career</h2>

                    <!--  <h3>We are hiring!</h3> -->

                    <asp:Literal ID="ltrCareerOverview" runat="server"></asp:Literal>
                    <%--  <p>Being one of the largest Portfolio Managers in the country comes with its fair share of responsibility and accountability; and Alchemy strives tirelessly to make sure neither the philosophy nor the investor’s goals are compromised.</p>
                    <p>Alchemy works with standardized best-in-class practices so you never have to settle for anything less than what you expect. The work ambience at Alchemy is designed such that it proves conducive to value, and subsequently, wealth creation for the clients.</p>
                    <p>And all that Alchemy stands for and works towards is reflected in the company ethos: Where the mind is without fear and the head is held high.</p>
                    <p>We look forward to motivated candidates who want to work in a challenging but rewarding atmosphere for the following positions:</p>--%>


                    <%--   <p><strong>Role :</strong> Relationship Managers   </p>
                    <p><strong>Location :</strong> Mumbai </p>
                    <a href="#careerForm">Apply Now <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                    <div class="dashedBorder mrb-30"></div>

                    <p><strong>Role :</strong> Operations   </p>
                    <p><strong>Location :</strong> Mumbai </p>
                    <a href="#careerForm">Apply Now <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    <div class="dashedBorder mrb-30"></div>

                    <p><strong>Role :</strong> Client Servicing</p>
                    <p><strong>Location :</strong> Mumbai</p>
                    <a href="#careerForm">Apply Now <i class="fa fa-angle-right" aria-hidden="true"></i></a>--%>
                </div>

                <div class="col-sm-5 offset-sm-1" id="careerForm">
                    <div class="service-content">
                        <h3>Apply for job with us</h3>
                        <%-- <form runat="server" enctype="multipart/form-data">--%>
                        <div id="loginForm">

                            <asp:Label ID="lblMsgcareer" runat="server"></asp:Label>
                            <div class="input-group bottomSpace30">
                                <asp:TextBox ID="txtName" CssClass="form-control" placeholder="Full Name" runat="server" autocomplete="off" Width="100%"></asp:TextBox>
                                <span id="ErrorName" class="ErrorValidation">Name is required.</span>
                            </div>



                            <div class="input-group bottomSpace30">
                                <asp:TextBox ID="txtCareerEmail" CssClass="form-control" placeholder="Email" runat="server" autocomplete="off" Width="100%"></asp:TextBox>
                                <span id="ErrorEmail1" class="ErrorValidation">Email is required.</span>
                                <span id="ErrorEmail2" class="ErrorValidation">Invalid email address.</span>
                            </div>

                            <div class="input-group bottomSpace30">

                                <asp:TextBox ID="txtContactNo" CssClass="form-control" placeholder="Phone" runat="server" MinLength="10" MaxLength="12" autocomplete="off" onkeypress="return isNumberKey(event)" Width="100%"></asp:TextBox>
                                <span id="ErrorPhone" class="ErrorValidation">Phone is required.</span>
                            </div>

                            <div class="input-group bottomSpace30">
                                <asp:DropDownList ID="ddlCareer" runat="server">
                                </asp:DropDownList>
                                <span id="ErrorCareer" class="ErrorValidation">Select atleast one role.</span>
                            </div>


                            <div class="input-group bottomSpace30">
                                <asp:FileUpload ID="fileResume" placeholder="Resume" runat="server" CssClass="myFile form-control" />
                                <%--<input type="file" name="ctl00$ContentPlaceHolder1$fileUploadResume" id="ContentPlaceHolder1_fileUploadResume" class="myFile form-control">--%>
                                <asp:Label ID="lblfileUploadResume" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnFakePath" runat="server" />
                                <span id="ErrorResume" class="ErrorValidation">Resume is required.</span>

                            </div>

                            <!--<div class="checkbox">
    <label><input type="checkbox" name="remember"> Remember me</label>
    </div>-->
                            <asp:Button ID="btnCareerSubmit" OnClick="btnCareerSubmit_Click" OnClientClick="javascript:return validation();" Text="Submit" runat="server" CssClass="btn btn-default" />

                            <div class="borderBottom bottomSpace30"></div>

                        </div>
                        <%--</form>--%>
                    </div>
                </div>

                <div class="col-12 dashedBorder mrb-30"></div>


                <%-- <h3 class="col-12">Current Openings: Interested candidates can click on <b>Apply Now</b> to upload resume</h3>--%>

                <asp:Literal ID="ltrCrrentOprning" runat="server"></asp:Literal>
            </div>


        </div>
    </section>

    <!--
      Javascript Files
      ==================================================
      -->
    <script src="Content/js/jquery-1.12.4.js"></script>

    <script type="text/javascript">
        //$(document).ready(function () {
        ////$('input').focusout(function () {
        //validation();
        //});
        //});

        function validation() {
            var Name = $('#<%=txtName.ClientID%>').val();
            var Email = $('#<%=txtCareerEmail.ClientID%>').val();
            var Phone = $('#<%=txtContactNo.ClientID%>').val();
            var career = $('#<%=ddlCareer.ClientID%>').val();
            var resume = $('#<%=fileResume.ClientID%>').get(0).files.length;
            var blank = false;

            if (Name == '') {
                $('#ErrorName').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorName').css('display', 'none');

            }


            if (Email == '') {
                $('#ErrorEmail2').css('display', 'none');
                $('#ErrorEmail1').css('display', 'block');
                blank = true;
            }
            else {
                var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

                if (!EmailText.test(Email)) {
                    $('#ErrorEmail1').css('display', 'none');
                    $('#ErrorEmail2').css('display', 'block');
                    blank = true;
                }
                else {
                    $('#ErrorEmail2').css('display', 'none');


                }
                $('#ErrorEmail1').css('display', 'none');


            }

            if (Phone == '') {
                $('#ErrorPhone').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorPhone').css('display', 'none');

            }

            if ($('#<%=ddlCareer.ClientID%>').val() == '0') {
                $('#ErrorCareer').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorCareer').css('display', 'none');

            }

            if (resume == 0) {
                $('#ErrorResume').css('display', 'block');
                blank = true;
            }
            else {
                $('#ErrorResume').css('display', 'none');

            }

            if (blank) {
                return false;
            }
            else {
                return true;
            }
        }
    </script>


    <script type="text/javascript">
        $('#<%=fileResume.ClientID%>').on("change", $('#<%=fileResume.ClientID%>'), function () {
            var fileUploadResume = document.getElementById('<%=fileResume.ClientID%>').value.replace("C:\\fakepath\\", "");
            $('#<%=hdnFakePath.ClientID%>').val(fileUploadResume);
            var allowedFiles = [".doc", ".docx", ".pdf"];
            var fileUpload = $('#<%=fileResume.ClientID%>');
            var lblError = $("#<%=lblfileUploadResume.ClientID%>");
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
            if (!regex.test(fileUpload.val().toLowerCase())) {
                lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.").attr('style', 'color:Red');
                $('.clsfile').val('notAllow');
                return false;
            }
            lblError.html(fileUploadResume).attr('style', 'color:black');
            $('.clsfile').val('Allow');
            return true;
        });

        function ShowPopUp() {
            $.fancybox.open('<div class="fade_anim"><div class="pop_head">Thank you, for your interest!</div><p style="color: cadetblue;">Our HR executive will get back to you shortly, <br /> if your resume is found eligible for the post applied.</p></div>');
        };

        $(document).ready(function () {

        })

        function RoleSelected(role) {
            $('#<%=ddlCareer.ClientID%>').val(role);
        }


        function isNumberKey(evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            var textBox = document.getElementById('<%=txtContactNo.ClientID%>').value.length;
            if (charCode > 31 && (charCode < 48 || charCode > 57) || textBox > 11)
                return false;

            return true;
        }

    </script>


    <script>
        $(document).ready(function (e) {
            $("a[href^='#careerForm']").click(function (e) {
                e.preventDefault();

                var position = $($(this).attr("href")).offset().top - 100;

                $("body, html").animate({
                    scrollTop: position
                }, 800);
            });
        });


    </script>

    <script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.alchemycapital.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Career",
    "item": "https://www.alchemycapital.com/career.aspx"  
  }]
}
</script>


</asp:Content>

