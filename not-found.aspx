﻿<%@ Page Title="Alchemy Capital Management" Language="C#" MasterPageFile="~/WebsiteMaster.master" AutoEventWireup="true" CodeFile="not-found.aspx.cs" Inherits="not_found" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="banner-area" id="banner-area" style="background-image: url(/Content/Images/banner_404.jpg); background-repeat: no-repeat;">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col">
                            <div class="banner-heading">
                                <h1 class="banner-title">Page Not Found</h1>
                                <ol class="breadcrumb">
                                    <li><a href="/">Home</a></li>
                                    <li>Page Not Found</li>
                                </ol>
                            </div>
                        </div>
                        <!-- Col end-->
                    </div>
                    <!-- Row end-->
                </div>
                <!-- Container end-->
            </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-5 text-center mb-4">
                <img src="/Content/images/404.jpg" class="img-fluid mb-3" />
                <h2 class="mb-4" style="color:#2f5498;">Sorry, But the page you requested doesn't exist</h2>
                <a href="/" class="btn btn-primary text-white">BACK HOME</a>
            </div>
            <div class="col-md-6 mb-4">
                <div class="row text-center">
                    <h3 class="col-12" style="color:#2f5498;">What are you looking for?</h3>
                    <div class="col-md-6">
                        <a href="/what-we-offer.aspx"><img src="/Content/images/2.jpg" class="img-fluid mb-3" /></a>
                        <a href="/what-we-offer.aspx"><h5 style="color:#2f5498;">What We Offer</h5></a>
                    </div>
                    <div class="col-md-6">
                        <a href="/people-and-culture.aspx"><img src="/Content/images/1.jpg" class="img-fluid mb-3" /></a>
                        <a href="/people-and-culture.aspx"><h5 style="color:#2f5498;">People and Culture</h5></a>
                    </div>
                </div>

                </div>
            </div>
        </div>

     
    

</asp:Content>

